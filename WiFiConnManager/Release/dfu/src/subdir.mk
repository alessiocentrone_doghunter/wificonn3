################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
..\dfu\src\arduino-server.cpp \
..\dfu\src\esp8266-log.cpp \
..\dfu\src\esp8266-spi-red.cpp 

C_SRCS += \
..\dfu\src\avrisp.c \
..\dfu\src\binary-file.c \
..\dfu\src\binary-format-bin.c \
..\dfu\src\binary-format-ihex.c \
..\dfu\src\binary-format-nordic-zip.c \
..\dfu\src\binary-formats-table.c \
..\dfu\src\crc32.c \
..\dfu\src\dfu-cmd.c \
..\dfu\src\dfu.c \
..\dfu\src\esp8266-spi.c \
..\dfu\src\esp8266.c \
..\dfu\src\file-container.c \
..\dfu\src\interface.c \
..\dfu\src\jsmn.c \
..\dfu\src\nordic-spi.c \
..\dfu\src\rx-method-http-arduino.c \
..\dfu\src\stk500-device-atmega328p.c \
..\dfu\src\stk500.c \
..\dfu\src\stm32-device-f469.c \
..\dfu\src\stm32-usart.c \
..\dfu\src\target.c 

C_DEPS += \
.\dfu\src\avrisp.c.d \
.\dfu\src\binary-file.c.d \
.\dfu\src\binary-format-bin.c.d \
.\dfu\src\binary-format-ihex.c.d \
.\dfu\src\binary-format-nordic-zip.c.d \
.\dfu\src\binary-formats-table.c.d \
.\dfu\src\crc32.c.d \
.\dfu\src\dfu-cmd.c.d \
.\dfu\src\dfu.c.d \
.\dfu\src\esp8266-spi.c.d \
.\dfu\src\esp8266.c.d \
.\dfu\src\file-container.c.d \
.\dfu\src\interface.c.d \
.\dfu\src\jsmn.c.d \
.\dfu\src\nordic-spi.c.d \
.\dfu\src\rx-method-http-arduino.c.d \
.\dfu\src\stk500-device-atmega328p.c.d \
.\dfu\src\stk500.c.d \
.\dfu\src\stm32-device-f469.c.d \
.\dfu\src\stm32-usart.c.d \
.\dfu\src\target.c.d 

LINK_OBJ += \
.\dfu\src\arduino-server.cpp.o \
.\dfu\src\avrisp.c.o \
.\dfu\src\binary-file.c.o \
.\dfu\src\binary-format-bin.c.o \
.\dfu\src\binary-format-ihex.c.o \
.\dfu\src\binary-format-nordic-zip.c.o \
.\dfu\src\binary-formats-table.c.o \
.\dfu\src\crc32.c.o \
.\dfu\src\dfu-cmd.c.o \
.\dfu\src\dfu.c.o \
.\dfu\src\esp8266-log.cpp.o \
.\dfu\src\esp8266-spi-red.cpp.o \
.\dfu\src\esp8266-spi.c.o \
.\dfu\src\esp8266.c.o \
.\dfu\src\file-container.c.o \
.\dfu\src\interface.c.o \
.\dfu\src\jsmn.c.o \
.\dfu\src\nordic-spi.c.o \
.\dfu\src\rx-method-http-arduino.c.o \
.\dfu\src\stk500-device-atmega328p.c.o \
.\dfu\src\stk500.c.o \
.\dfu\src\stm32-device-f469.c.o \
.\dfu\src\stm32-usart.c.o \
.\dfu\src\target.c.o 

CPP_DEPS += \
.\dfu\src\arduino-server.cpp.d \
.\dfu\src\esp8266-log.cpp.d \
.\dfu\src\esp8266-spi-red.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
dfu\src\arduino-server.cpp.o: ..\dfu\src\arduino-server.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -mlongcalls -mtext-section-literals -fno-exceptions -fno-rtti -falign-functions=4 -std=c++11 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\avrisp.c.o: ..\dfu\src\avrisp.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\binary-file.c.o: ..\dfu\src\binary-file.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\binary-format-bin.c.o: ..\dfu\src\binary-format-bin.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\binary-format-ihex.c.o: ..\dfu\src\binary-format-ihex.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\binary-format-nordic-zip.c.o: ..\dfu\src\binary-format-nordic-zip.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\binary-formats-table.c.o: ..\dfu\src\binary-formats-table.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\crc32.c.o: ..\dfu\src\crc32.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\dfu-cmd.c.o: ..\dfu\src\dfu-cmd.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\dfu.c.o: ..\dfu\src\dfu.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\esp8266-log.cpp.o: ..\dfu\src\esp8266-log.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -mlongcalls -mtext-section-literals -fno-exceptions -fno-rtti -falign-functions=4 -std=c++11 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\esp8266-spi-red.cpp.o: ..\dfu\src\esp8266-spi-red.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -mlongcalls -mtext-section-literals -fno-exceptions -fno-rtti -falign-functions=4 -std=c++11 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\esp8266-spi.c.o: ..\dfu\src\esp8266-spi.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\esp8266.c.o: ..\dfu\src\esp8266.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\file-container.c.o: ..\dfu\src\file-container.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\interface.c.o: ..\dfu\src\interface.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\jsmn.c.o: ..\dfu\src\jsmn.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\nordic-spi.c.o: ..\dfu\src\nordic-spi.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\rx-method-http-arduino.c.o: ..\dfu\src\rx-method-http-arduino.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\stk500-device-atmega328p.c.o: ..\dfu\src\stk500-device-atmega328p.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\stk500.c.o: ..\dfu\src\stk500.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\stm32-device-f469.c.o: ..\dfu\src\stm32-device-f469.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\stm32-usart.c.o: ..\dfu\src\stm32-usart.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

dfu\src\target.c.o: ..\dfu\src\target.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"D:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2/bin/xtensa-lx106-elf-gcc" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/lwip2/include" "-ID:\Programmi\sloeber_4_2\/arduinoPlugin/packages/esp8266/hardware/esp8266/2.4.1/tools/sdk/libc/xtensa-lx106-elf/include" "-ID:\Progetti\WiFiConn3_Workspace\WiFiConnManager/Release/core" -c -Wall -Wextra -Os -g -Wpointer-arith -Wno-implicit-function-declaration -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -std=gnu99 -ffunction-sections -fdata-sections -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=1460  -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_NODEMCU\""  -DESP8266  -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\cores\esp8266" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\variants\nodemcu" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WiFi\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\EEPROM" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266WebServer\src" -I"D:\Programmi\sloeber_4_2\arduinoPlugin\packages\esp8266\hardware\esp8266\2.4.1\libraries\ESP8266mDNS" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


