/*
 * SpiProtocolPacket.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SPI_PROTOCOL_PACKET_H_
#define SPI_PROTOCOL_PACKET_H_

// --- Includes .
#include <inttypes.h>

// --- Class definition .

// Class that represents a packet used to transfer data thought the SPI interface .
class EspiPacket
{
public:

	// Minimum packet length and minimum packet size .
	enum: uint16_t
	{
		MIN_PACKET_LEN 			= 6,
		MIN_PACKET_SIZE 		= 32
	};

	// Packet markers .
	enum Markers: uint8_t
	{
		START_PACKET 			= 0xe0,
		END_PACKET 				= 0xee
	};

	// Packet flags .
	enum PacketFlags: uint8_t
	{
		PACKET_HEADER_FLAG		= 0x01,
		PACKET_TAIL_FLAG		= 0x02,
		PACKET_COMPLETE_FLAG	= 0x03,
	};

	// Constructor .
	EspiPacket(uint8_t *buffer, uint16_t size);

	// Destructor .
	virtual ~EspiPacket();


	// Return the pointer to the memory that contains the data .
	uint8_t *buffer() { return _buffer; }

	// Return the maximum size of the buffer .
	uint16_t size()  { return _size; }

	// Code associated to the packet .
	uint8_t code() { return _code; }

	// Return the actual number of bytes present in the buffer .
	uint16_t length() { return _length; }

	// Return the number of parameters .
	uint8_t numberOfParams() { return _numberOfParams; }


	// Return if the packet buffer and packet size are valid .
	bool isValid() { return (_buffer != nullptr && _size >= MIN_PACKET_SIZE); }

	// Return if the packet is complete (contains the header and the tail) .
	bool isComplete() { return ((_flags & PACKET_COMPLETE_FLAG) == PACKET_COMPLETE_FLAG); }


	// Clear and reset the packet internal data .
	void clear();

	// Set the buffer pointer and the buffer size associated to the packet .
	void setBuffer(uint8_t *buffer, uint16_t size);


	// Initiate the edit phase .
	bool beginEdit(uint8_t code);

	// Append a 8 bit parameter to the packet buffer data .
	uint16_t appendBoolean(bool param);

	// Append a 8 bit parameter to the packet buffer data .
	uint16_t appendByte(uint8_t param);

	// Append a 8 bit parameter to the packet buffer data .
	uint16_t appendWord(uint16_t param);

	// Append a 8 bit parameter to the packet buffer data .
	uint16_t appendDWord(uint32_t param);

	// Append a buffer as a parameter to the packet buffer data .
	uint16_t appendBuffer(uint8_t *buffer, uint16_t bufferLength);

	// Append a text string to the packet buffer data .
	uint16_t appendString(const char *s);

	// Reserve the memory in the packet to be filled with data .
	uint8_t *reserveData(uint16_t dataLength);

	// Finalize the edit phase .
	bool endEdit();


	// Initiate the decoding phase .
	bool beginDecode();

	// Indicate if the packet has the specified parameter .
	bool hasParam(uint8_t paramId);

	// Return the parameter length in bytes, 0 if there are no parameters .
	uint16_t paramLength(uint8_t paramId);

	// Read an 8 bit parameter .
	uint16_t readBoolean(uint8_t paramId, bool *param);

	// Read an 8 bit parameter .
	uint16_t readByte(uint8_t paramId, uint8_t *param);

	// Read a 16 bit parameter .
	uint16_t readWord(uint8_t paramId, uint16_t *param);

	// Read a 32 bit parameter .
	uint16_t readDWord(uint8_t paramId, uint32_t *param);

	// Read a buffer coping data from packet buffer .
	uint16_t readBuffer(uint8_t paramId, uint8_t *buffer, uint16_t bytesToRead, uint16_t offset = 0);

	// Get the pointer to the data stored inside the packet buffer .
	uint8_t *getBuffer(uint8_t paramId);

	// Read a text string coping data from packet buffer .
	uint16_t readString(uint8_t paramId, char *s, uint16_t maxLength);

	// Get the pointer to the text string stored inside the packet buffer .
	const char *getString(uint8_t paramId);

	// Finalize the decoding phase .
	bool endDecode();

private:

	enum MaskFlags: uint8_t
	{
		REPLY_CODE_FLAG 		= 0x80,
		EXT_PARAM_LEN_FLAG		= 0x80
	};

	// Maximum parameter size .
	enum: uint16_t
	{
		MAX_PARAM_LEN 			= 0x7fff
	};

	// Pointer to the memory that contains the data .
	uint8_t *_buffer;

	// Max size of the buffer .
	uint16_t _size;

	// Code associated to the packet .
	uint8_t _code;

	// Actual number of bytes present in the buffer .
	uint16_t _length;

	// Number of parameters .
	uint8_t _numberOfParams;

	// State and decode flags .
	uint8_t _flags;

	// Return a pointer to parameter data memory inside the packet and the parameter length .
	uint8_t *_findParam(uint8_t paramId, uint16_t *paramLength);


	// Friend classes .
	friend class EspiMaster;
	friend class SpiProtocolSlave;

};

#endif /* SPI_PROTOCOL_PACKET_H_ */
