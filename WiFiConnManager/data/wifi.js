var currAp = "";
var blockScan = 0;
var attempt = 0;
var networkAlert = "Connect your laptop or smart device to the RED access point again and look at the new IP address in the dedicated box.<div onclick=\"hide('notification')\" class=\"gotit\">Got It</div>";
function createInputForAp(b, n) {
    if (b.essid == "" && b.rssi == 0) {
        return
    }
    var g = e("input");
    g.type = "radio";
    g.name = "essid";
    g.value = b.essid;
    g.id = "opt-" + b.essid + n;
    if (currAp == b.essid) {
        g.checked = "1"
    } 
    var i = e("div"), j;
    if (b.rssi >= -75) j = -88;
    else if (b.rssi >= -95 && b.rssi < -75) j = -67;
    else if (b.rssi < -95) j = -3;
    i.className = "lock-icon wifi-icon";
    i.style.backgroundPosition = "0px " + j + "px";
    var d = e("div");
    d.innerHTML = "" + b.rssi + "dB";
    
    var c = e("div");
    var h = "-25";
    if (b.enc == "0") {
        h = "0"
    }
    c.className = "lock-icon";
    c.style.backgroundPosition = "-19px " + h + "px";
    var f = e("div");
    f.innerHTML = b.essid;
    var a = m('<label for="opt-' + b.essid + n + '"><span></span></label>').childNodes[0];
    //a.appendChild(g);
    a.appendChild(c);
    a.appendChild(i);
    //a.appendChild(d);
    a.appendChild(f);
    var con = m('<div></div>').childNodes[0];
    con.appendChild(g);
    con.appendChild(a);
    
    return con
}
function getSelectedEssid() {
    var c = document.forms.wifiform.elements;
    for (var b = 0; b < c.length; b++) {
        if (c[b].type == "radio" && c[b].checked) {
            var a = c[b].value;
            if (a == "_hidden_ssid_") {
                a = $("#hidden-ssid").value
            }
            return a
        }
    }
    return currAp
}
var scanTimeout = null;
var scanReqCnt = 0;
function scanResult() {
    if (scanReqCnt > 60) {
        return scanAPs()
    }
    scanReqCnt += 1;
    ajaxJson("GET", "wifi/scan", function (c) {
        currAp = getSelectedEssid();
        if (c.result.APs.length > 0) {
            $("#aps").innerHTML = "";
            var d = 0;
            for (var b = 0; b < c.result.APs.length; b++) {
                if (c.result.APs[b].essid == "" && c.result.APs[b].rssi == 0) {
                    continue
                }
                $("#aps").appendChild(createInputForAp(c.result.APs[b], d));
                d = d + 1
            }
            enableNetworkSelection();
            showNotification("Scan found " + d + " networks", 'wifinotification');
            var a = $("#connect-button");
            a.className = a.className.replace(" pure-button-disabled", "");
            if (scanTimeout != null) {
                clearTimeout(scanTimeout)
            }
        }
        else {
            showWarning("AP's not found")
        }
    }, function (b, a) {
        showWarning("Please rescan")
    })
}
function scanAPs() {
    hideWarning();
    if (blockScan) {
        scanTimeout = window.setTimeout(scanAPs, 1000);
        return
    }
    showNotification("Scannning...", 'wifinotification');
    ajaxReq("GET", "wifi/netNumber", function (a){
        scanTimeout = null;
        scanReqCnt = 0;
        scanResult();
    }, function(b,a){
        $("#wifinotification").setAttribute("hidden", "");
        showWarning("Error during scanning, please retry.")
    })    
}
function getStatus() {
    ajaxJsonSpin("GET", "connstatus", function (c) {
        if (c.status == "idle" || c.status == "connecting") {
            $("#aps").innerHTML = "Connecting...";
            showNotification("Connecting...", 'wifinotification');
            window.setTimeout(getStatus, 1000)
        }
        else {
            if (c.status == "connected") {
                var a = "Connected! Got IP " + c.ip;
                showNotification(a, 'wifinotification');
                showWifiInfo(c);
                blockScan = 0;
                if (c.modechange == "yes") {
                    var b = "esp will switch to STA-only mode in a few seconds";
                    window.setTimeout(function () {
                        showNotification(b, 'wifinotification')
                    }, 4000)
                }
                $("#notification").innerHTML = (window.location.hostname == '192.168.240.1') ? "Your board is now connected to your network at the following IP: " + c.ip + " - Pay attention to the yellow box and remember this IP in case you need to join this panel again.<div onclick=\"hide('notification')\" class=\"gotit\">Got It</div>" : networkAlert;
                $("#notification").removeAttribute("hidden");
            }
            else {
                blockScan = 0;
                $("#wifinotification").setAttribute("hidden", "");
                showWarning("Connection failed");
                //$("#aps").innerHTML = 'Check password and selected AP. <a href="wifi.html">Go Back</a>'
            }
        }
        enableNetworkSelection()
    }, function (b, a) {
        if (attempt < 3 && window.location.hostname == '192.168.240.1') {
            showWarning("Problems in connection...I'm trying again");
            window.setTimeout(hideWarning, 3000);
            window.setTimeout(getStatus, 2000);
            attempt++;
        }
        else {
            $("#notification").innerHTML = networkAlert;
            $("#notification").removeAttribute("hidden");
            $("#wifi-ip").innerHTML = '';
            removeClass($('#ip-address'), "yellow");
            attempt = 0;
            blockScan = 0;
        }
    })
}
function changeWifiMode(a) {
    if (confirm(networkAlert)) {
        blockScan = 1;
        hideWarning();
        ajaxSpin("GET", "setmode?mode=" + a, function (b) {
            showNotification("Mode changed");
            window.setTimeout(getWifiInfo, 100);
            blockScan = 0;
            window.setTimeout(enableNetworkSelection, 500)
        }, function (c, b) { //b is the error message, sometimes is empty
            showWarning("Error changing mode ");
            window.setTimeout(getWifiInfo, 100);
            blockScan = 0;
            window.setTimeout(enableNetworkSelection, 500)
        })
    }
}
function changeWifiAp(d) {
        d.preventDefault();
        var b = $("#wifi-passwd").value;
        var f = getSelectedEssid();
        //showNotification("Connecting to " + f, 'wifinotification');
        $("#wifinotification").innerHTML = "Connecting to " + f;
        $("#wifinotification").removeAttribute("hidden");
        var c = "connect?essid=" + encodeURIComponent(f) + "&passwd=" + encodeURIComponent(b);
        hideWarning();
        $("#reconnect").setAttribute("hidden", "");
        $("#wifi-passwd").value = "";
        var a = $("#connect-button");
        var g = a.className;
        a.className += " pure-button-disabled";
        blockScan = 1;
        ajaxSpin("GET", c, function (h) {
            if(h==1)
                window.setTimeout(function(){
                    showNotification("Waiting for network change...", 'wifinotification');
                    window.scrollTo(0, 0);
                    window.setTimeout(getStatus, 2000)
                },10000);
        }, function (i, h) {
            $("#wifinotification").setAttribute("hidden", "");
            showWarning("Error switching network: " + h);
            a.className = g;
            window.setTimeout(scanAPs, 1000)
        })
}

function changeSpecial(c) {
        c.preventDefault();
        var b = "special";
        b += "?dhcp=" + document.querySelector('input[name="dhcp"]:checked').value;
        b += "&staticip=" + encodeURIComponent($("#wifi-staticip").value);
        b += "&netmask=" + encodeURIComponent($("#wifi-netmask").value);
        b += "&gateway=" + encodeURIComponent($("#wifi-gateway").value);
        hideWarning();
        var a = $("#special-button");
        addClass(a, "pure-button-disabled");
        ajaxSpin("GET", b, function (d) {
            removeClass(a, "pure-button-disabled")
            if (d != 1) {
                //alert("New IP set, you will be redirect to: " + JSON.parse(d).url);
                $("#notification").innerHTML = "Your board is now connected to your network at the following IP: " + JSON.parse(d).url + " - Pay attention to the yellow box and remember this IP in case you need to join this panel again.<div onclick=\"hide('notification')\" class=\"gotit\">Got It</div>";
                $("#notification").removeAttribute("hidden");
                setTimeout(document.location.href = "http://" + JSON.parse(d).url + "/index.html", 5000);
            }
            else  {
                $("#notification").innerHTML = networkAlert;
                $("#notification").removeAttribute("hidden");
                $("#wifi-ip").innerHTML = '';
                removeClass($('#ip-address'), "yellow");
                
            }
        }, function (f, d) {
            showWarning("Error: " + d);
            removeClass(a, "pure-button-disabled");
            getWifiInfo()
        })
}
function changeHostname() {
        var a = $("#change-hostname-input").value;
        if (a == "") {
            alert("Insert hostname!")
        }
        else {
            ajaxSpin("GET", "/system/update?name=" + a, function () {
                showNotification("Hostname changed in : " + a, 'hostnamenotification');
            })
        }
}
function showHostnameModal(b) {
    var a = "Hostname changed in : " + b; //+ "\nYour board will be reboot to apply change";
    var c = confirm(a);
    if (c == false) alert("Error in hostname change");
}
function hostnameLimitations(c) {
    var b = new RegExp("^[a-zA-Z0-9\-_\b]+$");
    var a = String.fromCharCode(!c.charCode ? c.which : c.charCode);
    if (!b.test(a)) {
        c.preventDefault();
        return false
    }
}
function enableNetworkSelection() {
    ajaxJson("GET", "/wifi/info", function (j) {
        var a = (j.mode == "STA");
        var h = document.getElementById("wifiform")
            , c = h.getElementsByTagName("input")
            , f = $("#connect-button")
            , s = $("#scan-button");
        var g, d = 0;
        while (g = c[d++]) {
            g.disabled = a
        }
        f.disabled = a;
        s.disabled = a;
        if (a) {
            bnd(h, "mouseover", displayWiFiModeAlert);
            bnd(h, "mouseout", hideWiFiModeAlert)
            toggleClass(f, "pure-button-disabled")
        }
        else {
            ubnd(h, "mouseover", displayWiFiModeAlert);
            ubnd(h, "mouseout", hideWiFiModeAlert)
            removeClass(f, "pure-button-disabled")
        }
    })
}
function displayWiFiModeAlert() {
    $("#alertWiFiMode").style.display = "inherit"
}
function hideWiFiModeAlert() {
    $("#alertWiFiMode").style.display = "none"
}
function showAdvanced() {
    var a = $("#advanced-menu-b"), b = $("#advanced-menu");
    toggleClass(b,'menu-hidden');
    toggleClass(a,"arrow-up");
    toggleClass(a,"arrow-down");
};
function enableStatic() {
    $("#wifi-staticip").removeAttribute('disabled');
    $("#wifi-netmask").removeAttribute('disabled');
    $("#wifi-gateway").removeAttribute('disabled');  
}
function disableStatic() {
    $("#wifi-staticip").setAttribute('disabled',1);
    $("#wifi-netmask").setAttribute('disabled',1);
    $("#wifi-gateway").setAttribute('disabled',1);  
}