/*
 * DfuManager.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created on 25 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include "dfu/src/dfu.h"
#include "dfu/src/dfu-avrisp.h"

#include <ESP8266WebServer.h>

#include "WiFiConnConfig.h"

#include "DfuManager.h"

/*******************************************************************************
    Static member initialization .
*******************************************************************************/

// Initialization state .
bool DfuManager::_initialized = false;

// Pointer to service data .
struct dfu_data *DfuManager::_dfuData = nullptr;

// Pointer to binary file .
struct dfu_binary_file *DfuManager::_binaryFile = nullptr;

// Pointer to the Web server .
ESP8266WebServer *DfuManager::_server = nullptr;

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/

// Initialize the "Device Firmware Upgrade Manager" .
bool DfuManager::initialize(ESP8266WebServer *server)
{

#ifdef WIFI_CONN_DEBUG_DFU

	// Initialize DFU log system .
	dfu_log_set_serial(Serial);

#endif // WIFI_CONN_DEBUG_DFU

	if (server != nullptr)
	{
		_server = server;

		_initialized = true;
	}

	return _initialized;
}

// Handle the DFU service .
void DfuManager::handle()
{
	if (_initialized == false)
	{
		return;
	}

	if (_isStarted() == false)
	{
		_start();
	}

	if (_isStarted() == true)
	{
		switch (_process())
		{
			case DFU_ERROR:
			{
				dfu_log("Error programming file");

				// End service in order to reinitialize it .
				_end();

				break;
			}
			case DFU_ALL_DONE:
			{
				// Launch target application firmware .
				if (_launchTarget() == true)
				{
					dfu_log("Programming OK");
				}
				else
				{
					dfu_log("Error launching target firmware");
				}

				// End service in order to reinitialize it .
				_end();

				break;
			}
			case DFU_CONTINUE:
			{
				break;
			}
		}
	}
}

/*******************************************************************************
    Private methods implementation .
*******************************************************************************/

// Start the DFU service .
bool DfuManager::_start()
{
	_dfuData = dfu_init(&esp8266_spi_red_interface_ops,
			NULL,
			NULL,
			NULL,
			NULL,
			&avrisp_dfu_target_ops,
			&atmega328p_device_data,
			&esp8266_dfu_host_ops,
			NULL,
			NULL);

	if (_dfuData == nullptr)
	{
		dfu_log("Error initializing DFU library");

		return false;
	}

	_binaryFile = dfu_binary_file_start_rx(&dfu_rx_method_http_arduino, _dfuData, _server);

	if (_binaryFile == nullptr)
	{
		dfu_log("Error instantiating binary file");

		return false;
	}

	if (dfu_binary_file_flush_start(_binaryFile) < 0)
	{
		dfu_log("Error in dfu_binary_file_flush_start()");
	}

	return true;
}

// Indicate if the service has been started .
bool DfuManager::_isStarted()
{
	if (_dfuData != nullptr && _binaryFile != nullptr)
	{
		return true;
	}

	return false;
}

// Process the service .
int DfuManager::_process()
{
	return dfu_idle(_dfuData);
}

// Launch target application firmware .
bool DfuManager::_launchTarget()
{
	if (dfu_target_go(_dfuData) < 0)
	{
		return false;
	}

	return true;
}

// End the DFU service .
void DfuManager::_end()
{
	dfu_binary_file_fini(_binaryFile);
	dfu_fini(_dfuData);

	_dfuData = nullptr;
	_binaryFile = nullptr;
}

/******************************************************************************/
