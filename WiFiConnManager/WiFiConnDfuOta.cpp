/*
 * WiFiConnDfuOta.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created on 01 October 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include "DfuManager.h"
#include "WiFiConnConfig.h"
#include "WiFiConnParam.h"
#include "WiFiConnDfuOta.h"


// Global variable .
ESP8266WebServer server(80);

const char ssid[] = "whatsnext-guest";
const char passphrase[] = "ht34!eG$";

// The setup function is called once at startup of the sketch
void setup()
{
	Serial.begin(115200);

	delay(3000);

	// Startup message .
	Serial.println();
	Serial.println(" - WiFiConn DFU OTA " + (String)WIFI_CONN_FW_VERSION + " - ");

	// Try to connect to the access points .
	startWiFi();

	MDNS.begin(WIFI_CONN_DEVICE_NAME);

	MDNS.enableArduino(80);

	// Initialize DFU manager .
	DfuManager::initialize(&server);
}

// The loop function is called in an endless loop
void loop()
{
	if (WiFi.isConnected() == true)
	{
		// Handle DFU service .
		DfuManager::handle();
	}
	else
	{
		Serial.println();
		Serial.println(" - Connection with WiFi network LOST! Restarting WiFi ...");

		startWiFi();
	}
}

// Try to connect to the access points .
void startWiFi()
{
	// Read WiFi module status .
	wl_status_t status = WiFi.status();

	// Attempt to connect to WiFi network:
	while (status != WL_CONNECTED)
	{
		Serial.println();
		Serial.print(" - Attempting to connect to ");
		Serial.println(ssid);

		// Connect to WPA/WPA2 network .
		status = WiFi.begin(ssid, passphrase);

		if (status != WL_CONNECTED)
		{
			uint32_t startMillis = millis();

			while ((millis() - startMillis) < 5000 && status == WL_DISCONNECTED)
			{
				status = WiFi.status();

				delay(10);
			}

			if (status != WL_CONNECTED)
			{
				WiFi.disconnect();
			}
		}

		if (status != WL_CONNECTED)
		{
			// Wait 5 seconds before trying to connect again .
			delay(5000);
		}
	}

	Serial.println();

	Serial.print(" - Connected to ");
	Serial.println(WiFi.SSID());

	Serial.print(" - IP address:\t");
	Serial.println(WiFi.localIP());

	Serial.print(" - RSSI:\t");
	Serial.print(WiFi.RSSI());
	Serial.println(" dBm");
	Serial.println();

	Serial.flush();
}
