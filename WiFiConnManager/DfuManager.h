/*
 * DfuManager.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created on 25 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DFU_MANAGER_H_
#define DFU_MANAGER_H_

#include <ESP8266WebServer.h>

// Class that implements the "Device Firmware Upgrade" service .
class DfuManager
{
public:

	// Initialize the DFU service .
	static bool initialize(ESP8266WebServer *server);

	// Handle the DFU service .
	static void handle();

private:

	// Initialization state .
	static bool _initialized;

	// Pointer to service data .
	static struct dfu_data *_dfuData;

	// Pointer to binary file .
	static struct dfu_binary_file *_binaryFile;

	// Pointer to the Web server .
	static ESP8266WebServer *_server;


	// Start the DFU service .
	static bool _start();

	// Indicate if the service has been started .
	static bool _isStarted();

	// Process the service .
	static int _process();

	// Launch target application firmware .
	static bool _launchTarget();

	// End the DFU service .
	static void _end();
};

#endif /* DFU_MANAGER_H_ */
