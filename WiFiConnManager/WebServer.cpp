/*
 * WebServer.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created on 04 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <FS.h>
#include <ESP8266mDNS.h>
#include <user_interface.h>

#include "WiFiConnParam.h"
#include "WiFiConnConfig.h"

#include "WebServer.h"

/*******************************************************************************
    Definitions .
*******************************************************************************/

#define CONFIG_FILE_NAME	"/config.json"

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/


// Constructor .
WebServer::WebServer() :
	_isRunning(false),
	_server(nullptr),
	_numberOfNetworks(-1),
	_firstNetworksScan(true),
	_scanNetworksTime(0),
	_connectionInProgress(false),
	_dhcpState(true)
{

}

// Destructor .
WebServer::~WebServer()
{
	end();
}

// Initialize the server and the hw/sw resources .
bool WebServer::begin(IPAddress ipAddress, uint16_t port)
{
	// Close any previous instances .
	end();

	// Instantiate a new web server .
	_server = new ESP8266WebServer(ipAddress, port);

	if (_server != nullptr)
	{
		if (SPIFFS.begin() == true)
		{
			// Deploy the common parameters system .
			if (WiFiCommon.begin() == true)
			{
				String hostname = WiFiCommon.getHostname();

				// Set the host name .
				WiFi.hostname(hostname);

				// Start a multi-DNS service .
				MDNS.begin(hostname.c_str());
				MDNS.setInstanceName(hostname);

				SPIFFS.begin();

				// Initialize web page handlers  .
				_initializeHandlers();

				_server->begin();

#ifdef WIFI_CONN_DEBUG_SERVER

				Serial.println(" - WebServer running ...");

#endif // WIFI_CONN_DEBUG_SERVER

				// Mark the service as 'running' .
				_isRunning = true;
			}
			else
			{
				SPIFFS.end();
			}
		}
	}

	return _isRunning;
}

// Release the hw/sw resources .
void WebServer::end()
{
	if (_isRunning == true)
	{
		_server->close();

		// Delete the web server .
		delete _server;

		// Reset the pointer .
		_server = nullptr;

		// Dispose the common parameters system .
		WiFiCommon.end();

		SPIFFS.end();

		// Mark the service as 'not running' .
		_isRunning = false;
	}
}

// Handle the incoming HTTP requests .
void WebServer::handle()
{
	if (_isRunning == true)
	{
		_server->handleClient();
	}
}

/*******************************************************************************
    Private methods implementation .
*******************************************************************************/

// Initialize web page handlers  .
void WebServer::_initializeHandlers()
{
	_server->on("/wifi/info", [&] { _handleWiFiInfo(); } );

	_server->on("/system/info", [&] { _handleSystemInfo(); } );

	_server->on("/system/update", [&] { _handleSystemNameUpdate(); } );

	_server->on("/wifi/netNumber", [&] { _handleNumberOfNetworks(); } );

	_server->on("/wifi/scan", [&] { _handleNetworkScanning(); } );

	_server->on("/connect", [&] { _handleStartConnection(); } );

	_server->on("/connstatus", [&] { _handleConnectionStatus(); } );

	_server->on("/special", [&] { _handleIPConfig(); } );

	_server->on("/boardInfo", [&] { _handleBoardInfo(); } );

	_server->on("/heap", [&] { _handleFreeHeap(); } );

	_server->on("/sketch", [&] { _handleSketchName(); } );

	_server->onNotFound( [&] { _handleFileNotFound(); } );
}


// Handle a request of WiFi informations .
void WebServer::_handleWiFiInfo()
{
	String response;

	String ipAddress;
	String ssid;
	String staticAddress;
	String dhcp;

	WiFiMode_t wifiMode = WiFi.getMode();
	WiFiMode_t nextWiFiMode;

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.print("handleWiFiInfo: ... ");

#endif // WIFI_CONN_DEBUG_SERVER

	if (wifiMode == WiFiMode::WIFI_STA || wifiMode == WiFiMode::WIFI_AP_STA )
	{
		ipAddress = WiFi.localIP().toString();
		ssid = WiFi.SSID();
	}
	else
	{
		ipAddress = WiFi.softAPIP().toString();
		ssid = "none";
	}

	if (_dhcpState == true)
	{
		staticAddress = "0.0.0.0";
		dhcp = "on";
	}
	else
	{
		staticAddress = ipAddress;
		dhcp = "off";
	}

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println("dhcp" + dhcp);

#endif // WIFI_CONN_DEBUG_SERVER

	if (wifiMode == WIFI_STA)
	{
		nextWiFiMode = WiFiMode::WIFI_AP_STA;
	}
	else
	{
		nextWiFiMode = WiFiMode::WIFI_STA;
	}

	response += "{";

	response += "\"ssid\":\"" + ssid + "\",";
	response += "\"hostname\":\"" + WiFi.hostname() + "\",";
	response += "\"ip\":\"" + ipAddress + "\",";
	response += "\"mode\":\"" + _wifiModeToString(WiFi.getMode()) + "\",";
	response += "\"chan\":\"" + String(WiFi.channel()) + "\",";
	response += "\"status\":\"" + _wifiStatusToString(WiFi.status()) + "\",";
	response += "\"gateway\":\"" + WiFi.gatewayIP().toString() + "\",";
	response += "\"netmask\":\"" + WiFi.subnetMask().toString() + "\",";
	response += "\"rssi\":\"" + String(WiFi.RSSI()) + "\",";
	response += "\"mac\":\"" + WiFi.macAddress() + "\",";
	response += "\"phy\":\"" + _wifiPhyModeToString(WiFi.getPhyMode()) + "\",";
	response += "\"dhcp\":\"" + dhcp + "\",";
	response += "\"staticip\":\"" + staticAddress + "\",";
	response += "\"warn\": \"<a href='#' class='pure-button button-primary button-larger-margin' onclick='changeWifiMode(" + String((int)nextWiFiMode) + ")'>Switch to " + _wifiModeToString(nextWiFiMode) + " mode</a>\"";

	response += "}";

	_server->send(200, "text/plain", response);

}

// Handle a request of system informations .
void WebServer::_handleSystemInfo()
{
	String response;

	response += "{";
	response += "\"heap\":\"" + String(ESP.getFreeHeap() / 1024) + " KB\",";
	response += "\"id\":\"" + String(ESP.getFlashChipId()) + "\",";
	response += "\"size\":\"" + String(ESP.getFlashChipSize() / 1024 / 1024) + " MB\"";
	response += "}";

	_server->send(200, "text/plain", response);
}

// Handle system name update .
void WebServer::_handleSystemNameUpdate()
{
	String newHostname = _server->arg("name");

	if (newHostname.length() <= 32)
	{
		if (WiFiCommon.setHostname(newHostname) == true)
		{
			MDNS.begin(newHostname.c_str());
			MDNS.setInstanceName(newHostname);

			_server->send(200, "text/plain", newHostname);
		}
		else
		{
			_server->send(500, "text/plain", "Internal Server Error");
		}
	}
	else
	{
		_server->send(400, "text/plain", "Bad Request");
	}
}

// Handle a request of number of scanned networks .
void WebServer::_handleNumberOfNetworks()
{
	int8_t scanResult;

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.print("handleNumberOfNetworks: ... ");

#endif // WIFI_CONN_DEBUG_SERVER

	if (_firstNetworksScan == true || (millis() - _scanNetworksTime) > 15000)
	{
		_scanNetworksTime = millis();
		_firstNetworksScan = false;

		scanResult = WiFi.scanNetworks();

		//delay(100);
	}
	else
	{
		scanResult = WiFi.scanComplete();
	}

	if (scanResult < 0)
	{

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println("Error during scanning");

#endif // WIFI_CONN_DEBUG_SERVER

		_server->send(500, "text/plain", "Error during scanning");
	}
	else
	{

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println(String(scanResult) + " networks found");

#endif // WIFI_CONN_DEBUG_SERVER

		_server->send(200, "text/plain", String(scanResult));
	}
}

// Handle WiFi networks scanning .
void WebServer::_handleNetworkScanning()
{
	int8_t scanResult;

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.print("handleNetworkScanning: ... ");

#endif // WIFI_CONN_DEBUG_SERVER

	if (_firstNetworksScan == true || (millis() - _scanNetworksTime) > 15000)
	{
		_scanNetworksTime = millis();
		_firstNetworksScan = false;

		scanResult = WiFi.scanNetworks();

		//delay(100);
	}
	else
	{
		scanResult = WiFi.scanComplete();
	}

	if (scanResult < 0)
	{

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println("Error during scanning");

#endif // WIFI_CONN_DEBUG_SERVER

		_server->send(500, "text/plain", "Error during scanning");
	}
	else if (scanResult == 0)
	{

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println("No networks found");

#endif // WIFI_CONN_DEBUG_SERVER

		_server->send(200, "text/plain", "No networks found");
	}
	else
	{

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println(String(scanResult) + " networks found");

#endif // WIFI_CONN_DEBUG_SERVER

		String response;

		response += "{\"result\": { \"APs\" : [ ";

		for (int8_t netIndex = 0; netIndex < scanResult; netIndex++)
		{
			response += "{\"enc\" : \"";
			response += _encryptionTypeToString(WiFi.encryptionType(netIndex));
			response += "\",";
			response += "\"essid\":\"";
			response += WiFi.SSID(netIndex);
			response += "\",";
			response += "\"rssi\" :\"";
			response += String((WiFi.RSSI(netIndex)));
			response += "\"}";

			if (netIndex != (scanResult - 1))
			{
				response += ",";
			}
		}

		response += "]}}";

		_server->send(200, "text/plain", response);

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.print(" - response: ");
		Serial.println(response);

#endif // WIFI_CONN_DEBUG_SERVER

	}
}

// Handle connection .
void WebServer::_handleStartConnection()
{
	// Retrieve the connection parameters .
	_connectionSSID = _server->arg("essid");
	_connectionPassphrase = _server->arg("passwd");

	// Start a new connection .
	WiFi.begin(_connectionSSID.c_str(), _connectionPassphrase.c_str());

	//delay(100);

	// Send the response .
	_server->send(200, "text/plain", "1");

	// Set the connection state flag .
	_connectionInProgress = true;
}

// Handle connection status .
void WebServer::_handleConnectionStatus()
{
	String response;
	String ipAddress;

	WiFiMode_t wifiMode = WiFi.getMode();
	wl_status_t status = WiFi.status();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.print("handleConnectionStatus: ... ");

#endif // WIFI_CONN_DEBUG_SERVER

	if (wifiMode == WiFiMode::WIFI_STA || wifiMode == WiFiMode::WIFI_AP_STA )
	{
		ipAddress = WiFi.localIP().toString();
	}
	else
	{
		ipAddress = WiFi.softAPIP().toString();
	}

	response += "{";

	response += "\"url\":\"got IP address\",";
	response += "\"ip\":\"" + ipAddress +"\",";
	response += "\"modechange\":\"no\",";
	response += "\"ssid\":\""+ WiFi.SSID() + "\",";
	response += "\"reason\":\"-\",";
	response += "\"status\":\""+ _wifiStatusToString(status) + "\"";

	response += "}";

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(_wifiStatusToString(status));

#endif // WIFI_CONN_DEBUG_SERVER

	if (_connectionInProgress == true && WiFi.status() == WL_CONNECTED)
	{
		if (WiFi.SSID() == _connectionSSID)
		{
			// Save Access Points credentials .
			WiFiCommon.addAccessPoint(_connectionSSID, _connectionPassphrase);
		}

		_connectionInProgress = false;
	}

	_server->send(200, "text/plain", response);
}

// Handle IP configuration of the station .
void WebServer::_handleIPConfig()
{
	String dhcpParam = _server->arg("dhcp");

	if (dhcpParam == "off")
	{
		bool success = true;

		IPAddress localIP;
		IPAddress subnet;
		IPAddress gateway;

		// Convert the input argument in IP addresses .
		if (localIP.fromString(_server->arg("staticip")) == false)
		{
			success = false;
		}
		if (subnet.fromString(_server->arg("netmask")) == false)
		{
			success = false;
		}
		if (gateway.fromString(_server->arg("gateway")) == false)
		{
			success = false;
		}

		if (success == false)
		{
			localIP = WiFi.localIP();
		}

		_server->send(200, "text/plain", String("{\"url\":\"" + localIP.toString() + "\"}"));

		if (success == true)
		{


			// Configure the IP addresses of the station, the Access Point configuration remains unchanged .
			WiFi.config(localIP, gateway, subnet);

			_dhcpState = false;
		}
	}
	else
	{
		_server->send(200, "text/plain",  "1");

		// Configure the IP addresses of the station, the Access Point configuration remains unchanged .
		WiFi.config(0u, 0u, 0u);

		_dhcpState = true;
	}
}

// Handle board info JSON file .
void WebServer::_handleBoardInfo()
{
	String response;

	response += "{";

	response += "\"fw_name\":\"" + String(WIFI_CONN_FW_NAME) + "\",";
	response += "\"fw_version\":\"" + String(WIFI_CONN_FW_VERSION) +"\",";
	response += "\"build_date\":\"" + String(WIFI_CONN_FW_BUILD_DATE) +"\"";

	response += "}";

	_server->send(200, "text/plain", response);
}

// Handle a request of free heap .
void WebServer::_handleFreeHeap()
{
	_server->send(200, "text/plain", String(ESP.getFreeHeap()));
}

// Handle a request of sketch name .
void WebServer::_handleSketchName()
{
	_server->send(200, "text/plain", String("{\"sketch\":\"" + WiFiCommon.getRunningSketch() + "\"}"));
}

// Handle file not found .
void WebServer::_handleFileNotFound()
{
	if (_handleFileRead(_server->uri()) == false)
	{
		_server->send(404, "text/plain", "File Not Found");
	}
}

// Handle a request to read a file
bool WebServer::_handleFileRead(String path)
{
	if (path.endsWith("/") == true)
	{
		path += "index.html";
	}

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.print("handleFileRead: " + path);
	Serial.print(" ... ");

#endif // WIFI_CONN_DEBUG_SERVER

	if (SPIFFS.exists(path) == true)
	{
		String contentType = _getContentType(path);

		File file = SPIFFS.open(path, "r");

		_server->streamFile(file, contentType);

		file.close();

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println("OK");

#endif // WIFI_CONN_DEBUG_SERVER

		return true;
	}

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println("File not found");

#endif // WIFI_CONN_DEBUG_SERVER

	return false;
}

// Return the type of content of the file .
String WebServer::_getContentType(String filename)
{
	if(_server->hasArg("download"))
	{
		return "application/octet-stream";
	}
	else if(filename.endsWith(".htm"))
	{
		return "text/html";
	}
	else if(filename.endsWith(".html"))
	{
		return "text/html";
	}
	else if(filename.endsWith(".css"))
	{
		return "text/css";
	}
	else if(filename.endsWith(".js"))
	{
		return "application/javascript";
	}
	else if(filename.endsWith(".png"))
	{
		return "image/png";
	}
	else if(filename.endsWith(".gif"))
	{
		return "image/gif";
	}
	else if(filename.endsWith(".jpg"))
	{
		return "image/jpeg";
	}
	else if(filename.endsWith(".ico"))
	{
		return "image/x-icon";
	}
	else if(filename.endsWith(".xml"))
	{
		return "text/xml";
	}
	else if(filename.endsWith(".pdf"))
	{
		return "application/x-pdf";
	}
	else if(filename.endsWith(".zip"))
	{
		return "application/x-zip";
	}
	else if(filename.endsWith(".gz"))
	{
		return "application/x-gzip";
	}
	return "text/plain";
}

// Convert a WiFi mode into a string .
String WebServer::_wifiModeToString(WiFiMode mode)
{
	switch (mode)
	{
		case WiFiMode::WIFI_OFF:
		{
			return "OFF";
		}
		case WiFiMode::WIFI_STA:
		{
			return "STA";
		}
		case WiFiMode::WIFI_AP:
		{
			return "AP";
		}
		case WiFiMode::WIFI_AP_STA:
		{
			return "AP+STA";
		}
		default:
		{
			return  "----";
		}
	}
}

// Convert a WiFi status into a string .
String WebServer::_wifiStatusToString(wl_status_t status)
{
	switch (status)
	{
		case WL_IDLE_STATUS:
		{
			return "idle";
		}
		case WL_NO_SSID_AVAIL:
		{
			return "no ssid available";
		}
		case WL_SCAN_COMPLETED:
		{
			return "scan completed";
		}
		case WL_CONNECTED:
		{
			return "connected";
		}
		case WL_CONNECT_FAILED:
		{
			return "connection failed";
		}
		case WL_CONNECTION_LOST:
		{
			return "connection lost";
		}
		case WL_DISCONNECTED:
		{
			return "disconnected";
		}
		default:
		{
			return "----";
		}
	}
}

// Convert a WiFi Phy mode into a string .
String WebServer::_wifiPhyModeToString(WiFiPhyMode_t phyMode)
{
	switch (phyMode)
	{
		case  WIFI_PHY_MODE_11B:
		{
			return "11B";
		}
		case WIFI_PHY_MODE_11G:
		{
			return "11G";
		}
		case WIFI_PHY_MODE_11N:
		{
			return "11N";
		}
		default:
		{
			return "---";
		}
	}
}

// Convert an encryption type into a string .
String WebServer::_encryptionTypeToString(uint8_t encryptionType)
{
	switch (encryptionType)
	{
		case ENC_TYPE_WEP:
		{
			return "WEP";
		}
		case ENC_TYPE_TKIP:
		{
			return "WPA";
		}
		case ENC_TYPE_CCMP:
		{
			return "WPA2";
		}
		case ENC_TYPE_NONE:
		{
			return "None";
		}
		case ENC_TYPE_AUTO:
		{
			return "Auto";
		}
		default:
		{
			return "unknown";
		}
	}
}

/******************************************************************************/

