/*
 * WiFiConnConfig.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_CONFIG_H_
#define WIFI_CONN_CONFIG_H_

// Device name .
#define WIFI_CONN_DEVICE_NAME					"Whatsnext-RED"

// Firmware name, version, build date .
#define WIFI_CONN_FW_NAME						"WiFiConn"

#define WIFI_CONN_FW_VERSION					"3.0.0"
#define WIFI_CONN_FW_VERSION_MAJOR				3
#define WIFI_CONN_FW_VERSION_MINOR				0
#define WIFI_CONN_FW_VERSION_PATCH				0

#define WIFI_CONN_FW_BUILD_DATE 				"20180930"

#define WIFI_CONN_SOFT_AP_IPADDR				"192.168.240.1"

// Pin definitions .
#define WIFI_CONN_SLAVE_SELECT					5
#define WIFI_CONN_SLAVE_READY					16
#define WIFI_CONN_LED							15

// Debug definitions .
#define WIFI_CONN_DEBUG_ESPI					1
#define WIFI_CONN_DEBUG_SERVER					1
#define WIFI_CONN_DEBUG_DFU						1

#endif /* WIFI_CONN_CONFIG_H_ */
