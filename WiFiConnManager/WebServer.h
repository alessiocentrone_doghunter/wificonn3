/*
 * WebServer.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created on 04 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WEB_SERVER_H_
#define WEB_SERVER_H_

#include <ESP8266WebServer.h>

// Class to manage the WEB interface in Access Point mode .
class WebServer
{
public:

	// Constructor .
	WebServer();

	// Destructor .
	virtual ~WebServer();

	// Initialize the server and the hw/sw resources .
	bool begin(IPAddress ipAddress, uint16_t port);

	// Release the hw/sw resources .
	void end();

	// Handle the incoming HTTP requests .
	void handle();

private:

	// State of the service .
	bool _isRunning;

	// Web server .
	ESP8266WebServer *_server;

	// Number of available WiFi networks .
	int _numberOfNetworks;

	// Flag that indicate the no previous scan has been performed .
	bool _firstNetworksScan;

	// Time in which the scan of networks is started .
	uint32_t _scanNetworksTime;

	// Connection state flag .
	bool _connectionInProgress;

	// Connection parameters .
	String _connectionSSID;
	String _connectionPassphrase;

	// DHCP status flag .
	bool _dhcpState;


	// Initialize web page handlers  .
	void _initializeHandlers();

	// Handle a request of WiFi informations .
	void _handleWiFiInfo();

	// Handle a request of system informations .
	void _handleSystemInfo();

	// Handle system name update .
	void _handleSystemNameUpdate();

	// Handle a request of number of scanned networks .
	void _handleNumberOfNetworks();

	// Handle WiFi networks scanning .
	void _handleNetworkScanning();

	// Handle start connection .
	void _handleStartConnection();

	// Handle connection status .
	void _handleConnectionStatus();

	// Handle IP configuration of the station .
	void _handleIPConfig();

	// Handle board info JSON file .
	void _handleBoardInfo();

	// Handle a request of free heap .
	void _handleFreeHeap();

	// Handle a request of sketch name .
	void _handleSketchName();

	// Handle file not found .
	void _handleFileNotFound();

	// Handle a request to read a file
	bool _handleFileRead(String path);

	// Return the type of content of the file .
	String _getContentType(String filename);

	// Convert a WiFi mode into a string .
	static String _wifiModeToString(WiFiMode mode);

	// Convert a WiFi status into a string .
	static String _wifiStatusToString(wl_status_t status);

	// Convert a WiFi Phy mode into a string .
	static String _wifiPhyModeToString(WiFiPhyMode_t phyMode);

	// Convert an encryption type into a string .
	static String _encryptionTypeToString(uint8_t encryptionType);

};

#endif /* WEB_SERVER_H_ */
