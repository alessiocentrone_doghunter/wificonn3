/*
 * WiFiConnServer.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_SERVER_H_
#define WIFI_CONN_SERVER_H_

// --- Includes .
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include "Arduino.h"
#include "EspiSlave.h"
#include "WiFiConnConfig.h"

class WiFiConnServer
{
public:

	// Constructor .
	WiFiConnServer();

	// Destructor .
	virtual ~WiFiConnServer();

	// Initialize the server and the hw/sw resources .
	bool begin();

	// Release the hw/sw resources .
	void end();

	// Handle the incoming requests .
	void handle();

private:

	// Maximum available sockets .
	enum { MAX_SOCK_NUMBER = 4 };

	// Flag added to the command code to indicate an answer .
	enum { REPLAY_CODE_FLAG = 0x80 };

	// Size of MAC address in bytes .
	enum { MAC_ADDR_LENGTH = 6 };

	// List of supported command codes .
	enum CommandCodes
	{
		CONNECT_CMD = 0x01,
		DISCONNECT_CMD,

		GET_CONN_STATUS_CMD,
		GET_FW_VERSION_CMD,

		SET_IP_CONFIG_CMD,
		SET_DNS_CONFIG_CMD,
		SET_HOSTNAME_CMD,

		GET_LOCAL_IPADDR_CMD,
		GET_GATEWAY_IPADDR_CMD,
		GET_SUBNET_MASK_CMD,
		GET_MACADDR_CMD,
		GET_DNS_IPADDR_CMD,
		GET_HOSTNAME_CMD,
		GET_HOST_BY_NAME_CMD,

		GET_CURR_SSID_CMD,
		GET_CURR_ENCT_CMD,
		GET_CURR_RSSI_CMD,
		GET_CURR_BSSID_CMD,
		GET_CURR_CHANNEL_CMD,
		GET_CURR_NET_INFO_CMD,

		SCAN_NETWORKS_CMD,

		GET_IDX_SSID_CMD,
		GET_IDX_ENCT_CMD,
		GET_IDX_RSSI_CMD,
		GET_IDX_BSSID_CMD,
		GET_IDX_CHANNEL_CMD,
		GET_IDX_NET_INFO_CMD,

		START_SERVER_CMD,
		STOP_SERVER_CMD,
		GET_SERVER_STATE_CMD,
		GET_SERVER_CLIENT_CMD,

		START_CLIENT_CMD,
		STOP_CLIENT_CMD,
		GET_CLIENT_STATE_CMD,
		GET_CLIENT_AVAIL_DATA_CMD,
		GET_CLIENT_STATE_INFO_CMD,
		PEEK_CLIENT_DATA_CMD,
		READ_CLIENT_DATA_CMD,
		READ_CLIENT_BUFF_CMD,
		WRITE_CLIENT_BUFF_CMD,
		EMPTY_FLUSH_CLIENT_BUFF_CMD,

		START_UDP_CMD,
		STOP_UDP_CMD,
		BEGIN_UDP_PKT_CMD,
		END_UDP_PKT_CMD,
		PARSE_UDP_PKT_CMD,
		GET_UDP_AVAIL_DATA_CMD,
		READ_UDP_BUFF_CMD,
		WRITE_UDP_BUFF_CMD,
		EMPTY_FLUSH_UDP_BUFF_CMD,
		GET_UDP_REMOTE_INFO_CMD,
		GET_UDP_LOCAL_INFO_CMD,

		GET_NUM_STORED_AP_CMD,
		GET_STORED_SSID_CMD,
		CLEAR_STORED_AP_CMD,

		SET_SKETCH_NAME_CMD,

		UNKNOWN_CMD	= 0xff,
	};

	// Encryption modes .
	enum EncryptionTypes
	{
		/* Values map to 802.11 encryption suites... */
		ENC_TYPE_WEP  = 5,
		ENC_TYPE_TKIP = 2,
		ENC_TYPE_CCMP = 4,
		/* ... except these two, 7 and 8 are reserved in 802.11-2007 */
		ENC_TYPE_NONE = 7,
		ENC_TYPE_AUTO = 8,
		ENC_TYPE_UNKNOWN = 255
	};

	// Dimension of the communication buffers .
	enum BufferSizes
	{
		REQ_BUFFER_SIZE = 128,
		ANS_BUFFER_SIZE = 128,
		EVT_BUFFER_SIZE = 64
	};

	// Parameters to connect to the access point .
	struct AccessPointParams
	{
		uint16_t marker;
		int8_t age;
		char ssid[32 + 1];
		char passphrase[64 + 1];
	};

	// Command entry structure .
	struct CommandEntry
	{
		// Command code .
		CommandCodes code;

		// Command handler .
		bool (WiFiConnServer::*handler)(EspiPacket *, EspiPacket *);
	};

	// Command table .
	static const CommandEntry _commandEntryTable[];

	// Indicate the state of use of the resources .
	bool _isDeployed;

	// Buffers used by the packets .
	uint8_t _requestBuffer[REQ_BUFFER_SIZE];
	uint8_t _answerBuffer[ANS_BUFFER_SIZE];
	uint8_t _eventBuffer[EVT_BUFFER_SIZE];

	// Packets .
	EspiPacket _requestPacket;
	EspiPacket _answerPacket;
	EspiPacket _eventPacket;

	// ESP slave interface .
	EspiSlave _espiSlave;

	// TCP server socket vector .
	WiFiServer *_tcpServerSocket[MAX_SOCK_NUMBER];

	// TCP client socket vector .
	WiFiClient *_tcpClientSocket[MAX_SOCK_NUMBER];

	// UDP socket vector .
	WiFiUDP *_udpSocket[MAX_SOCK_NUMBER];

	// Led blink timing .
	uint32_t _ledMillis;
	uint32_t _ledState;
	uint32_t _ledOnTime;
	uint32_t _ledOffTime;

	// Manage signaling led .
	void _handleSignalingLed();

	// Manage the incoming request and send the answer .
	bool _commandHandler(EspiPacket *request, EspiPacket *answer);

	// --- Command handler methods .

	// Connect to a network .
	bool _execCmd_Connect(EspiPacket *request, EspiPacket *answer);

	// Disconnect from the network .
	bool _execCmd_Disconnect(EspiPacket *request, EspiPacket *answer);


	// Get connection status .
	bool _execCmd_GetConnectionStatus(EspiPacket *request, EspiPacket *answer);

	// Get firmware version .
	bool _execCmd_GetFirmwareVersion(EspiPacket *request, EspiPacket *answer);


	// Change IP configuration settings disabling the DHCP client .
	bool _execCmd_SetIPConfig(EspiPacket *request, EspiPacket *answer);

	// Change DNS IP configuration .
	bool _execCmd_SetDns(EspiPacket *request, EspiPacket *answer);

	// Set the DHCP hostname .
	bool _execCmd_SetHostname(EspiPacket *request, EspiPacket *answer);


 	// Get the interface IP address .
	bool _execCmd_GetLocalIP(EspiPacket *request, EspiPacket *answer);

	// Get the gateway IP address .
	bool _execCmd_GetGatewayIP(EspiPacket *request, EspiPacket *answer);

	// Get the interface sub-net mask address .
	bool _execCmd_GetSubnetMask(EspiPacket *request, EspiPacket *answer);

	// Get the interface MAC address .
	bool _execCmd_GetMacAddress(EspiPacket *request, EspiPacket *answer);

	// Get the DNS IP address .
	bool _execCmd_GetDnsIP(EspiPacket *request, EspiPacket *answer);

	// Get the DHCP hostname .
	bool _execCmd_GetHostname(EspiPacket *request, EspiPacket *answer);

	// Resolve the given hostname to an IP address .
	bool _execCmd_GetHostByName(EspiPacket *request, EspiPacket *answer);


	// Return the current SSID (Service Set Identifier) associated with the network .
	bool _execCmd_GetCurrentSSID(EspiPacket *request, EspiPacket *answer);

	// Return the Encryption Type associated with the network .
	bool _execCmd_GetCurrentEncryptionType(EspiPacket *request, EspiPacket *answer);

	// Return the current RSSI (Received Signal Strength) in dBm .
	bool _execCmd_GetCurrentRSSI(EspiPacket *request, EspiPacket *answer);

	// Return the current BSSID (Basic Service Set Identifier) associated with the network .
	bool _execCmd_GetCurrentBSSID(EspiPacket *request, EspiPacket *answer);

	// Return the current channel associated with the network .
	bool _execCmd_GetCurrentChannel(EspiPacket *request, EspiPacket *answer);

	// Return all informations associated with the network .
	bool _execCmd_GetCurrentNetworkInfo(EspiPacket *request, EspiPacket *answer);


	// Scan available WiFi networks .
	bool _execCmd_ScanNetworks(EspiPacket *request, EspiPacket *answer);


	// Return the SSID discovered during the network scan .
	bool _execCmd_GetSSID(EspiPacket *request, EspiPacket *answer);

	// Return the encryption type of the networks discovered during the scanNetworks .
	bool _execCmd_GetEncryptionType(EspiPacket *request, EspiPacket *answer);

	// Return the RSSI of the networks discovered during the scanNetworks .
	bool _execCmd_GetRSSI(EspiPacket *request, EspiPacket *answer);

	// Return the BSSID of the networks discovered during the scanNetworks .
	bool _execCmd_GetBSSID(EspiPacket *request, EspiPacket *answer);

	// Return the channel of the networks discovered during the scanNetworks .
	bool _execCmd_GetChannel(EspiPacket *request, EspiPacket *answer);

	// Return all informations about a scanned WiFi network .
	bool _execCmd_GetNetworkInfo(EspiPacket *request, EspiPacket *answer);


	// Start a TCP server on a specified port .
	bool _execCmd_StartServer(EspiPacket *request, EspiPacket *answer);

	// Stop a TCP server .
	bool _execCmd_StopServer(EspiPacket *request, EspiPacket *answer);

	// Get TCP server state .
	bool _execCmd_GetServerState(EspiPacket *request, EspiPacket *answer);

	// Get a new client from the server connection .
	bool _execCmd_GetServerClient(EspiPacket *request, EspiPacket *answer);


	// Start a TCP client on a specified port .
	bool _execCmd_StartClient(EspiPacket *request, EspiPacket *answer);

	// Stop a TCP client .
	bool _execCmd_StopClient(EspiPacket *request, EspiPacket *answer);

	// Get TCP client state .
	bool _execCmd_GetClientState(EspiPacket *request, EspiPacket *answer);

	// Get TCP client available data .
	bool _execCmd_GetClientAvailableData(EspiPacket *request, EspiPacket *answer);

	// Get client connection state and informations .
	bool _execCmd_GetClientStateInfo(EspiPacket *request, EspiPacket *answer);

	// Peek client data .
	bool _execCmd_PeekClient(EspiPacket *request, EspiPacket *answer);

	// Read client data .
	bool _execCmd_ReadClient(EspiPacket *request, EspiPacket *answer);

	// Read client buffer .
	bool _execCmd_ReadClientBuffer(EspiPacket *request, EspiPacket *answer);

	// Write client buffer .
	bool _execCmd_WriteClientBuffer(EspiPacket *request, EspiPacket *answer);

	// Empty/Flush client buffer .
	bool _execCmd_EmptyFlushClientBuffer(EspiPacket *request, EspiPacket *answer);


	// Start a UDP connection on a specified port .
	bool _execCmd_StartUdp(EspiPacket *request, EspiPacket *answer);

	// Stop a UDP connection .
	bool _execCmd_StopUdp(EspiPacket *request, EspiPacket *answer);

	// Start building up an UDP packet .
	bool _execCmd_BeginUdpPacket(EspiPacket *request, EspiPacket *answer);

	// Finish off the packet and send it .
	bool _execCmd_EndUdpPacket(EspiPacket *request, EspiPacket *answer);

	// Parse incoming UDP packet and return the size of the packet in bytes .
	bool _execCmd_ParseUdpPacket(EspiPacket *request, EspiPacket *answer);

	// Get UDP available data .
	bool _execCmd_GetUdpAvailableData(EspiPacket *request, EspiPacket *answer);

	// Read UDP buffer .
	bool _execCmd_ReadUdpBuffer(EspiPacket *request, EspiPacket *answer);

	// Write UDP buffer .
	bool _execCmd_WriteUdpBuffer(EspiPacket *request, EspiPacket *answer);

	// Empty/Flush UDP packet buffers .
	bool _execCmd_EmptyFlushUdpBuffer(EspiPacket *request, EspiPacket *answer);

	// Retrieve remote IP address and port .
	bool _execCmd_GetUdpRemoteInfo(EspiPacket *request, EspiPacket *answer);

	// Retrieve local IP address and port .
	bool _execCmd_GetUdpLocalInfo(EspiPacket *request, EspiPacket *answer);


	// Return the number of Access Points stored in the non-volatile memory .
	bool _execCmd_GetNumberOfStoredAccessPoints(EspiPacket *request, EspiPacket *answer);

	// Return the SSID related to a stored Access Point .
	bool _execCmd_GetStoredSSID(EspiPacket *request, EspiPacket *answer);

	// Clear all Access Points stored in the non-volatile memory .
	bool _execCmd_ClearStoredAccessPoints(EspiPacket *request, EspiPacket *answer);


	// Inform the WiFi module about the current running sketch .
	bool _execCmd_SetSketchName(EspiPacket *request, EspiPacket *answer);


#ifdef WIFI_CONN_DEBUG_ESPI

	// Print error message when it occurs .
	void _errorHandler(uint32_t errorCode);

#endif // WIFI_CONN_DEBUG_ESPI

};

#endif /* WIFI_CONN_SERVER_H_ */
