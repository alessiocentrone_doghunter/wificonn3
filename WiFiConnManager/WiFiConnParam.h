/*
 * WiFiConnParam.h
 *
 * Dog Hunter. All right reserved.
 *
 * Created on 04 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_PARAMS_H_
#define WIFI_CONN_PARAMS_H_

#include "Arduino.h"

/*******************************************************************************
    Class declaration .
*******************************************************************************/

// Class that implements a common non-volatile parameters system .
class WiFiConnParam
{
public:

	// Constructor .
	WiFiConnParam();

	// Destructor .
	virtual ~WiFiConnParam();


	// Allocate resources and load parameters from non-volatile memory .
	bool begin();

	// Save parameters into non-volatile memory (if necessary) and release resources .
	void end();

	// Indicate the state of use of the resources .
	bool isDeployed() { return (_isDeployed > 0); }


	// Get the host name .
	String getHostname();

	// Set the host name .
	bool setHostname(String hostname);

	// Reset the hostname .
	bool resetHostname();


	// Set the name of the currently running sketch .
	void setRunningSketch(String sketchName);

	// Get the name of the currently running sketch .
	String getRunningSketch();


	// Return the number of Access Points stored in non-volatile memory .
	uint8_t getNumberOfAccessPoints();

	// Return the SSID of a selected Access Points stored in non-volatile memory .
	String getAccessPointSSID(uint8_t accessPointItem);

	// Return the pass-phrase of a selected Access Points stored in non volatile memory .
	String getAccessPointPassphrase(uint8_t accessPointItem);

	// Find best available Access Point to connect to .
	bool findBestAvailableAccessPoint(String &ssid, String &passphrase);

	// Add a new Access Point to the vector .
	bool addAccessPoint(String ssid, String passphrase);

	// Clear all Access Points .
	bool clearAllAccessPoints();

private:

	// Maximum number of Access Point used for auto-connection .
	enum { MAX_ACCESS_POINT_NUMBER = 8 };

	// Host related informations .
	struct HostInfo
	{
		uint16_t marker;
		char name[32 + 1];
	};

	// Parameters to connect to the access point .
	struct AccessPointInfo
	{
		uint16_t marker;
		int8_t age;
		char ssid[32 + 1];
		char passphrase[64 + 1];
	};

	// Common parameters .
	struct ParamInfo
	{
		// Host name .
		HostInfo host;

		// Vector of access point parameters .
		AccessPointInfo accessPointVector[MAX_ACCESS_POINT_NUMBER];

	} _params;

	// Indicate the state of use of the resources .
	uint32_t _isDeployed;

	// Name of the sketch currently running .
	String _skecthName;

	// Load all parameters from non-volatile memory .
	void _load();

	// Save all parameters in non-volatile memory .
	void _save();

};

// --- Class instantiation .
extern WiFiConnParam WiFiCommon;

// --- End of file .

#endif /* WIFI_CONN_PARAMS_H_ */
