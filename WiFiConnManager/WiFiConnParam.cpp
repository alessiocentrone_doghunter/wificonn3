/*
 * WiFiConnParam.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created on 04 set 2018 by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConnParam.h"

#include "limits.h"

#include <EEPROM.h>
#include <ESP8266WiFi.h>

#include "WiFiConnConfig.h"

/*******************************************************************************
    Class instantiation .
*******************************************************************************/

// Class instance .
WiFiConnParam WiFiCommon;

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/

// Constructor .
WiFiConnParam::WiFiConnParam() :
	_isDeployed(0)
{

}

// Destructor .
WiFiConnParam::~WiFiConnParam()
{
	end();
}

// Allocate resources and load parameters from non-volatile memory .
bool WiFiConnParam::begin()
{
	if (_isDeployed == 0)
	{
		EEPROM.begin(1024);

		// Load parameters from non-volatile memory .
		_load();
	}

	_isDeployed++;

	return true;
}

// Save parameters into non-volatile memory (if necessary) and release resources .
void WiFiConnParam::end()
{
	if (_isDeployed > 0)
	{
		_isDeployed--;

		if (_isDeployed == 0)
		{
			EEPROM.end();
		}
	}
}

// Get the host name .
String WiFiConnParam::getHostname()
{
	if (_isDeployed > 0)
	{
		return _params.host.name;
	}

	return String();
}

// Set the host name .
bool WiFiConnParam::setHostname(String hostname)
{
	if (_isDeployed > 0 && hostname.length() <= 32)
	{
		if (hostname != _params.host.name)
		{
			// Set the new host name .
			if (WiFi.hostname(hostname.c_str()) == true)
			{
				strcpy(_params.host.name, hostname.c_str());

				// Update the non-volatile memory .
				_save();

				return true;
			}
		}
	}

	return false;
}

// Reset the hostname .
bool WiFiConnParam::resetHostname()
{
	if (_isDeployed > 0)
	{
		uint8_t mac[6] = { 0, 0, 0, 0, 0, 0 };

		// Read MAC address .
		WiFi.macAddress(mac);

		_params.host.marker = 0xC1A0;

		// Initialize the name array .
		memset(_params.host.name, 0, 32 + 1);

		// Write a default host name .
		sprintf(_params.host.name, "%s_%02x%02x%02x", WIFI_CONN_DEVICE_NAME, mac[3], mac[4], mac[5]);

		// Set the default host name .
		WiFi.hostname(_params.host.name);

		_save();

		return true;
	}

	return false;
}

// Set the name of the currently running sketch .
void WiFiConnParam::setRunningSketch(String sketchName)
{
	_skecthName = sketchName;
}

// Get the name of the currently running sketch .
String WiFiConnParam::getRunningSketch()
{
	return _skecthName;
}

// Return the number of Access Points stored in non-volatile memory .
uint8_t WiFiConnParam::getNumberOfAccessPoints()
{
	uint8_t count = 0;

	if (_isDeployed > 0)
	{
		// Check the number of elements present in the Access Point vector .
		for(int i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
		{
			if (_params.accessPointVector[i].age >= 0)
			{
				count++;
			}
		}
	}

	return count;
}

// Return the SSID of a selected Access Points stored in non-volatile memory .
String WiFiConnParam::getAccessPointSSID(uint8_t accessPointItem)
{
	String ssid;

	if (_isDeployed > 0)
	{
		uint8_t count = 0;

		// Check the number of elements present in the Access Point vector .
		for(int i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
		{
			if (_params.accessPointVector[i].age >= 0)
			{
				if (count == accessPointItem)
				{
					ssid = _params.accessPointVector[i].ssid;

					break;
				}

				count++;
			}
		}
	}

	return ssid;
}

// Return the pass-phrase of a selected Access Points stored in non volatile memory .
String WiFiConnParam::getAccessPointPassphrase(uint8_t accessPointItem)
{
	String passphrase;

	if (_isDeployed > 0)
	{
		uint8_t count = 0;

		// Check the number of elements present in the Access Point vector .
		for(int i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
		{
			if (_params.accessPointVector[i].age >= 0)
			{
				if (count == accessPointItem)
				{
					passphrase = _params.accessPointVector[i].passphrase;

					break;
				}

				count++;
			}
		}
	}

	return passphrase;
}

// Find best available Access Point to connect to .
bool WiFiConnParam::findBestAvailableAccessPoint(String &ssid, String &passphrase)
{
	AccessPointInfo *bestAccessPoint = nullptr;

	if (_isDeployed > 0)
	{
		// Scan available networks .
		int8_t numberOfNetworks = WiFi.scanNetworks();

		if (numberOfNetworks > 0)
		{
			// Search the network with the highest signal strength .
			for (int8_t i = 0; i < numberOfNetworks; i++)
			{
				int bestNetworkRssi = INT_MIN;

				String ssid_scan;
				int32_t rssi;
				uint8_t encryption;
				uint8_t* bssid;
				int32_t channel;
				bool hidden;

				WiFi.getNetworkInfo(i, ssid_scan, encryption, rssi, bssid, channel, hidden);

				for (int j = 0; j < MAX_ACCESS_POINT_NUMBER; j++)
				{
					if (_params.accessPointVector[j].age >= 0)
					{
						if (ssid_scan == _params.accessPointVector[j].ssid)
						{
							if (rssi > bestNetworkRssi)
							{
								if (encryption == ENC_TYPE_NONE || strlen(_params.accessPointVector[j].passphrase) > 0)
								{
									bestNetworkRssi = rssi;

									bestAccessPoint = &_params.accessPointVector[j];
								}
							}
						}
					}
				}
			}
		}
	}

	if (bestAccessPoint != nullptr)
	{
		ssid = bestAccessPoint->ssid;
		passphrase = bestAccessPoint->passphrase;

		return true;
	}

	return false;
}

// Add a new Access Point to the vector .
bool WiFiConnParam::addAccessPoint(String ssid, String passphrase)
{
	if (_isDeployed == 0)
	{
		return false;
	}

	int i, index = -1, count = 0;
	int emptyIndex = -1;

	int olderIndex = -1, olderAge = -1;
	int newerIndex = -1, newerAge = 255;

	// Operation flags .
	bool updateVectorAge = false;
	bool copyNewElement = false;

	// Check the elements present in the Access Point vector .
	for(i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
	{
		if (_params.accessPointVector[i].age >= 0)
		{
			if (_params.accessPointVector[i].age > olderAge)
			{
				olderAge = _params.accessPointVector[i].age;
				olderIndex = i;
			}
			if (_params.accessPointVector[i].age < newerAge)
			{
				newerAge = _params.accessPointVector[i].age;
				newerIndex = i;
			}

			if (strcmp(ssid.c_str(), _params.accessPointVector[i].ssid) == 0)
			{
				index = i;
			}

			count++;
		}
		else
		{
			if (emptyIndex == - 1)
			{
				// Set the first empty element in the vector .
				emptyIndex = i;
			}
		}
	}

	if (count == 0)
	{
		// The vector is empty .

		index = emptyIndex;

		// Copy the new element in the first empty slot in the vector .
		copyNewElement = true;
	}
	else if (index != -1)
	{
		// The element is already present in the vector .

		// Check if it is necessary update the age of all element in the vector .
		if (newerIndex != index)
		{
			// Update the age of all element in the vector .
			updateVectorAge = true;
		}
	}
	else
	{
		// Check if the vector is full .
		if (count == MAX_ACCESS_POINT_NUMBER)
		{
			// The new element overwrite the oldest .
			index = olderIndex;
		}
		else
		{
			// The new element is written in the first empty element slot .
			index = emptyIndex;
		}

		// Update the age of all element in the vector .
		updateVectorAge = true;

		// Copy the element in the vector .
		copyNewElement = true;
	}

	// Update the age of all element in the vector .
	if (updateVectorAge == true)
	{
		// Update the age of all element in the vector .
		for(i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
		{
			if (_params.accessPointVector[i].age >= 0)
			{
				_params.accessPointVector[i].age++;
			}
		}

		_params.accessPointVector[index].age = 0;
	}

	// Copy the element in the vector .
	if (copyNewElement == true)
	{
		// Add the first element in the vector .
		strcpy(_params.accessPointVector[index].ssid, ssid.c_str());
		strcpy(_params.accessPointVector[index].passphrase, passphrase.c_str());
		_params.accessPointVector[index].age = 0;
	}

	// Check if vector has been updated and needs to be saved .
	if (updateVectorAge == true || copyNewElement == true)
	{
		// Update the non-volatile memory .
		_save();
	}

	return true;
}

// Clear all Access Points .
bool WiFiConnParam::clearAllAccessPoints()
{
	if (_isDeployed > 0)
	{
		for (int i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
		{
			// Initialize the element .
			_params.accessPointVector[i].marker = 0xABCD;
			_params.accessPointVector[i].age = -1;
			memset(_params.accessPointVector[i].ssid, 0, 32 + 1);
			memset(_params.accessPointVector[i].passphrase, 0, 64 + 1);
		}

		_save();

		return true;
	}

	return false;
}

/*******************************************************************************
    Private methods implementation .
*******************************************************************************/

// Load all parameters from non-volatile memory .
void WiFiConnParam::_load()
{
	uint32_t i;

	bool toSave = false;
	bool valid = true;

	uint8_t *data = (uint8_t *)&_params;

	for(i = 0; i < sizeof(_params); i++)
	{
		data[i] = EEPROM.read(i);
	}

	// Check the host name .
	if (_params.host.marker != 0xC1A0)
	{
		valid = false;
	}
	if (_params.host.name[32] != '\0')
	{
		valid = false;
	}

	if (valid == false)
	{
		uint8_t mac[6] = { 0, 0, 0, 0, 0, 0 };

		// Read MAC address .
		WiFi.macAddress(mac);

		_params.host.marker = 0xC1A0;

		// Initialize the name array .
		memset(_params.host.name, 0, 32 + 1);

		// Write a default host name .
		sprintf(_params.host.name, "%s_%02x%02x%02x", WIFI_CONN_DEVICE_NAME, mac[3], mac[4], mac[5]);

		toSave = true;
	}

	// Check the content of the vector .
	for (i = 0; i < MAX_ACCESS_POINT_NUMBER; i++)
	{
		valid = true;

		if (_params.accessPointVector[i].marker != 0xABCD)
		{
			valid = false;
		}
		if (_params.accessPointVector[i].age > MAX_ACCESS_POINT_NUMBER || _params.accessPointVector[i].age < -1)
		{
			valid = false;
		}
		if (_params.accessPointVector[i].ssid[32] != '\0' || _params.accessPointVector[i].passphrase[64] != '\0')
		{
			valid = false;
		}

		if (valid == false)
		{
			// Initialize the invalid element .
			_params.accessPointVector[i].marker = 0xABCD;
			_params.accessPointVector[i].age = -1;
			memset(_params.accessPointVector[i].ssid, 0, 32 + 1);
			memset(_params.accessPointVector[i].passphrase, 0, 64 + 1);

			toSave = true;
		}
	}

	// Set the host name .
	WiFi.hostname(_params.host.name);

	if (toSave == true)
	{
		_save();
	}
}

// Save all parameters into non-volatile memory .
void WiFiConnParam::_save()
{
	uint32_t i;

	uint8_t *data = (uint8_t *)&_params;

	for(i = 0; i < sizeof(_params); i++)
	{
		EEPROM.write(i, data[i]);
	}
	for (; i < 1024; i++)
	{
		EEPROM.write(i, 0xff);
	}

	EEPROM.commit();
}

/******************************************************************************/

