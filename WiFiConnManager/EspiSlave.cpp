/*
 * EspiSlave.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "Arduino.h"
#include "esp8266_peri.h"
#include "interrupts.h"

#include "EspiSlave.h"

// --- Defines .
#define SYNC_TIMEOUT				250		// 250 ms .
#define DATA_BLOCK_TIMEOUT			10		// 10 ms .

#define SR_TX_US_DELAY				100		// 100 us .
#define SR_RX_US_DELAY				3000	// 3000 us .
#define ANTI_COLLISION_US_DELAY		50		// 50 us .

// --- Static member declaration .
bool EspiSlave::_isDeployed = false;

// --- Class implementation .

// Constructor .
EspiSlave::EspiSlave(uint8_t slaveSelectPin, uint8_t slaveReadyPin, uint8_t ledPin) :
	_requestPacket(nullptr),
	_answerPacket(nullptr),
	_commandObject(nullptr),
	_commandMethod(nullptr),
	_commandFunction(nullptr),
	_errorObject(nullptr),
	_errorMethod(nullptr),
	_errorFunction(nullptr),
	_errorFlags(0),
	_ss_pin(slaveSelectPin),
	_sr_pin(slaveReadyPin),
	_ss_lockedLowCount(0),
	_led_pin(ledPin),
	_led_state(LOW)
{

}

// Destructor .
EspiSlave::~EspiSlave()
{
	end();
}

// Initialize the protocol and the hardware resources .
bool EspiSlave::begin()
{
	if (_isDeployed == false)
	{
		// Reset internal data .
		_requestPacket = nullptr;
		_answerPacket = nullptr;
		_commandObject = nullptr;
		_commandMethod = nullptr;
		_commandFunction = nullptr;
		_errorObject = nullptr;
		_errorMethod = nullptr;
		_errorFunction = nullptr;
		_errorFlags = 0;
		_ss_lockedLowCount = 0;

		// Initialize SPI in slave mode .
		_deploySpiSlave();

		// Configure SLAVE_READY as output .
		pinMode(_sr_pin, OUTPUT);
		digitalWrite(_sr_pin, LOW);

		if (_ss_pin != SS)
		{
			// Configure SLAVE_SELECT as input .
			pinMode(_ss_pin, INPUT);
		}

		if (_led_pin != 255)
		{
			pinMode(_led_pin, OUTPUT);
			digitalWrite(_led_pin, LOW);
		}

		_isDeployed = true;
	}

	return true;
}

// Release the hardware resources .
void EspiSlave::end()
{
	if (_isDeployed == true)
	{
		// Disable the SPI peripheral .
		_disposeSpiSlave();

		// Release all hardware resources .
		pinMode(_sr_pin, INPUT);
		pinMode(_ss_pin, INPUT);

		if (_led_pin != 255)
		{
			pinMode(_led_pin, INPUT);
		}

		// Reset internal data .
		_requestPacket = nullptr;
		_answerPacket = nullptr;
		_commandObject = nullptr;
		_commandMethod = nullptr;
		_commandFunction = nullptr;
		_errorObject = nullptr;
		_errorMethod = nullptr;
		_errorFunction = nullptr;
		_errorFlags = 0;
		_ss_lockedLowCount = 0;

		_isDeployed = false;
	}
}

// Set the command callback handler .
void EspiSlave::setCommandCallback(EspiCommandFunction commandFunction, EspiPacket *requestPacket, EspiPacket *answerPacket)
{
	_commandObject = nullptr;
	_commandMethod = nullptr;
	_commandFunction = commandFunction;

	_requestPacket = requestPacket;
	_answerPacket = answerPacket;
}

// Set the command callback handler .
void EspiSlave::setCommandCallback(void *commandObject, EspiCommandMethod commandMethod, EspiPacket *requestPacket, EspiPacket *answerPacket)
{
	_commandObject = reinterpret_cast<EspiSlave*>(commandObject);
	_commandMethod = commandMethod;
	_commandFunction = nullptr;

	_requestPacket = requestPacket;
	_answerPacket = answerPacket;
}

// Set the error callback handler .
void EspiSlave::setErrorCallback(EspiErrorFunction errorFunction)
{
	_errorObject = nullptr;
	_errorMethod = nullptr;
	_errorFunction = errorFunction;
}

// Set the error callback handler .
void EspiSlave::setErrorCallback(void *errorObject, EspiErrorMethod errorMethod)
{
	_errorObject = reinterpret_cast<EspiSlave*>(errorObject);
	_errorMethod = errorMethod;
	_errorFunction = nullptr;
}

// Handle the incoming requests .
void EspiSlave::handle()
{
	if (_isDeployed == true)
	{
		// Verify that the 'slave select' signal is active but not locked low .
		if (_checkSlaveSelect(LOW) == true && _ss_lockedLowCount < MAX_SS_LOW_TIMEOUT)
		{
			if (_requestPacket != nullptr)
			{
				// Try to receive the request packet .
				if (receivePacket(_requestPacket) == true)
				{
					bool result = false;

					if (_answerPacket != nullptr)
					{
						if (_commandObject != nullptr && _commandMethod != nullptr)
						{
							// Call error callback handler .
							result = (_commandObject->*_commandMethod)(_requestPacket, _answerPacket);
						}
						else if (_commandFunction != nullptr)
						{
							// Call error callback handler .
							result = _commandFunction(_requestPacket, _answerPacket);
						}
					}

					if (result == true)
					{
						// Try to send answer packet .
						sendPacket(_answerPacket);
					}
				}
			}
			else
			{
				// Discard the incoming data .
				_receiveData(nullptr, 0);
			}
		}
	}
}

// Send a packet eventually splitting it in multiple data blocks .
bool EspiSlave::sendPacket(EspiPacket *packet)
{
	bool success = false;

	if (_isDeployed == true && packet != nullptr)
	{
		// Check packet validity .
		if (packet->isValid() == true)
		{
			// Try to take the control of the bus .
			if (_startTransmission() == true)
			{
				uint16_t dataSent = 0;
				uint16_t dataToSend = 0;

				uint8_t *packetData = packet->buffer();
				uint16_t packetLength = packet->length();

				success = true;

				// Send the packet splitting it in blocks of 32 bytes .
				while(success == true && dataSent < packetLength)
				{
					dataToSend = packetLength - dataSent;

					if (dataToSend > ESP8266_SPI_MAX_PACKET_LEN)
					{
						dataToSend = ESP8266_SPI_MAX_PACKET_LEN;
					}

					if (_sendData(&packetData[dataSent], dataToSend) == true)
					{
						dataSent += dataToSend;
					}
					else
					{
						// Exit on a transmission error .
						success = false;
					}
				}
			}
		}
	}

	return success;
}

// Receive a packet eventually joining together multiple data blocks .
bool EspiSlave::receivePacket(EspiPacket *packet)
{
	bool success = false;

	// Check input parameters .
	if (_isDeployed == true && packet != nullptr)
	{
		// Check packet validity .
		if (packet->isValid() == true)
		{
			uint16_t dataReceived = 0;
			uint16_t dataToReceive = 32;

			uint8_t *packetData = packet->buffer();
			uint16_t packetLength = 0;

			// Receive the first data block .
			success = _receiveData(packetData, dataToReceive);

			if (success == true)
			{
				dataReceived += dataToReceive;

				// Try to decode header .
				if (packet->beginDecode() == true)
				{
					// Retrieve packet length .
					packetLength = packet->length();

					// Receive the packet joining together multiple data blocks .
					while(success == true && dataReceived < packetLength)
					{
						dataToReceive = packetLength - dataReceived;

						if (dataToReceive > ESP8266_SPI_MAX_PACKET_LEN)
						{
							dataToReceive = ESP8266_SPI_MAX_PACKET_LEN;
						}

						if (_receiveData(&packetData[dataReceived], dataToReceive) == true)
						{
							dataReceived += dataToReceive;
						}
						else
						{
							// Exit on receiving error .
							success = false;
						}
					}

					if (success == true)
					{
						if (packet->endDecode() == false)
						{
							// Report an invalid packet tail .
							_reportError(ERR_INVALID_PACKET_TAIL);

							success = false;
						}
					}
				}
				else
				{
					// Report an invalid packet header .
					_reportError(ERR_INVALID_PACKET_HEADER);

					success = false;
				}
			}
		}

		// Clear the packet if reception fails .
		if (success == false)
		{
			packet->clear();
		}
	}

	return success;
}

// Read (and eventually reset) error flags .
uint32_t EspiSlave::readErrors(bool reset)
{
	uint32_t errorFlags = _errorFlags;

	if (reset == true)
	{
		_errorFlags = 0;
	}

	return errorFlags;
}

// Drive the signaling led .
void EspiSlave::setLed(uint8_t state)
{
	if (_isDeployed == true)
	{
		if (_led_pin != 255)
		{
			_led_state = state;

			digitalWrite(_led_pin, state);
		}
	}
}

// --- Private methods implementation .

// Try to take control of the communication bus .
bool EspiSlave::_startTransmission(uint8_t attempts)
{
	while (attempts > 0)
	{
		bool collision = false;

		uint32_t startMicros = 0;

		// Check for incoming packet .
		while (_checkSlaveSelect(LOW) == true && _ss_lockedLowCount < MAX_SS_LOW_TIMEOUT)
		{
			// Handle incoming commands .
			handle();
		}

		// Prevent execution when the 'Slave Ready' is locked high .
		if (_ss_lockedLowCount >= MAX_SS_LOW_TIMEOUT)
		{
			return false;
		}

		// Disable global interrupts .
		noInterrupts();

		startMicros = micros();

		// Take control of the bus enabling slave ready .
		_setSlaveReady(HIGH);

		// Poll slave select signal
		while((micros() - startMicros) < (ANTI_COLLISION_US_DELAY / 2) && collision == false)
		{
			// Slave select signal must remains high during the anti-collision delay .
			collision = _checkSlaveSelect(LOW);
		}

		// Enable global interrupts .
		interrupts();

		if (collision == false)
		{
			return true;
		}

		// Release the control of the bus disabling slave ready .
		_setSlaveReady(LOW, SR_TX_US_DELAY + (rand() % SR_TX_US_DELAY));
	}

	// Report a collision error .
	_reportError(ERR_BUS_COLLISION);

	return false;
}

// Send a single data block .
bool EspiSlave::_sendData(uint8_t *data, uint16_t length)
{
	// Verify that the 'slave select' signal is not active before sending packet .
	if (_checkSlaveSelect(HIGH) == false)
	{
		// Report 'slave select' signal is active before transmission .
		_reportError(ERR_SYNC_SS_ALREADY_ACTIVE);

		return false;
	}

	// Load low level buffer with data to transmit .
	_writeOutgoingDataBuffer(data, length);

	// Enable slave ready signal .
	_setSlaveReady(HIGH);

	// Wait for 'slave select' signal goes low .
	if (_waitSlaveSelect(LOW, SYNC_TIMEOUT) == false)
	{
		// Disable slave ready signal .
		_setSlaveReady(LOW, SR_TX_US_DELAY);

		// Report a synchronization error: 'slave select' signal remains high .
		_reportError(ERR_SYNC_SS_HIGH_TIMEOUT);

		return false;
	}

	// Drive hardware SS low .
	_setHardwareSlaveSelect(LOW);

	// Disable slave ready signal .
	_setSlaveReady(LOW);

	// Wait for 'slave select' signal goes high .
	if (_waitSlaveSelect(HIGH, DATA_BLOCK_TIMEOUT) == false)
	{
		// Drive hardware SS high .
		_setHardwareSlaveSelect(HIGH);

		// Increase the 'SS_LOW_TIMEOUT' counter .
		_ss_lockedLowCount++;

		// Report a synchronization error: 'slave select' signal remains low .
		_reportError(ERR_SYNC_SS_LOW_TIMEOUT);

		return false;
	}

	// Drive hardware SS high .
	_setHardwareSlaveSelect(HIGH);

	// Check if the data has been transmitted .
	if (_checkDataTransmission() == false)
	{
		// Report a transmission error:  the SPI device has not transmitted the packet .
		_reportError(ERR_TRANSMISSION_FAILED);

		return false;
	}

	return true;
}

// Receive a single data block .
bool EspiSlave::_receiveData(uint8_t *data, uint16_t length)
{
	// Wait for the 'slave select' signal goes low .
	if (_waitSlaveSelect(LOW, SYNC_TIMEOUT) == false)
	{
		// Report a synchronization error: 'slave select' signal remains high .
		_reportError(ERR_SYNC_SS_HIGH_TIMEOUT);

		return false;
	}

	// Insert a small anti-collision delay .
	_delayMicroseconds(ANTI_COLLISION_US_DELAY);

	// Clear low level SPI data buffer .
	_clearIncomingDataBuffer();

	// Drive hardware SS low .
	_setHardwareSlaveSelect(LOW);

	// Enable slave ready signal .
	_setSlaveReady(HIGH);

	// Wait for 'slave select' signal goes high .
	if (_waitSlaveSelect(HIGH, DATA_BLOCK_TIMEOUT) == false)
	{
		// Drive hardware SS high .
		_setHardwareSlaveSelect(HIGH);

		// Clear low level SPI data buffer .
		_clearIncomingDataBuffer();

		// Disable slave ready signal .
		_setSlaveReady(LOW, SR_RX_US_DELAY);

		// Increase the 'SS_LOW_TIMEOUT' counter .
		_ss_lockedLowCount++;

		// Report a synchronization error: 'slave select' signal remains low .
		_reportError(ERR_SYNC_SS_LOW_TIMEOUT);

		return false;
	}

	// Drive hardware SS high .
	_setHardwareSlaveSelect(HIGH);

	// Check the data has been received .
	if (_checkDataReception() == false)
	{
		// Clear low level SPI data buffer .
		_clearIncomingDataBuffer();

		// Disable slave ready signal .
		_setSlaveReady(LOW, SR_RX_US_DELAY);

		// Report a reception error:  the SPI device has not received the packet .
		_reportError(ERR_RECEPTION_FAILED);

		return false;
	}

	// Read received data block .
	_readIncomingDataBuffer(data, length);

	// Disable slave ready signal .
	_setSlaveReady(LOW, SR_RX_US_DELAY);

	return true;
}

// Write a single block of data to SPI low level buffer .
void EspiSlave::_writeOutgoingDataBuffer(uint8_t *data, uint16_t length)
{
	uint8_t i;
	uint32_t out = 0;
	uint8_t bi = 0;
	uint8_t wi = 8;

	for (i = 0; i < 32; i++)
	{
		if (i < length)
		{
			out |= (uint32)data[i] << (bi * 8);
		}

		bi++;
		bi &= 3;

		if(!bi)
		{
			SPI1W(wi) = out;
			out = 0;
			wi++;
		}
	}

	// Sync reset .
	SPI1S |= SPISSRES;

	// Clear interrupt flags .
	SPI1S &= ~(0x1F);
}

// Check if the data has been transmitted .
bool EspiSlave::_checkDataTransmission()
{
	if ((SPI1S & SPISRBIS) != 0)
	{
		return true;
	}

	return false;
}

// Clear low level SPI data buffer .
void EspiSlave::_clearIncomingDataBuffer()
{
	volatile uint8_t __attribute__((unused)) dummyData;

	SPI1WS = 0;

	// Empty the SPI receive buffer .
	for (uint8_t i = 0; i < 8; i++)
	{
		dummyData = SPI1W(i);
	}

	// Sync reset .
	SPI1S |= SPISSRES;

	// Clear interrupt flags .
	SPI1S &= ~(0x1F);
}

// Check if the data has been received .
bool EspiSlave::_checkDataReception()
{
	if ((SPI1S & SPISWBIS) != 0)
	{
		return true;
	}

	return false;
}

// Read a single block of data from SPI low level buffer .
void EspiSlave::_readIncomingDataBuffer(uint8_t *data, uint16_t length)
{
	uint8_t tempBuffer[32];

	uint8_t i;
	uint32_t value;

	for (i = 0; i < 8; i++)
	{
		value = SPI1W(i);

		tempBuffer[i<<2] = value & 0xff;
		tempBuffer[(i<<2)+1] = (value >> 8) & 0xff;
		tempBuffer[(i<<2)+2] = (value >> 16) & 0xff;
		tempBuffer[(i<<2)+3] = (value >> 24) & 0xff;
	}

	if (data != nullptr)
	{
		for (i = 0; i < 32 && i < length; i++)
		{
			data[i] = tempBuffer[i];
		}
	}
}

// Report error .
void EspiSlave::_reportError(uint32_t errorCode)
{
	// Set the related error flag .
	_errorFlags |= errorCode;

	// Call error callback handler if any .
	if (_errorObject != nullptr && _errorMethod != nullptr)
	{
		(_errorObject->*_errorMethod)(errorCode);
	}
	else if (_errorFunction != nullptr)
	{
		_errorFunction(errorCode);
	}
}

// Driver slave select signal .
void EspiSlave::_setSlaveReady(uint8_t state, uint32_t us_delay)
{
	digitalWrite(_sr_pin, state);

	_delayMicroseconds(us_delay);
}

// Check the 'slave select' signal state .
bool EspiSlave::_checkSlaveSelect(uint8_t state)
{
	uint8_t pinState = digitalRead(_ss_pin);

	if (pinState == HIGH)
	{
		_ss_lockedLowCount = 0;
	}

	return (pinState == state);
}

// Wait for the 'slave select' signal goes in a specific state .
bool EspiSlave::_waitSlaveSelect(uint8_t state, uint32_t timeout)
{
	uint8_t pinState;
	uint32_t startMillis = millis();

	while(1)
	{
		if ((pinState = digitalRead(_ss_pin)) == HIGH)
		{
			_ss_lockedLowCount = 0;
		}

		if (pinState == state || (millis() - startMillis) > timeout)
		{
			break;
		}

		yield();
	}

	return (pinState == state);
}

// Drive hardware SS signal only if SS != 'slave select' .
void EspiSlave::_setHardwareSlaveSelect(uint8_t state)
{
	if (_ss_pin != SS)
	{
		if (state == LOW)
		{
			// Configure SS as a special pin .
			pinMode(SS, SPECIAL);
		}
		else
		{
			if (_led_pin == SS)
			{
				// Configure SS as an output pin .
				pinMode(SS, OUTPUT);
				digitalWrite(SS, _led_state);
			}
			else
			{
				// Configure SS as an input pin .
				pinMode(SS, INPUT);
			}
		}
	}
}

// Initialize the SPI peripheral in slave mode .
void EspiSlave::_deploySpiSlave()
{
	uint8_t statusLen = 4;

	if (_ss_pin != SS)
	{
		pinMode(SS, OUTPUT);
		digitalWrite(SS, HIGH);
	}
	else
	{
		pinMode(SS, SPECIAL);
	}
	pinMode(SCK, SPECIAL);
	pinMode(MISO, SPECIAL);
	pinMode(MOSI, SPECIAL);

	// SPI1S = SPISE | SPISBE | 0x3E0;
	SPI1S = SPISE | SPISBE;

	SPI1U = SPIUMISOH | SPIUCOMMAND | SPIUSSE;
	SPI1CLK = 0;
	SPI1U2 = (7 << SPILCOMMAND);
	SPI1S1 = (((statusLen * 8) - 1) << SPIS1LSTA) | (0xff << SPIS1LBUF) | (7 << SPIS1LWBA) | (7 << SPIS1LRBA) | SPIS1RSTA;
	SPI1P = (1 << 19);
	SPI1CMD = SPIBUSY;

}

// Disable the SPI peripheral .
void EspiSlave::_disposeSpiSlave()
{
	pinMode(SS, INPUT);
	pinMode(SCK, INPUT);
	pinMode(MISO, INPUT);
	pinMode(MOSI, INPUT);

	// defaults
	SPI1S = 0;
	SPI1U = SPIUSSE | SPIUCOMMAND;
	SPI1S1 = 0;
	SPI1P = B110;
}

// Delay in microsecond .
void EspiSlave::_delayMicroseconds(uint32_t us)
{
	uint32_t startMicros = micros();

	while((micros() - startMicros) < us)
	{
		yield();
	}
}


// --- End of file .
