/*
 * SpiProtocolPacket.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>

#include "Arduino.h"

#include "EspiPacket.h"


// --- Class implementation .

// Constructor .
EspiPacket::EspiPacket(uint8_t *buffer, uint16_t size) :
	_buffer(buffer),
	_size(size),
	_code(0),
	_length(0),
	_numberOfParams(0),
	_flags(0)
{
	// Empty .
}

// Destructor .
EspiPacket::~EspiPacket()
{
	// Empty .
}

// Clear and reset the packet internal data .
void EspiPacket::clear()
{
	_code = 0;
	_length = 0;
	_numberOfParams = 0;
	_flags = 0;
}

// Set the buffer pointer and the buffer size associated to the packet .
void EspiPacket::setBuffer(uint8_t *buffer, uint16_t size)
{
	_buffer = buffer;
	_size = size;

	// Initialize internal data .
	clear();
}

// Initiate the edit phase .
bool EspiPacket::beginEdit(uint8_t code)
{
	// Initialize internal data .
	clear();

	// Check data consistency .
	if (_buffer != nullptr && _size >= MIN_PACKET_SIZE)
	{
		_code = code;

		// Insert the start marker .
		_buffer[_length++] = START_PACKET;

		// Insert the command/data code .
		_buffer[_length++] = _code;

		// Leave the space to insert the length of the packet (2 Bytes) .
		_buffer[_length++] = 0;
		_buffer[_length++] = 0;

		// Leave the space to insert the number of parameters .
		_buffer[_length++] = 0;

		// The packet contains a valid header .
		_flags |= PACKET_HEADER_FLAG;

		return true;
	}

	return false;
}

// Append a 8 bit parameter to the packet buffer data .
uint16_t EspiPacket::appendBoolean(bool param)
{
	return appendByte((uint8_t) param);
}

// Append a 8 bit parameter to the packet buffer data .
uint16_t EspiPacket::appendByte(uint8_t param)
{
	// Check data consistency .
	if (_buffer != nullptr && (_length + 1 + 1 + 1) <= _size)
	{
		// Check if packet contains a valid header but not the tail .
		if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
		{
			// Add parameter size (1 byte) .
			_buffer[_length++] = 1;

			// Add parameter value .
			_buffer[_length++] = param;

			// Increase the number of parameters .
			_numberOfParams++;

			return 1;
		}
	}

	return 0;
}

// Append a 16 bit parameter to the packet buffer data .
uint16_t EspiPacket::appendWord(uint16_t param)
{
	// Check data consistency .
	if (_buffer != nullptr && (_length + 1 + 2 + 1) <= _size)
	{
		// Check if packet contains a valid header but not the tail .
		if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
		{
			// Add parameter size (2 byte) .
			_buffer[_length++] = 2;

			// Add parameter value .
			_buffer[_length++] = (uint8_t)(param >> 8);
			_buffer[_length++] = (uint8_t)(param);

			// Increase the number of parameters .
			_numberOfParams++;

			return 2;
		}
	}

	return 0;
}

// Append a 32 bit parameter to the packet buffer data .
uint16_t EspiPacket::appendDWord(uint32_t param)
{
	// Check data consistency .
	if (_buffer != nullptr && (_length + 1 + 4 + 1) <= _size)
	{
		// Check if packet contains a valid header but not the tail .
		if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
		{
			// Add parameter size (4 byte) .
			_buffer[_length++] = 4;

			// Add parameter value .
			_buffer[_length++] = (uint8_t)(param >> 24);
			_buffer[_length++] = (uint8_t)(param >> 16);
			_buffer[_length++] = (uint8_t)(param >> 8);
			_buffer[_length++] = (uint8_t)(param);

			// Increase the number of parameters .
			_numberOfParams++;

			return 4;
		}
	}

	return 0;
}

// Append a buffer as a parameter to the packet buffer data .
uint16_t EspiPacket::appendBuffer(uint8_t *buffer, uint16_t bufferLength)
{
	// Check input arguments .
	if (buffer != nullptr && bufferLength > 0)
	{
		uint8_t extendedParamSize = 1;

		// Clamp the the parameters length to the maximum value;
		if (bufferLength > MAX_PARAM_LEN)
		{
			bufferLength = 0x7fff;
		}

		if (bufferLength > 127)
		{
			extendedParamSize = 2;
		}

		// Check data consistency .
		if (_buffer != nullptr && (_length + extendedParamSize + bufferLength + 1) <= _size)
		{
			// Check if packet contains a valid header but not the tail .
			if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
			{
				// Add parameter size (bufferLength) .
				if (extendedParamSize == 1)
				{
					_buffer[_length++] = (uint8_t)bufferLength;
				}
				else
				{
					_buffer[_length++] = (uint8_t)(bufferLength >> 8) | EXT_PARAM_LEN_FLAG;
					_buffer[_length++] = (uint8_t)(bufferLength);
				}

				// Copy parameter data .
				for (uint16_t i = 0; i < bufferLength; i++)
				{
					_buffer[_length++] = buffer[i];
				}

				// Increase the number of parameters .
				_numberOfParams++;

				return bufferLength;
			}
		}
	}

	return 0;
}

// Append a text string to the packet buffer data .
uint16_t EspiPacket::appendString(const char *s)
{
	if (s != nullptr)
	{
		// Retrieve the length of the string including the terminator .
		uint16_t length = strlen(s) + 1;

		return appendBuffer((uint8_t *)s, length);
	}

	return 0;
}

// Reserve the memory in the packet to be filled with data .
uint8_t *EspiPacket::reserveData(uint16_t dataLength)
{
	uint8_t *data = nullptr;

	// Check input arguments .
	if (dataLength > 0)
	{
		uint8_t extendedParamSize = 1;

		// Clamp the the parameters length to the maximum value;
		if (dataLength > MAX_PARAM_LEN)
		{
			dataLength = 0x7fff;
		}

		if (dataLength > 127)
		{
			extendedParamSize = 2;
		}

		// Check data consistency .
		if (_buffer != nullptr && (_length + extendedParamSize + dataLength + 1) <= _size)
		{
			// Check if packet contains a valid header but not the tail .
			if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
			{
				// Add parameter size (bufferLength) .
				if (extendedParamSize == 1)
				{
					_buffer[_length++] = (uint8_t)dataLength;
				}
				else
				{
					_buffer[_length++] = (uint8_t)(dataLength >> 8) | EXT_PARAM_LEN_FLAG;
					_buffer[_length++] = (uint8_t)(dataLength);
				}

				// Set the pointer at the start of the memory to fill with data .
				data = &_buffer[_length];

				// Increase the packet length .
				_length += dataLength;

				// Increase the number of parameters .
				_numberOfParams++;
			}
		}
	}

	return data;
}

// Finalize the edit phase .
bool EspiPacket::endEdit()
{
	// Check data consistency .
	if (_buffer != nullptr && (_length + 1) <= _size)
	{
		// Check if packet contains a valid header but not the tail .
		if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_HEADER_FLAG)
		{
			// Insert the end of packet marker .
			_buffer[_length++] = END_PACKET;

			// Insert the packet length .
			_buffer[2] = (uint8_t)(_length >> 8);
			_buffer[3] = (uint8_t)(_length);

			// Insert the number of parameters .
			_buffer[4] = _numberOfParams;

			_flags |= PACKET_TAIL_FLAG;

			return true;
		}
	}

	return false;
}

// Initiate the decoding phase .
bool EspiPacket::beginDecode()
{
	bool success = false;

	// Check data consistency .
	if (_buffer != nullptr && _size >= MIN_PACKET_SIZE)
	{
		// Check the header structure and data .
		if (_buffer[0] == START_PACKET)
		{
			// Decode packet code .
			_code = _buffer[1];

			// Decode packet length .
			_length = ((uint16_t)_buffer[2] << 8) | ((uint16_t)_buffer[3]);

			// Decode the number of parameters .
			_numberOfParams = _buffer[4];

			_flags |= PACKET_HEADER_FLAG;

			// Check packet length .
			if (_length >= MIN_PACKET_LEN && _length <= _size)
			{
				success = true;
			}
		}
	}

	if (success == false)
	{
		// Initialize internal data .
		clear();
	}

	return success;
}

// Indicate if the packet has the specified parameter .
bool EspiPacket::hasParam(uint8_t paramId)
{
	// Check if packet is valid and complete .
	if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_COMPLETE_FLAG && paramId < _numberOfParams)
	{
		return true;
	}

	return false;
}

// Return the parameter length in bytes, 0 if there are no parameters .
uint16_t EspiPacket::paramLength(uint8_t paramId)
{
	uint16_t paramLength = 0;

	_findParam(paramId, &paramLength);

	return paramLength;
}

// Read an 8 bit parameter .
uint16_t EspiPacket::readBoolean(uint8_t paramId, bool *param)
{
	uint8_t value = 0;

	uint16_t count = readByte(paramId, &value);

	*param = (value != 0);

	return count;
}

// Read an 8 bit parameter .
uint16_t EspiPacket::readByte(uint8_t paramId, uint8_t *param)
{
	uint16_t paramLength = 0;

	// Check input arguments .
	if (param != nullptr)
	{
		uint8_t *data = _findParam(paramId, &paramLength);

		if (data != nullptr && paramLength == 1)
		{
			*param = data[0];
		}
		else
		{
			paramLength = 0;
		}
	}

	return paramLength;
}

// Read a 16 bit parameter .
uint16_t EspiPacket::readWord(uint8_t paramId, uint16_t *param)
{
	uint16_t paramLength = 0;

	// Check input arguments .
	if (param != nullptr)
	{
		uint8_t *data = _findParam(paramId, &paramLength);

		if (data != nullptr && paramLength == 2)
		{
			*param = ((uint16_t)data[0] << 8) | ((uint16_t)data[1]);
		}
		else
		{
			paramLength = 0;
		}
	}

	return paramLength;
}

// Read a 32 bit parameter .
uint16_t EspiPacket::readDWord(uint8_t paramId, uint32_t *param)
{
	uint16_t paramLength = 0;

	// Check input arguments .
	if (param != nullptr)
	{
		uint8_t *data = _findParam(paramId, &paramLength);

		if (data != nullptr && paramLength == 4)
		{
			*param = ((uint32_t)data[0] << 24) | ((uint32_t)data[1] << 16) |
					 ((uint32_t)data[2] << 8)  | ((uint32_t)data[3]);
		}
		else
		{
			paramLength = 0;
		}
	}

	return paramLength;
}

// Read a buffer coping data from packet buffer .
uint16_t EspiPacket::readBuffer(uint8_t paramId, uint8_t *buffer, uint16_t bytesToRead, uint16_t offset)
{
	uint16_t count = 0;

	// Check input arguments .
	if (buffer != nullptr)
	{
		uint16_t paramLength = 0;

		uint8_t *data = _findParam(paramId, &paramLength);

		if (data != nullptr)
		{
			// Copy the buffer content .
			for (count = 0; (offset + count) < paramLength && count < bytesToRead; count++)
			{
				buffer[count] = data[offset + count];
			}
		}
	}

	return count;
}

// Get the pointer to the data stored inside the packet buffer .
uint8_t *EspiPacket::getBuffer(uint8_t paramId)
{
	uint16_t paramLength = 0;

	uint8_t *bufferPtr = _findParam(paramId, &paramLength);

	if (bufferPtr != nullptr && paramLength > 0)
	{
		return bufferPtr;
	}

	return nullptr;
}

// Read a string coping data from packet buffer .
uint16_t EspiPacket::readString(uint8_t paramId, char *s, uint16_t maxLength)
{
	uint16_t count = 0;

	// Check input arguments .
	if (s != nullptr && maxLength > 0)
	{
		uint16_t paramLength = 0;

		uint8_t *data = _findParam(paramId, &paramLength);

		if (data != nullptr)
		{
			// Verify that 'message' is a null-terminated string .
			if (data[paramLength - 1] == 0)
			{
				// Copy the buffer content .
				for (count = 0; count < paramLength && count < maxLength; count++)
				{
					s[count] = (char) data[count];
				}

				// Ensure that last character is a string terminator .
				s[count - 1] = '\0';
			}
		}
	}

	return count;
}

// Get the pointer to the text string stored inside the packet buffer .
const char *EspiPacket::getString(uint8_t paramId)
{
	uint16_t paramLength = 0;

	const char *s = (const char *) _findParam(paramId, &paramLength);

	if (s != nullptr && paramLength > 0)
	{
		// Verify that 'message' is a null-terminated string .
		if (s[paramLength - 1] == '\0')
		{
			return s;
		}
	}

	return nullptr;
}

// Finalize the decoding phase .
bool EspiPacket::endDecode()
{
	bool success = false;

	// Check if packet contains a valid header .
	if ((_flags & PACKET_HEADER_FLAG) == PACKET_HEADER_FLAG)
	{
		// Check packet tail.
		if (_length >= MIN_PACKET_LEN && _length <= _size)
		{
			if (_buffer[_length - 1] == END_PACKET)
			{
				_flags |= PACKET_TAIL_FLAG;

				success = true;
			}
		}
	}
	else
	{
		// Initialize internal data .
		clear();
	}

	return success;
}


// --- Private methods .

// Return a pointer to parameter data memory inside the packet and the parameter length .
uint8_t *EspiPacket::_findParam(uint8_t paramId, uint16_t *paramLength)
{

	uint8_t *paramPtr = nullptr;

	// Initialize the return argument .
	*paramLength = 0;

	// Check if packet is valid and complete .
	if ((_flags & PACKET_COMPLETE_FLAG) == PACKET_COMPLETE_FLAG && paramId < _numberOfParams)
	{
		bool done = false, success = false;

		uint8_t paramIndex = 0;
		uint16_t index = 5;
		uint16_t length = 0;

		// Scroll the list of parameters .
		while(!done)
		{
			// Decode parameter length .
			if ((_buffer[index] & EXT_PARAM_LEN_FLAG) == EXT_PARAM_LEN_FLAG)
			{
				length = (uint16_t)(_buffer[index++] & (EXT_PARAM_LEN_FLAG - 1));
				length <<= 8;
				length |= (uint16_t)_buffer[index++];
			}
			else
			{
				length = (uint16_t)_buffer[index++];
			}

			if ((index + length) < _length)
			{
				if (paramIndex == paramId)
				{
					success = true;
					done = true;
				}
				else
				{
					index += length;
					paramIndex++;
				}
			}
			else
			{
				// Error: length out of range .
				success = false;
				done = true;
			}
		}

		if (success == true)
		{
			// Return the pointer and the length of the parameter .
			paramPtr = &_buffer[index];
			*paramLength = length;
		}
		else
		{
			paramPtr = nullptr;
			*paramLength = 0;
		}
	}

	return paramPtr;
}

// --- End of file .
