/*
 * WiFiConnManager.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConnParam.h"
#include "WiFiConnServer.h"
#include "WiFiConnManager.h"
#include "WebServer.h"

// WiFiConn server instance .
WiFiConnServer WifiConnSrv;

// WebServer instance .
WebServer WebSrv;

// The setup function is called once at startup of the sketch
void setup()
{
	IPAddress softAP_ipAddress;

	softAP_ipAddress.fromString(WIFI_CONN_SOFT_AP_IPADDR);

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.begin(115200);

	// Startup message .
	Serial.println();
	Serial.println(" - WiFiConn Manager v" + (String)WIFI_CONN_FW_VERSION + " - ");

#endif // WIFI_CONN_DEBUG_SERVER

	// Initialize common parameters .
	WiFiCommon.begin();

	// Initialize the WiFi Access Point and Station .
	initializeWiFi(softAP_ipAddress);

	// Start the 'WiFiConn' protocol server .
	WifiConnSrv.begin();

	// Start the Web server .
	WebSrv.begin(softAP_ipAddress, 80);
}

// The loop function is called in an endless loop
void loop()
{
	// Handle the incoming requests .
	WifiConnSrv.handle();

	// Handle HTTP requests .
	WebSrv.handle();
}

// Initialize the WiFi Access Point and Station .
void initializeWiFi(IPAddress ipAddress)
{
	// Retrieve the host name .
	String hostname = WiFiCommon.getHostname();

	// Don't save WiFi configuration in flash .
	WiFi.persistent(false);

	// Enable WiFi in station mode .
	WiFi.mode(WiFiMode::WIFI_AP_STA);

	// Start the software access point .
	WiFi.softAP(hostname.c_str());

	// Set default IP for AP mode .
	WiFi.softAPConfig(ipAddress, ipAddress, IPAddress(255, 255, 255, 0));
}
