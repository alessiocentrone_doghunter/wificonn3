/*
 * WiFiConnServer.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "limits.h"
#include "WiFiConnParam.h"

#include "WiFiConnServer.h"

/*******************************************************************************
    Static member initialization .
*******************************************************************************/

// Command table .
const WiFiConnServer::CommandEntry WiFiConnServer::_commandEntryTable[] =
{
	{ WiFiConnServer::CONNECT_CMD,					&WiFiConnServer::_execCmd_Connect						},
	{ WiFiConnServer::DISCONNECT_CMD,				&WiFiConnServer::_execCmd_Disconnect					},

	{ WiFiConnServer::GET_CONN_STATUS_CMD,			&WiFiConnServer::_execCmd_GetConnectionStatus			},
	{ WiFiConnServer::GET_FW_VERSION_CMD,			&WiFiConnServer::_execCmd_GetFirmwareVersion 			},

	{ WiFiConnServer::SET_IP_CONFIG_CMD,			&WiFiConnServer::_execCmd_SetIPConfig					},
	{ WiFiConnServer::SET_DNS_CONFIG_CMD,			&WiFiConnServer::_execCmd_SetDns						},
	{ WiFiConnServer::SET_HOSTNAME_CMD,				&WiFiConnServer::_execCmd_SetHostname					},

	{ WiFiConnServer::GET_LOCAL_IPADDR_CMD,			&WiFiConnServer::_execCmd_GetLocalIP					},
	{ WiFiConnServer::GET_GATEWAY_IPADDR_CMD,		&WiFiConnServer::_execCmd_GetGatewayIP					},
	{ WiFiConnServer::GET_SUBNET_MASK_CMD,			&WiFiConnServer::_execCmd_GetSubnetMask					},
	{ WiFiConnServer::GET_MACADDR_CMD,				&WiFiConnServer::_execCmd_GetMacAddress					},

	{ WiFiConnServer::GET_DNS_IPADDR_CMD,			&WiFiConnServer::_execCmd_GetDnsIP						},
	{ WiFiConnServer::GET_HOSTNAME_CMD,				&WiFiConnServer::_execCmd_GetHostname					},
	{ WiFiConnServer::GET_HOST_BY_NAME_CMD,			&WiFiConnServer::_execCmd_GetHostByName					},

	{ WiFiConnServer::GET_CURR_SSID_CMD,			&WiFiConnServer::_execCmd_GetCurrentSSID				},
	{ WiFiConnServer::GET_CURR_ENCT_CMD,			&WiFiConnServer::_execCmd_GetCurrentEncryptionType		},
	{ WiFiConnServer::GET_CURR_RSSI_CMD,			&WiFiConnServer::_execCmd_GetCurrentRSSI				},
	{ WiFiConnServer::GET_CURR_BSSID_CMD,			&WiFiConnServer::_execCmd_GetCurrentBSSID				},
	{ WiFiConnServer::GET_CURR_CHANNEL_CMD,			&WiFiConnServer::_execCmd_GetCurrentChannel				},
	{ WiFiConnServer::GET_CURR_NET_INFO_CMD,		&WiFiConnServer::_execCmd_GetCurrentNetworkInfo			},

	{ WiFiConnServer::SCAN_NETWORKS_CMD,			&WiFiConnServer::_execCmd_ScanNetworks					},

	{ WiFiConnServer::GET_IDX_SSID_CMD,				&WiFiConnServer::_execCmd_GetSSID						},
	{ WiFiConnServer::GET_IDX_ENCT_CMD,				&WiFiConnServer::_execCmd_GetEncryptionType				},
	{ WiFiConnServer::GET_IDX_RSSI_CMD,				&WiFiConnServer::_execCmd_GetRSSI						},
	{ WiFiConnServer::GET_IDX_BSSID_CMD,			&WiFiConnServer::_execCmd_GetBSSID						},
	{ WiFiConnServer::GET_IDX_CHANNEL_CMD,			&WiFiConnServer::_execCmd_GetChannel					},
	{ WiFiConnServer::GET_IDX_NET_INFO_CMD,			&WiFiConnServer::_execCmd_GetNetworkInfo				},

	{ WiFiConnServer::START_SERVER_CMD,				&WiFiConnServer::_execCmd_StartServer					},
	{ WiFiConnServer::STOP_SERVER_CMD,				&WiFiConnServer::_execCmd_StopServer					},
	{ WiFiConnServer::GET_SERVER_STATE_CMD,			&WiFiConnServer::_execCmd_GetServerState				},
	{ WiFiConnServer::GET_SERVER_CLIENT_CMD,		&WiFiConnServer::_execCmd_GetServerClient				},

	{ WiFiConnServer::START_CLIENT_CMD,				&WiFiConnServer::_execCmd_StartClient					},
	{ WiFiConnServer::STOP_CLIENT_CMD,				&WiFiConnServer::_execCmd_StopClient					},
	{ WiFiConnServer::GET_CLIENT_STATE_CMD,			&WiFiConnServer::_execCmd_GetClientState				},
	{ WiFiConnServer::GET_CLIENT_AVAIL_DATA_CMD,	&WiFiConnServer::_execCmd_GetClientAvailableData		},
	{ WiFiConnServer::GET_CLIENT_STATE_INFO_CMD,	&WiFiConnServer::_execCmd_GetClientStateInfo			},
	{ WiFiConnServer::PEEK_CLIENT_DATA_CMD,			&WiFiConnServer::_execCmd_PeekClient					},
	{ WiFiConnServer::READ_CLIENT_DATA_CMD,			&WiFiConnServer::_execCmd_ReadClient					},
	{ WiFiConnServer::READ_CLIENT_BUFF_CMD,			&WiFiConnServer::_execCmd_ReadClientBuffer				},
	{ WiFiConnServer::WRITE_CLIENT_BUFF_CMD,		&WiFiConnServer::_execCmd_WriteClientBuffer				},
	{ WiFiConnServer::EMPTY_FLUSH_CLIENT_BUFF_CMD, 	&WiFiConnServer::_execCmd_EmptyFlushClientBuffer		},

	{ WiFiConnServer::START_UDP_CMD,				&WiFiConnServer::_execCmd_StartUdp						},
	{ WiFiConnServer::STOP_UDP_CMD,					&WiFiConnServer::_execCmd_StopUdp						},
	{ WiFiConnServer::BEGIN_UDP_PKT_CMD,			&WiFiConnServer::_execCmd_BeginUdpPacket				},
	{ WiFiConnServer::END_UDP_PKT_CMD,				&WiFiConnServer::_execCmd_EndUdpPacket					},
	{ WiFiConnServer::PARSE_UDP_PKT_CMD,			&WiFiConnServer::_execCmd_ParseUdpPacket				},
	{ WiFiConnServer::GET_UDP_AVAIL_DATA_CMD,		&WiFiConnServer::_execCmd_GetUdpAvailableData			},
	{ WiFiConnServer::READ_UDP_BUFF_CMD,			&WiFiConnServer::_execCmd_ReadUdpBuffer					},
	{ WiFiConnServer::WRITE_UDP_BUFF_CMD,			&WiFiConnServer::_execCmd_WriteUdpBuffer				},
	{ WiFiConnServer::EMPTY_FLUSH_UDP_BUFF_CMD,		&WiFiConnServer::_execCmd_EmptyFlushUdpBuffer			},
	{ WiFiConnServer::GET_UDP_REMOTE_INFO_CMD,		&WiFiConnServer::_execCmd_GetUdpRemoteInfo				},
	{ WiFiConnServer::GET_UDP_LOCAL_INFO_CMD,		&WiFiConnServer::_execCmd_GetUdpLocalInfo				},

	{ WiFiConnServer::GET_NUM_STORED_AP_CMD,		&WiFiConnServer::_execCmd_GetNumberOfStoredAccessPoints	},
	{ WiFiConnServer::GET_STORED_SSID_CMD,			&WiFiConnServer::_execCmd_GetStoredSSID					},
	{ WiFiConnServer::CLEAR_STORED_AP_CMD,			&WiFiConnServer::_execCmd_ClearStoredAccessPoints		},

	{ WiFiConnServer::SET_SKETCH_NAME_CMD,			&WiFiConnServer::_execCmd_SetSketchName					},

	{ WiFiConnServer::UNKNOWN_CMD,					nullptr 												},
};

/*******************************************************************************
    Public methods implementation .
 *******************************************************************************/

// Constructor .
WiFiConnServer::WiFiConnServer() :
			_isDeployed(false),
			_requestPacket(_requestBuffer, REQ_BUFFER_SIZE),
			_answerPacket(_answerBuffer, ANS_BUFFER_SIZE),
			_eventPacket(_eventBuffer, EVT_BUFFER_SIZE),
			_espiSlave(WIFI_CONN_SLAVE_SELECT, WIFI_CONN_SLAVE_READY, WIFI_CONN_LED),
			_ledMillis(0),
			_ledState(false),
			_ledOnTime(0),
			_ledOffTime(1000)
{
	for (int i = 0; i < MAX_SOCK_NUMBER; i++)
	{
		_tcpServerSocket[i] = nullptr;
		_tcpClientSocket[i] = nullptr;
		_udpSocket[i] = nullptr;
	}
}

// Destructor .
WiFiConnServer::~WiFiConnServer()
{
	end();
}

// Initialize the server and the hw/sw resources .
bool WiFiConnServer::begin()
{
	if (_isDeployed == false)
	{
		if (_espiSlave.begin() == true)
		{
			// Deploy the common parameters system .
			if (WiFiCommon.begin() == true)
			{
				// Set the command callback handler .
				_espiSlave.setCommandCallback(this, (EspiSlave::EspiCommandMethod) &WiFiConnServer::_commandHandler, &_requestPacket, &_answerPacket);

	#ifdef WIFI_CONN_DEBUG_ESPI

				// Set the error callback handler .
				_espiSlave.setErrorCallback(this, (EspiSlave::EspiErrorMethod) &WiFiConnServer::_errorHandler);

	#endif // WIFI_CONN_DEBUG_ESPI

				_isDeployed = true;
			}
			else
			{
				_espiSlave.end();
			}
		}
	}

	return _isDeployed;
}

// Release the hw/sw resources .
void WiFiConnServer::end()
{
	if (_isDeployed == true)
	{
		// Close all open sockets .
		for (int i = 0; i < MAX_SOCK_NUMBER; i++)
		{
			// Close and delete TCP server sockets .
			if (_tcpServerSocket[i] != nullptr)
			{
				_tcpServerSocket[i]->close();

				delete _tcpServerSocket[i];

				_tcpServerSocket[i] = nullptr;
			}

			// Close and delete TCP client sockets .
			if (_tcpClientSocket[i] != nullptr)
			{
				_tcpClientSocket[i]->stop();

				delete _tcpClientSocket[i];

				_tcpClientSocket[i] = nullptr;
			}

			// Close and delete UDP  sockets .
			if (_udpSocket[i] != nullptr)
			{
				_udpSocket[i]->stop();

				delete _udpSocket[i];

				_udpSocket[i] = nullptr;
			}
		}

		// Dispose the common parameters system .
		WiFiCommon.end();

		// Dispose the communication protocol .
		_espiSlave.end();

		// Disconnect station from the network .
		WiFi.disconnect(true);

		_isDeployed = false;
	}
}

// Handle the incoming requests .
void WiFiConnServer::handle()
{
	if (_isDeployed == true)
	{
		_espiSlave.handle();

		// Blink signaling led .
		_handleSignalingLed();

	}
}

/*******************************************************************************
    Private methods implementation .
 *******************************************************************************/

// Manage signaling led .
void WiFiConnServer::_handleSignalingLed()
{
	uint32_t currentMillis = millis();

	if (_ledState == false)
	{
		if ((currentMillis - _ledMillis) >= _ledOffTime)
		{
			if (WiFi.isConnected() == true)
			{
				_ledOnTime = 250;
				_ledOffTime = 750;
			}
			else
			{
				_ledOnTime = 50;
				_ledOffTime = 2950;
			}

			_ledMillis = currentMillis;

			_espiSlave.setLed(HIGH);

			_ledState = true;
		}
	}
	else
	{
		if ((currentMillis - _ledMillis) >= _ledOnTime)
		{
			_ledMillis = currentMillis;

			_espiSlave.setLed(LOW);

			_ledState = false;
		}
	}
}

// Manage the incoming request and send the answer .
bool WiFiConnServer::_commandHandler(EspiPacket *request, EspiPacket *answer)
{
	int i = 0;
	bool sendAnswer = false;

	// Retrieve the command code associated to the request .
	uint8_t code = request->code();

	// Search the request code into the command table .
	for (i = 0; _commandEntryTable[i].code != UNKNOWN_CMD; i++)
	{
		if ((uint8_t)_commandEntryTable[i].code == code)
		{
			sendAnswer = (this->*_commandEntryTable[i].handler)(request, answer);

			break;
		}
	}

#ifdef WIFI_CONN_DEBUG_SERVER

	if (_commandEntryTable[i].code == UNKNOWN_CMD)
	{
		Serial.print((String)millis() + " - ");
		Serial.println("WiFiConnServer: unknown command (code = " + (String)code + ")");
	}

#endif // WIFI_CONN_DEBUG

	return sendAnswer;
}

// Connect to a network .
bool WiFiConnServer::_execCmd_Connect(EspiPacket *request, EspiPacket *answer)
{
	wl_status_t status = WL_CONNECT_FAILED;
	uint32_t timeout = 0;

	String ssid;
	String passphrase;

	// Determine the number of request parameters .
	uint8_t numOfParams = request->numberOfParams();

	if (numOfParams == 1 && request->paramLength(0) == 4)
	{
		// Retrieve connection timeout .
		request->readDWord(0, &timeout);

		if (WiFiCommon.findBestAvailableAccessPoint(ssid, passphrase) == true)
		{
			if (passphrase.length() > 0)
			{

#ifdef WIFI_CONN_DEBUG_SERVER

				Serial.print((String)millis() + " - ");
				Serial.println("Connect to ssid = " + ssid + ", passphrase = " + passphrase);

#endif // WIFI_CONN_DEBUG_SERVER

				status = WiFi.begin(ssid.c_str(), passphrase.c_str());
			}
			else
			{

#ifdef WIFI_CONN_DEBUG_SERVER

				Serial.print((String)millis() + " - ");
				Serial.println("Connect to ssid = " + ssid);

#endif // WIFI_CONN_DEBUG_SERVER

				status = WiFi.begin(ssid.c_str());
			}
		}
		else
		{
			status = WL_NO_SSID_AVAIL;
		}
	}
	else if (numOfParams == 2 && request->paramLength(1) == 4)
	{
		// Retrieve the Service Set Identifier of network .
		ssid = request->getString(0);

		// Retrieve connection timeout .
		request->readDWord(1, &timeout);

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.print((String)millis() + " - ");
		Serial.println("Connect to ssid = " + ssid);

#endif // WIFI_CONN_DEBUG_SERVER

		// Connection to a public network .
		status = WiFi.begin(ssid.c_str());
	}
	else if (numOfParams == 3 && request->paramLength(2) == 4)
	{
		// Retrieve the Service Set Identifier of network and the passphrase .
		ssid = request->getString(0);
		passphrase = request->getString(1);

		// Retrieve connection timeout .
		request->readDWord(2, &timeout);

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.print((String)millis() + " - ");
		Serial.println("Connect to ssid = " + ssid + ", passphrase = " + passphrase);

#endif // WIFI_CONN_DEBUG_SERVER

		// Connection to a private network .
		status = WiFi.begin(ssid.c_str(), passphrase.c_str());
	}

	if (status != WL_CONNECTED && timeout > 0)
	{
		uint32_t startMillis = millis();

		while ((millis() - startMillis) < timeout && status == WL_DISCONNECTED)
		{
			status = WiFi.status();

			delay(10);
		}

		if (status != WL_CONNECTED)
		{
			WiFi.disconnect();
		}
	}

	if (timeout == 0 || status == WL_CONNECTED)
	{
		// Add/update access point vector .
		WiFiCommon.addAccessPoint(ssid, passphrase);
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte((uint8_t)status);
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.print((String)millis() + " - ");
	Serial.println("Connection status = " + (String)status);

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Disconnect from the network .
bool WiFiConnServer::_execCmd_Disconnect(EspiPacket *request, EspiPacket *answer)
{
	bool wifiOff = false;
	bool result = false;

	request->readBoolean(0, &wifiOff);

	result = WiFi.disconnect(wifiOff);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Get connection status .
bool WiFiConnServer::_execCmd_GetConnectionStatus(EspiPacket *request, EspiPacket *answer)
{
	wl_status_t status = WiFi.status();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte((uint8_t)status);
	answer->endEdit();

	return true;
}

// Get firmware version .
bool WiFiConnServer::_execCmd_GetFirmwareVersion(EspiPacket *request, EspiPacket *answer)
{

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - GetFirmwareVersion():");

#endif // WIFI_CONN_DEBUG_SERVER

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(WIFI_CONN_FW_VERSION_MAJOR);
	answer->appendByte(WIFI_CONN_FW_VERSION_MINOR);
	answer->appendWord(WIFI_CONN_FW_VERSION_PATCH);
	answer->endEdit();

	return true;
}

// Change IP configuration settings disabling the DHCP client .
bool WiFiConnServer::_execCmd_SetIPConfig(EspiPacket *request, EspiPacket *answer)
{
	bool result = true;
	uint32_t param[5];

	// Read the request parameters .
	for (int i = 0; i < 5; i++)
	{
		if (request->readDWord(i, &param[i]) == 0)
		{
			result = false;
		}
	}

	if (result == true)
	{
		result = WiFi.config(param[0], param[1], param[2], param[3], param[4]);

	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Change DNS IP configuration .
bool WiFiConnServer::_execCmd_SetDns(EspiPacket *request, EspiPacket *answer)
{
	bool result = true;
	uint32_t param[2];

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - SetDns():");

#endif // WIFI_CONN_DEBUG_SERVER

	// Read the request parameters .
	for (int i = 0; i < 2; i++)
	{
		if (request->readDWord(i, &param[i]) == 0)
		{
			result = false;
		}
	}

	if (result == true)
	{
		IPAddress localIP = WiFi.localIP();
		IPAddress gateway = WiFi.gatewayIP();
		IPAddress subnet = WiFi.subnetMask();

		result = WiFi.config(localIP, gateway, subnet, param[0], param[1]);

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println(" -> IPAddress = " + ((IPAddress)param[0]).toString());
		Serial.println(" -> IPAddress = " + ((IPAddress)param[1]).toString());

#endif // WIFI_CONN_DEBUG_SERVER
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(" <- Boolean = " + (String)result);

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Set the DHCP hostname .
bool WiFiConnServer::_execCmd_SetHostname(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - SetHostname():");

#endif // WIFI_CONN_DEBUG_SERVER

	if (request->paramLength(0) > 1)
	{
		String hostname = request->getString(0);

		result = WiFiCommon.setHostname(hostname);

#ifdef WIFI_CONN_DEBUG_SERVER

		Serial.println(" -> String = " + hostname);

#endif // WIFI_CONN_DEBUG_SERVER

	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(" <- Boolean = " + (String)result);

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Get the interface IP address .
bool WiFiConnServer::_execCmd_GetLocalIP(EspiPacket *request, EspiPacket *answer)
{
	IPAddress localIP = WiFi.localIP();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)localIP);
	answer->endEdit();

	return true;
}

// Get the gateway IP address .
bool WiFiConnServer::_execCmd_GetGatewayIP(EspiPacket *request, EspiPacket *answer)
{
	IPAddress gateway = WiFi.gatewayIP();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)gateway);
	answer->endEdit();

	return true;
}

// Get the interface sub-net mask address .
bool WiFiConnServer::_execCmd_GetSubnetMask(EspiPacket *request, EspiPacket *answer)
{
	IPAddress subnet = WiFi.subnetMask();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - GetSubnetMask():");

#endif // WIFI_CONN_DEBUG_SERVER

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)subnet);
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(" <- IPAddress = " + subnet.toString());

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Get the interface MAC address .
bool WiFiConnServer::_execCmd_GetMacAddress(EspiPacket *request, EspiPacket *answer)
{
	uint8_t mac[MAC_ADDR_LENGTH];

	// Retrieve MAC address .
	WiFi.macAddress(mac);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBuffer(mac, MAC_ADDR_LENGTH);
	answer->endEdit();

	return true;
}

// Get the DNS IP address .
bool  WiFiConnServer::_execCmd_GetDnsIP(EspiPacket *request, EspiPacket *answer)
{
	uint8_t dnsNumber = 0;

	// Retrieve DNS server number .
	request->readByte(0, &dnsNumber);

	IPAddress dnsServer = WiFi.dnsIP(dnsNumber);

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - GetDnsIP():");

#endif // WIFI_CONN_DEBUG_SERVER

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)dnsServer);
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(" <- IPAddress = " + dnsServer.toString());

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Get the DHCP hostname .
bool WiFiConnServer::_execCmd_GetHostname(EspiPacket *request, EspiPacket *answer)
{
	// Retrieve the DHCP hostname .
	String hostname = WiFiCommon.getHostname();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println((String)millis() + " - GetHostname():");

#endif // WIFI_CONN_DEBUG_SERVER

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendString(hostname.c_str());
	answer->endEdit();

#ifdef WIFI_CONN_DEBUG_SERVER

	Serial.println(" <- String = " + hostname);

#endif // WIFI_CONN_DEBUG_SERVER

	return true;
}

// Resolve the given hostname to an IP address .
bool WiFiConnServer::_execCmd_GetHostByName(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;
	IPAddress hostIP;
	uint32_t timeout_ms;

	if (request->paramLength(0) > 1 && request->paramLength(1) == 4)
	{
		const char *hostname = request->getString(0);
		request->readDWord(1, &timeout_ms);

		result = (WiFi.hostByName(hostname, hostIP, timeout_ms) == 1);
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->appendDWord((uint32_t)hostIP);
	answer->endEdit();

	return true;
}

// Return the current SSID (Service Set Identifier) associated with the network .
bool WiFiConnServer::_execCmd_GetCurrentSSID(EspiPacket *request, EspiPacket *answer)
{
	// Retrieve SSID of the current network .
	String ssid = WiFi.SSID();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendString(ssid.c_str());
	answer->endEdit();

	return true;
}

// Return the current RSSI (Received Signal Strength) in dBm .
bool WiFiConnServer::_execCmd_GetCurrentRSSI(EspiPacket *request, EspiPacket *answer)
{
	int32_t rssi = WiFi.RSSI();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)rssi);
	answer->endEdit();

	return true;
}

// Return the Encryption Type associated with the network .
bool WiFiConnServer::_execCmd_GetCurrentEncryptionType(EspiPacket *request, EspiPacket *answer)
{
	int networkItem = -1;
	String ssid, current_ssid, current_bssid;

	uint8_t encryption = ENC_TYPE_UNKNOWN;

	// Get current SSID .
	current_ssid = WiFi.SSID();
	current_bssid = WiFi.BSSIDstr();

	// Search the current network index .
	for(int i = 0; i < 256 && (ssid = WiFi.SSID((uint8_t)i)) != ""; i++)
	{
		if (ssid == current_ssid && WiFi.BSSIDstr((uint8_t)i) == current_bssid)
		{
			networkItem = i;

			break;
		}
	}

	// Check if the system is connected to a network .
	if (networkItem != -1)
	{
		// Try to retrieve the encryption type .
		encryption = WiFi.encryptionType((uint8_t)networkItem);
	}

	// Prepare the answer packet .
	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(encryption);
	answer->endEdit();

	return true;
}

// Return the current BSSID (Basic Service Set Identifier) associated with the network .
bool WiFiConnServer::_execCmd_GetCurrentBSSID(EspiPacket *request, EspiPacket *answer)
{
	// Retrieve the BSSID .
	uint8_t *bssid = WiFi.BSSID();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBuffer(bssid, MAC_ADDR_LENGTH);
	answer->endEdit();

	return true;
}

// Return the current channel associated with the network .
bool WiFiConnServer::_execCmd_GetCurrentChannel(EspiPacket *request, EspiPacket *answer)
{
	// Retrieve the channel .
	uint8_t channel = WiFi.channel();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(channel);
	answer->endEdit();

	return true;
}

// Return all informations associated with the network .
bool WiFiConnServer::_execCmd_GetCurrentNetworkInfo(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;
	int networkItem = -1;

	String ssid, current_ssid, current_bssid;
	uint8_t encryption;
	int32_t rssi;
	uint8_t *bssid;
	int32_t channel;
	bool isHidden;

	// Get current SSID .
	current_ssid = WiFi.SSID();
	current_bssid = WiFi.BSSIDstr();

	// Search the current network index .
	for(int i = 0; i < 256 && (ssid = WiFi.SSID((uint8_t)i)) != ""; i++)
	{
		if (ssid == current_ssid && WiFi.BSSIDstr((uint8_t)i) == current_bssid)
		{
			networkItem = i;

			break;
		}
	}

	// Check if the system is connected to a network .
	if (networkItem != -1)
	{
		result = WiFi.getNetworkInfo((uint8_t)networkItem, ssid, encryption, rssi, bssid, channel, isHidden);

		// Take the latest RSSI level .
		rssi = WiFi.RSSI();
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);

	if (result == true)
	{
		answer->appendString(ssid.c_str());
		answer->appendByte(encryption);
		answer->appendDWord((uint32_t)rssi);
		answer->appendBuffer(bssid, MAC_ADDR_LENGTH);
		answer->appendByte((uint8_t)channel);
		answer->appendBoolean(isHidden);
	}

	answer->endEdit();

	return true;
}

// Scan available WiFi networks .
bool WiFiConnServer::_execCmd_ScanNetworks(EspiPacket *request, EspiPacket *answer)
{
	bool showHidden = false;

	// Retrieve the 'showHidden' option .
	request->readBoolean(0, &showHidden);

	int8_t scanResult = WiFi.scanNetworks(false, showHidden);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte((uint8_t)scanResult);
	answer->endEdit();

	return true;
}

// Return the SSID discovered during the network scan .
bool WiFiConnServer::_execCmd_GetSSID(EspiPacket *request, EspiPacket *answer)
{
	uint8_t networkItem = 255;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	// Retrieve SSID of the network .
	String ssid = WiFi.SSID(networkItem);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendString(ssid.c_str());
	answer->endEdit();

	return true;
}

// Return the RSSI of the networks discovered during the scanNetworks .
bool WiFiConnServer::_execCmd_GetRSSI(EspiPacket *request, EspiPacket *answer)
{
	uint8_t networkItem = 255;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	int32_t rssi = WiFi.RSSI(networkItem);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord((uint32_t)rssi);
	answer->endEdit();

	return true;
}

// Return the encryption type of the networks discovered during the scanNetworks .
bool WiFiConnServer::_execCmd_GetEncryptionType(EspiPacket *request, EspiPacket *answer)
{
	uint8_t networkItem = 255;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	uint8_t encryption = WiFi.encryptionType(networkItem);

	// Prepare the answer packet .
	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(encryption);
	answer->endEdit();

	return true;
}

// Return the BSSID of the networks discovered during the scanNetworks .
bool WiFiConnServer::_execCmd_GetBSSID(EspiPacket *request, EspiPacket *answer)
{
	uint8_t networkItem = 255;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	// Retrieve the BSSID .
	uint8_t *bssid = WiFi.BSSID(networkItem);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBuffer(bssid, MAC_ADDR_LENGTH);
	answer->endEdit();

	return true;
}

// Return the channel of the networks discovered during the scanNetworks .
bool WiFiConnServer::_execCmd_GetChannel(EspiPacket *request, EspiPacket *answer)
{
	uint8_t networkItem = 255;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	// Retrieve the channel .
	uint8_t channel = WiFi.channel(networkItem);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(channel);
	answer->endEdit();

	return true;
}

// Return all informations about a scanned WiFi network .
bool WiFiConnServer::_execCmd_GetNetworkInfo(EspiPacket *request, EspiPacket *answer)
{
	bool result;
	uint8_t networkItem = 255;

	String ssid;
	uint8_t encryption;
	int32_t rssi;
	uint8_t *bssid;
	int32_t channel;
	bool isHidden;

	// Retrieve the network index .
	request->readByte(0, &networkItem);

	result = WiFi.getNetworkInfo(networkItem, ssid, encryption, rssi, bssid, channel, isHidden);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);

	if (result == true)
	{
		answer->appendString(ssid.c_str());
		answer->appendByte(encryption);
		answer->appendDWord((uint32_t)rssi);
		answer->appendBuffer(bssid, MAC_ADDR_LENGTH);
		answer->appendByte((uint8_t)channel);
		answer->appendBoolean(isHidden);
	}

	answer->endEdit();

	return true;
}

// Start a TCP server on a specified port .
bool WiFiConnServer::_execCmd_StartServer(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

	// Check input parameters .
	if (request->paramLength(0) == 1 && request->paramLength(1) == 4 && request->paramLength(2) == 2)
	{
		uint8_t sock = 0;
		uint32_t ipAddress;
		uint16_t port = 0;

		// Retrieve the network index .
		request->readByte(0, &sock);
		request->readDWord(1, &ipAddress);
		request->readWord(2, &port);

		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpServerSocket[sock] != nullptr)
			{
				// Close previous socket .
				_tcpServerSocket[sock]->close();

				// Delete previous socket .
				delete _tcpServerSocket[sock];

				// Reset the pointer .
				_tcpServerSocket[sock] = nullptr;
			}

			// Create a new socket .
			_tcpServerSocket[sock] = new WiFiServer(ipAddress, port);

			// Start server socket .
			_tcpServerSocket[sock]->begin();

			result = true;
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Stop a TCP server on a specified port .
bool WiFiConnServer::_execCmd_StopServer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	bool result = false;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpServerSocket[sock] != nullptr)
			{
				// Close previous socket .
				_tcpServerSocket[sock]->close();

				// Delete previous socket .
				delete _tcpServerSocket[sock];

				// Reset the pointer .
				_tcpServerSocket[sock] = nullptr;
			}

			result = true;
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Get TCP server state .
bool WiFiConnServer::_execCmd_GetServerState(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint8_t state = (uint8_t)(wl_tcp_state::CLOSED);

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpServerSocket[sock] != nullptr)
			{
				state = _tcpServerSocket[sock]->status();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(state);
	answer->endEdit();

	return true;
}

// Get a new client from the server connection .
bool WiFiConnServer::_execCmd_GetServerClient(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

	uint8_t sock = 0;
	uint8_t newClientSock = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0 && request->readByte(1, &newClientSock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check an existing open socket .
			if (_tcpServerSocket[sock] != nullptr)
			{
				WiFiClient newClient = _tcpServerSocket[sock]->available();

				if (newClient.status() != CLOSED)
				{
					// Check if socket is open .
					if (_tcpClientSocket[newClientSock] != nullptr)
					{
						// Stop previous socket .
						_tcpClientSocket[newClientSock]->stop();

						// Delete previous socket .
						delete _tcpClientSocket[newClientSock];

						// Reset the pointer .
						_tcpClientSocket[newClientSock] = nullptr;
					}

					// Create a new socket .
					_tcpClientSocket[newClientSock] = new WiFiClient(newClient);

					result = true;
				}
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(result);
	answer->endEdit();

	return true;
}

// Start a TCP client on a specified port .
bool WiFiConnServer::_execCmd_StartClient(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

	// Check input parameters .
	if (request->paramLength(0) == 1 && request->paramLength(1) == 4 && request->paramLength(2) == 2)
	{
		uint8_t sock = 0;
		uint32_t ipAddress;
		uint16_t port = 0;

		// Retrieve the network index .
		request->readByte(0, &sock);
		request->readDWord(1, &ipAddress);
		request->readWord(2, &port);

		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				uint32_t startMillis = millis();

				// Empty receive buffer before stopping connection .
				while(_tcpClientSocket[sock]->read() != EOF)
				{
					if ((millis() - startMillis) >= 1000)
					{
						break;
					}
				}

				// Stop previous socket .
				_tcpClientSocket[sock]->stop();

				// Delete previous socket .
				delete _tcpClientSocket[sock];

				// Reset the pointer .
				_tcpClientSocket[sock] = nullptr;
			}

			// Create a new socket .
			_tcpClientSocket[sock] = new WiFiClient();

			// Start client socket .
			result = (bool) _tcpClientSocket[sock]->connect(ipAddress, port);
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Stop a TCP client .
bool WiFiConnServer::_execCmd_StopClient(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	bool result = false;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				uint32_t startMillis = millis();

				// Empty receive buffer before stopping connection .
				while(_tcpClientSocket[sock]->read() != EOF)
				{
					if ((millis() - startMillis) >= 1000)
					{
						break;
					}
				}

				// Stop previous socket .
				_tcpClientSocket[sock]->stop();

				// Delete previous socket .
				delete _tcpClientSocket[sock];

				// Reset the pointer .
				_tcpClientSocket[sock] = nullptr;
			}

			result = true;
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Get TCP client state .
bool WiFiConnServer::_execCmd_GetClientState(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint8_t state = (uint8_t)(wl_tcp_state::CLOSED);

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				state = _tcpClientSocket[sock]->status();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(state);
	answer->endEdit();

	return true;
}

// Get TCP client available data .
bool WiFiConnServer::_execCmd_GetClientAvailableData(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint16_t available = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				available = _tcpClientSocket[sock]->available();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendWord(available);
	answer->endEdit();

	return true;
}

// Get client connection state and informations .
bool WiFiConnServer::_execCmd_GetClientStateInfo(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint8_t state = (uint8_t)wl_tcp_state::CLOSED;
	uint16_t availForRead = 0;
	uint16_t availForWrite = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				state = _tcpClientSocket[sock]->status();
				availForRead = _tcpClientSocket[sock]->available();
				availForWrite = _tcpClientSocket[sock]->availableForWrite();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(state);
	answer->appendWord(availForRead);
	answer->appendWord(availForWrite);
	answer->endEdit();

	return true;
}

// Peek client data .
bool WiFiConnServer::_execCmd_PeekClient(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	int data = EOF;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				data = _tcpClientSocket[sock]->peek();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);

	// Append data only if available .
	if (data != EOF)
	{
		answer->appendByte((uint8_t)data);
	}

	answer->endEdit();

	return true;
}

// Read client data .
bool WiFiConnServer::_execCmd_ReadClient(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	int data = EOF;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				data = _tcpClientSocket[sock]->read();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);

	// Append data only if available .
	if (data != EOF)
	{
		answer->appendByte((uint8_t)data);
	}

	answer->endEdit();

	return true;
}

// Read client buffer .
bool WiFiConnServer::_execCmd_ReadClientBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	uint16_t length = 0;

	// Retrieve socket number and data length.
	if (request->readByte(0, &sock) > 0 && request->readWord(1, &length) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				uint16_t available = _tcpClientSocket[sock]->available();

				// Clamp data length .
				if (length > available)
				{
					length = available;
				}
				if (length > (ANS_BUFFER_SIZE - 10))
				{
					length = (ANS_BUFFER_SIZE - 10);
				}

				answer->beginEdit(request->code() | REPLAY_CODE_FLAG);

				if (length > 0)
				{
					// Try to reserve memory in the answer packet .
					uint8_t *data = answer->reserveData(length);

					if (data != nullptr)
					{
						// Fill reserved memory with data .
						_tcpClientSocket[sock]->readBytes(data, length);
					}
				}

				answer->endEdit();

				return true;
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->endEdit();

	return true;
}

// Write client buffer .
bool WiFiConnServer::_execCmd_WriteClientBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	uint16_t count = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0 && request->paramLength(1) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				uint16_t length = request->paramLength(1);
				uint8_t *buffer = request->getBuffer(1);

				count = _tcpClientSocket[sock]->write(buffer, length);
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendWord(count);
	answer->endEdit();

	return true;
}

// Empty/Flush client buffer .
bool WiFiConnServer::_execCmd_EmptyFlushClientBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	bool rx = false, tx = false;

	bool result = false;

	// Retrieve socket number .
	if ( request->readByte(0, &sock)  > 0 	&&
		 request->readBoolean(1, &rx) > 0	&&
		 request->readBoolean(2, &tx) > 0	)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_tcpClientSocket[sock] != nullptr)
			{
				if (rx == true)
				{
					uint32_t startMillis = millis();

					while(_tcpClientSocket[sock]->read() != EOF)
					{
						if ((millis() - startMillis) >= 1000)
						{
							break;
						}
					}
				}

				if (tx == true)
				{
					_tcpClientSocket[sock]->flush();
				}
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Start a UDP connection on a specified port .
bool WiFiConnServer::_execCmd_StartUdp(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

	// Check input parameters .
	if (request->paramLength(0) == 1 && request->paramLength(1) == 2)
	{
		uint8_t sock = 0;
		uint16_t port = 0;

		// Retrieve the network index .
		request->readByte(0, &sock);
		request->readWord(1, &port);

		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				// Close previous socket .
				_udpSocket[sock]->stop();

				// Delete previous socket .
				delete _udpSocket[sock];

				// Reset the pointer .
				_udpSocket[sock] = nullptr;
			}

			// Create a new socket .
			_udpSocket[sock] = new WiFiUDP();

			// Start listening .
			if (_udpSocket[sock]->begin(port) == 1)
			{
				result = true;
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Stop a UDP connection .
bool WiFiConnServer::_execCmd_StopUdp(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	bool result = false;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				// Close previous socket .
				_udpSocket[sock]->stop();

				// Delete previous socket .
				delete _udpSocket[sock];

				// Reset the pointer .
				_udpSocket[sock] = nullptr;
			}

			result = true;
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Start building up an UDP packet .
bool WiFiConnServer::_execCmd_BeginUdpPacket(EspiPacket *request, EspiPacket *answer)
{
	bool result = false;

	// Check input parameters .
	if (request->paramLength(0) == 1 && request->paramLength(1) == 4 && request->paramLength(2) == 2)
	{
		uint8_t sock = 0;
		uint32_t ipAddress;
		uint16_t port = 0;

		// Retrieve the network index .
		request->readByte(0, &sock);
		request->readDWord(1, &ipAddress);
		request->readWord(2, &port);

		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] == nullptr)
			{
				// Create a new socket .
				_udpSocket[sock] = new WiFiUDP();
			}

			// Connect to remote UDP server .
			_udpSocket[sock]->beginPacket(ipAddress, port);

			result = true;
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Finish off the packet and send it .
bool WiFiConnServer::_execCmd_EndUdpPacket(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	bool result = false;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				// Send UDP packet .
				if (_udpSocket[sock]->endPacket() == 1)
				{
					result = true;
				}
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Parse incoming UDP packet and return the size of the packet in bytes .
bool WiFiConnServer::_execCmd_ParseUdpPacket(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint16_t packetSize = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				// Retrieve new packet size .
				packetSize = _udpSocket[sock]->parsePacket();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendWord(packetSize);
	answer->endEdit();

	return true;
}

// Get UDP available data .
bool WiFiConnServer::_execCmd_GetUdpAvailableData(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	uint16_t available = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				available = _udpSocket[sock]->available();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendWord(available);
	answer->endEdit();

	return true;
}

// Read UDP buffer .
bool WiFiConnServer::_execCmd_ReadUdpBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	uint16_t length = 0;

	// Retrieve socket number and data length.
	if (request->readByte(0, &sock) > 0 && request->readWord(1, &length) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				uint16_t available = _udpSocket[sock]->available();

				// Clamp data length .
				if (length > available)
				{
					length = available;
				}
				if (length > (ANS_BUFFER_SIZE - 10))
				{
					length = (ANS_BUFFER_SIZE - 10);
				}

				answer->beginEdit(request->code() | REPLAY_CODE_FLAG);

				if (length > 0)
				{
					// Try to reserve memory in the answer packet .
					uint8_t *data = answer->reserveData(length);

					if (data != nullptr)
					{
						// Fill reserved memory with data .
						_udpSocket[sock]->read(data, length);
					}
				}

				answer->endEdit();

				return true;
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->endEdit();

	return true;
}

// Write UDP buffer .
bool WiFiConnServer::_execCmd_WriteUdpBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	uint16_t count = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0 && request->paramLength(1) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				uint16_t length = request->paramLength(1);
				uint8_t *buffer = request->getBuffer(1);

				count = _udpSocket[sock]->write(buffer, length);
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendWord(count);
	answer->endEdit();

	return true;
}

// Empty/Flush UDP packet buffers .
bool WiFiConnServer::_execCmd_EmptyFlushUdpBuffer(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;
	bool rx = false, tx = false;

	bool result = false;

	// Retrieve socket number .
	if ( request->readByte(0, &sock)  > 0 	&&
		 request->readBoolean(1, &rx) > 0	&&
		 request->readBoolean(2, &tx) > 0	)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				if (rx == true)
				{
					uint32_t startMillis = millis();

					while(_udpSocket[sock]->read() != EOF)
					{
						if ((millis() - startMillis) >= 1000)
						{
							break;
						}
					}
				}

				if (tx == true)
				{
					_udpSocket[sock]->flush();
				}
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendBoolean(result);
	answer->endEdit();

	return true;
}

// Retrieve remote IP address and port .
bool WiFiConnServer::_execCmd_GetUdpRemoteInfo(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	IPAddress ipAddress;
	uint16_t port = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				ipAddress = _udpSocket[sock]->remoteIP();
				port = _udpSocket[sock]->remotePort();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord(ipAddress);
	answer->appendWord(port);
	answer->endEdit();

	return true;
}

// Retrieve local IP address and port .
bool WiFiConnServer::_execCmd_GetUdpLocalInfo(EspiPacket *request, EspiPacket *answer)
{
	uint8_t sock = 0;

	IPAddress ipAddress;
	uint16_t port = 0;

	// Retrieve socket number .
	if (request->readByte(0, &sock) > 0)
	{
		if (sock < MAX_SOCK_NUMBER)
		{
			// Check if socket is open .
			if (_udpSocket[sock] != nullptr)
			{
				ipAddress = _udpSocket[sock]->destinationIP();
				port = _udpSocket[sock]->localPort();
			}
		}
	}

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendDWord(ipAddress);
	answer->appendWord(port);
	answer->endEdit();

	return true;
}

// Return the number of Access Points stored in the non-volatile memory .
bool WiFiConnServer::_execCmd_GetNumberOfStoredAccessPoints(EspiPacket *request, EspiPacket *answer)
{
	uint8_t storedNetworks = WiFiCommon.getNumberOfAccessPoints();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendByte(storedNetworks);
	answer->endEdit();

	return true;
}

// Return the SSID related to a stored Access Point .
bool WiFiConnServer::_execCmd_GetStoredSSID(EspiPacket *request, EspiPacket *answer)
{
	uint8_t accessPointIndex = 255;

	// Retrieve the access point index in the vector .
	request->readByte(0, &accessPointIndex);

	// Retrieve SSID .
	String ssid = WiFiCommon.getAccessPointSSID(accessPointIndex);

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->appendString(ssid.c_str());
	answer->endEdit();

	return true;
}

// Clear all Access Points stored in the non-volatile memory .
bool WiFiConnServer::_execCmd_ClearStoredAccessPoints(EspiPacket *request, EspiPacket *answer)
{
	// Clear all Access Points.
	WiFiCommon.clearAllAccessPoints();

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->endEdit();

	return true;
}

// Inform the WiFi module about the current running sketch .
bool WiFiConnServer::_execCmd_SetSketchName(EspiPacket *request, EspiPacket *answer)
{
	// Set sketch name as common information .
	WiFiCommon.setRunningSketch(request->getString(0));

	answer->beginEdit(request->code() | REPLAY_CODE_FLAG);
	answer->endEdit();

	return true;
}

#ifdef WIFI_CONN_DEBUG_ESPI

// Print error message when it occurs .
void WiFiConnServer::_errorHandler(uint32_t errorCode)
{
	Serial.print((String)millis() + " - ");

	switch (errorCode)
	{
		case EspiSlave::ERR_INVALID_PACKET_HEADER:
		{
			Serial.println("Espi error: INVALID PACKET HEADER");
			break;
		}
		case EspiSlave::ERR_INVALID_PACKET_TAIL:
		{
			Serial.println("Espi error: INVALID PACKET TAIL");
			break;
		}
		case EspiSlave::ERR_BUS_COLLISION:
		{
			Serial.println("Espi error: BUS COLLISION");
			break;
		}
		case EspiSlave::ERR_SYNC_SS_ALREADY_ACTIVE:
		{
			Serial.println("Espi error: SYNC SS ALREADY ACTIVE");
			break;
		}
		case EspiSlave::ERR_SYNC_SS_HIGH_TIMEOUT:
		{
			Serial.println("Espi error: SYNC SS HIGH TIMEOUT");
			break;
		}
		case EspiSlave::ERR_SYNC_SS_LOW_TIMEOUT:
		{
			Serial.println("Espi error: SYNC SS LOW TIMEOUT");
			break;
		}
		case EspiSlave::ERR_TRANSMISSION_FAILED:
		{
			Serial.println("Espi error: TRANSMISSION FAILED");
			break;
		}
		case EspiSlave::ERR_RECEPTION_FAILED:
		{
			Serial.println("Espi error: RECEPTION FAILED");
			break;
		}
		default:
		{
			Serial.println("Espi error: UNKNOWN");
			break;
		}
	}
}

#endif // WIFI_CONN_DEBUG_ESPI

/******************************************************************************/


