/*
 * EspiSlave.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ESPI_SLAVE_H_
#define ESPI_SLAVE_H_

// --- Includes .
#include "Arduino.h"
#include "EspiPacket.h"

// --- Class definition .

// Class that provides a communication protocol via SPI (Master side) .
class EspiSlave
{
public:

	// Command callback handler definition .
	typedef bool(*EspiCommandFunction)(EspiPacket *request, EspiPacket *answer);

	// Command callback method definition .
	typedef bool (EspiSlave::*EspiCommandMethod)(EspiPacket *request, EspiPacket *answer);

	// Error callback handler definition .
	typedef void(*EspiErrorFunction)(uint32_t errorCode);

	// Error callback method definition .
	typedef void(EspiSlave::*EspiErrorMethod)(uint32_t errorCode);

	// Enumeration of all error flags .
	enum ErrorFlags: uint32_t
	{
		ERR_INVALID_PACKET_HEADER	= 0x00000001,
		ERR_INVALID_PACKET_TAIL		= 0x00000002,
		ERR_BUS_COLLISION			= 0x00000004,
		ERR_SYNC_SS_ALREADY_ACTIVE  = 0x00000008,
		ERR_SYNC_SS_HIGH_TIMEOUT	= 0x00000010,
		ERR_SYNC_SS_LOW_TIMEOUT		= 0x00000020,
		ERR_TRANSMISSION_FAILED		= 0x00000040,
		ERR_RECEPTION_FAILED		= 0x00000080
	};

	// Constructor .
	EspiSlave(uint8_t slaveSelectPin, uint8_t slaveReadyPin, uint8_t ledPin = 255);

	// Destructor .
	virtual ~EspiSlave();


	// Initialize the protocol and the hardware resources .
	bool begin();

	// Release the hardware resources .
	void end();


	// Set the command callback handler .
	void setCommandCallback(EspiCommandFunction commandFunction, EspiPacket *requestPacket, EspiPacket *answerPacket);

	// Set the command callback handler .
	void setCommandCallback(void *commandObject, EspiCommandMethod commandMethod, EspiPacket *requestPacket, EspiPacket *answerPacket);

	// Set the error callback handler .
	void setErrorCallback(EspiErrorFunction errorFunction);

	// Set the error callback handler .
	void setErrorCallback(void *errorObject, EspiErrorMethod errorMethod);


	// Handle the incoming requests .
	void handle();

	// Send a packet eventually splitting it in multiple data blocks .
	bool sendPacket(EspiPacket *packet);

	// Receive a packet eventually joining together multiple data blocks .
	bool receivePacket(EspiPacket *packet);

	// Read (and eventually reset) error flags .
	uint32_t readErrors(bool reset = false);

	// Drive the signaling led .
	void setLed(uint8_t state);


private:

	enum { ESP8266_SPI_MAX_PACKET_LEN = 32 };

	enum { MAX_SS_LOW_TIMEOUT = 3 };

	// Indicate the state of use of the hardware resources .
	static bool _isDeployed;

	// Packet to handle and store incoming request data .
	EspiPacket *_requestPacket;

	// Packet to handle and store outgoing answer data .
	EspiPacket *_answerPacket;

	// Command callback .
	EspiSlave *_commandObject;
	EspiCommandMethod _commandMethod;
	EspiCommandFunction _commandFunction;

	// Error callback .
	EspiSlave *_errorObject;
	EspiErrorMethod _errorMethod;
	EspiErrorFunction _errorFunction;

	// Error flags .
	uint32_t _errorFlags;

	// Slave select signal .
	uint8_t _ss_pin;

	// Slave ready signal .
	uint8_t _sr_pin;

	// Count how many times the signal remains locked low .
	uint8_t _ss_lockedLowCount;

	// Led pin .
	uint8_t _led_pin;

	// led state .
	uint8_t _led_state;


	// Try to take control of the communication bus .
	bool _startTransmission(uint8_t attempts = 3);

	// Send a single data block .
	bool _sendData(uint8_t *data, uint16_t length);

	// Receive a single data block .
	bool _receiveData(uint8_t *data, uint16_t length);


	// Write a single block of data to SPI low level buffer .
	void _writeOutgoingDataBuffer(uint8_t *data, uint16_t length);

	// Check if the data has been transmitted .
	bool _checkDataTransmission();


	// Clear low level SPI data buffer .
	void _clearIncomingDataBuffer();

	// Check if the data has been received .
	bool _checkDataReception();

	// Read a single block of data from SPI low level buffer .
	void _readIncomingDataBuffer(uint8_t *data, uint16_t length);


	// Report error .
	void _reportError(uint32_t errorCode);

	// Driver slave ready signal .
	void inline _setSlaveReady(uint8_t state, uint32_t us_delay = 0);

	// Check the 'slave select' signal state .
	bool _checkSlaveSelect(uint8_t state);

	// Wait for the 'slave select' signal goes in a specific state .
	bool _waitSlaveSelect(uint8_t state, uint32_t timeout);

	// Drive hardware SS signal only if SS != 'slave select' .
	void _setHardwareSlaveSelect(uint8_t state);


	// Initialize the SPI peripheral in slave mode .
	void _deploySpiSlave();

	// Disable the SPI peripheral .
	void _disposeSpiSlave();

	// Delay in microsecond .
	void _delayMicroseconds(uint32_t us);

};

#endif /* ESPI_SLAVE_H_ */
