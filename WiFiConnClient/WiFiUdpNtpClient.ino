/*
 * WiFiUdpNtpClient.ino
 *
 * UDP NTP Client
 *
 * Get the time from a Network Time Protocol (NTP) time server .
 * Demonstrates how to use WiFiUdp class to send and receive UDP packets .
 * For more on NTP time servers and the messages needed to communicate with them,
 * see http://en.wikipedia.org/wiki/Network_Time_Protocol
 *
 * Created on 4 Sep 2010 by Michael Margolis
 *
 * Modified on 9 Apr 2012 by Tom Igoe
 *
 * Modified on 10 March 2017 by Sergio Tomasello and Andrea Cannistrá
 *
 * Modified on 24 August 2018 by Alessio Centrone
 *
 * This code is in the public domain.
 *
 */

#include "WiFiConn/src/WiFiConn.h"
#include "WiFiConn/src/WiFiUdp.h"


const char ssid[] = "whatsnext-guest";
const char passphrase[] = "ht34!eG$";


// Local port to listen for UDP packets
const uint16_t LOCAL_PORT = 2390;

// NTP time stamp is in the first 48 bytes of the message .
const int NTP_PACKET_SIZE = 48;

// Buffer to hold incoming and outgoing packets
uint8_t ntpBuffer[NTP_PACKET_SIZE];

// NTP server name .
const char ntpServerName[] = "time.nist.gov";

// NTP server default IP .
IPAddress ntpServerIP(129, 6, 15, 28);

// An UDP instance to let us send and receive packets over UDP .
WiFiUdp udp;

uint32_t requestMillis = 0;
uint32_t responseMillis = 0;

bool requestPending = false;


void setup()
{
	delay(500);

	// Initialize serial .
	Serial.begin(115200);

	// Print a welcome message .
	Serial.println("                           ");
	Serial.println("*** WiFi-Conn UDP NTP Client ***");

	Serial.println();
	Serial.println("Starting WiFi ...");
	
	// Check WiFi module and try to connect to the access points .
	startWiFi();

	// Start listening for UDP messages .
	startUdp();

	// Get the IP address of the NTP server .
	if (WiFi.hostByName(ntpServerName, ntpServerIP) == false)
	{
	    Serial.println(" - DNS lookup failed! Use a default address.");
	}

	Serial.print(" - Time server:\t");
	Serial.println(ntpServerIP);
	Serial.println();

	// Send first NTP request .
	Serial.println("Sending NTP request ...");
	sendNtpPacket(ntpServerIP);

	requestMillis = responseMillis = millis();
	requestPending = true;
}

void loop()
{
	if (WiFi.isConnected() == true)
	{
		uint32_t currentMillis = millis();

		// Send a request every 10 seconds .
		if ((currentMillis - requestMillis) >= 10000)
		{
			Serial.println("Sending NTP request ...");
			sendNtpPacket(ntpServerIP);
	
			requestMillis = responseMillis = currentMillis;
			requestPending = true;
		}		
		
		if (requestPending == true)
		{
			// Poll the response every 100 milliseconds if a request is pending .
			if ((currentMillis - responseMillis) >= 100)
			{
				uint32_t utcTime = getTime();
	
				if (utcTime != 0)
				{
					requestPending = false;
	
					// Print current time .
					printTime(utcTime);
				}
				else
				{
					responseMillis = currentMillis;
				}
			}
		}
	}
	else
	{
		Serial.println();
		Serial.println("Connection with WiFi network LOST! Restarting WiFi ...");

		startWiFi();
	}
}

// Check WiFi module and try to connect to the access points .
void startWiFi()
{
	// Read WiFi module status .
	wl_status_t status = WiFi.status();

	// Check if communication with the WiFi module has been established .
	if (status == WL_NO_WIFI_MODULE_COMM)
	{
		Serial.println();
		Serial.println(" - Communication with WiFi module not established.");
	}

	// Attempt to connect to WiFi network:
	while (status != WL_CONNECTED)
	{
		Serial.println();
		Serial.print(" - Attempting to connect to ");
		Serial.println(ssid);

		// Connect to WPA/WPA2 network .
		status = WiFi.begin(ssid, passphrase);

		if (status != WL_CONNECTED)
		{
			// Wait 5 seconds before trying to connect again .
			delay(5000);
		}
	}

	Serial.println();

	Serial.print(" - Connected to ");
	Serial.println(WiFi.SSID());

	Serial.print(" - IP address:\t");
	Serial.println(WiFi.localIP());

	Serial.print(" - RSSI:\t");
	Serial.print(WiFi.RSSI());
	Serial.println(" dBm");
}

// Start listening for UDP messages .
void startUdp()
{
	Serial.println();
	Serial.println("Starting UDP ...");

	udp.begin(LOCAL_PORT);

	Serial.println();
	Serial.print(" - Local port:\t");
	Serial.println(udp.localPort());
}

// Send an NTP request to the time server at the given address .
void sendNtpPacket(IPAddress& address)
{
	// Set all bytes in the buffer to zero .
	memset(ntpBuffer, 0, NTP_PACKET_SIZE);

	// Initialize values needed to form NTP request .
	// (see URL above for details on the packets)

	ntpBuffer[0] = 0b11100011;   	// LI, Version, Mode

	// Send a packet requesting a time-stamp. The requests are to port 123 .
	udp.beginPacket(address, 123);
	udp.write(ntpBuffer, NTP_PACKET_SIZE);
	udp.endPacket();
}

// Parse NTP response and return the current time (UTC) .
uint32_t getTime()
{
	// Return immediately if there is no response .
	if (udp.parsePacket() == 0)
	{
		return 0;
	}

	// Read the packet into the buffer .
	udp.read(ntpBuffer, NTP_PACKET_SIZE);

	// Combine the 4 time-stamp bytes into one 32-bit number .
	uint32_t ntpTime = ((uint32_t)ntpBuffer[40] << 24) | ((uint32_t)ntpBuffer[41] << 16) |
					   ((uint32_t)ntpBuffer[42] << 8)  | (uint32_t)ntpBuffer[43];

	// Convert NTP time to a UNIX time-stamp:

	// UNIX time starts on January 1 1970. That's 2208988800 seconds in NTP time .
	const uint32_t seventyYears = 2208988800UL;

	// Subtract seventy years:
	uint32_t utcTime = ntpTime - seventyYears;

	return utcTime;
}

// Print current time .
void printTime(uint32_t utcTime)
{
	 // UTC is the time at Greenwich Meridian (GMT)
	Serial.print(" - UTC time:\t");

	int hour = getHours(utcTime);
	Serial.print(hour);
	Serial.print(':');

	int minutes = getMinutes(utcTime);

	if (minutes < 10)
	{
		Serial.print('0');
	}
	Serial.print(minutes);

	Serial.print(':');

	int seconds = getSeconds(utcTime);

	if (seconds < 10)
	{
		Serial.print('0');
	}
	Serial.print(seconds);

	Serial.println();
}

inline int getSeconds(uint32_t unixTime)
{
	return unixTime % 60;
}

inline int getMinutes(uint32_t unixTime)
{
	return unixTime / 60 % 60;
}

inline int getHours(uint32_t unixTime)
{
	return unixTime / 3600 % 24;
}


