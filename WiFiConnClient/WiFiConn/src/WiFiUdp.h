/*
 * WiFiUdp.h
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_UDP_H_
#define WIFI_UDP_H_

#include <Udp.h>

class WiFiUdp : public UDP
{
public:

	// Constructor .
	WiFiUdp();

	// Constructor .
	WiFiUdp(const WiFiUdp & other);

	// Destructor .
	virtual ~WiFiUdp();

	// Assignment operator .
	virtual WiFiUdp & operator=(WiFiUdp& other);

	// Initialize, start listening on specified port. Returns 1 if successful, 0 if there are no sockets available to use .
	virtual uint8_t begin(uint16_t port);

	// Finish with the UDP socket .
	virtual void stop();

	// --- Sending UDP packets ---

	// Start building up a packet to send to the remote host specific in ip and port .
	// Returns 1 if successful, 0 if there was a problem with the supplied IP address or port .
	virtual int beginPacket(IPAddress ip, uint16_t port);

	// Start building up a packet to send to the remote host specific in host and port .
	// Returns 1 if successful, 0 if there was a problem resolving the hostname or port .
	virtual int beginPacket(const char *host, uint16_t port);

	// Finish off this packet and send it .
	// Returns 1 if the packet was sent successfully, 0 if there was an error .
	virtual int endPacket();

	// Write a single byte into the packet .
	virtual size_t write(uint8_t);

	// Write size bytes from buffer into the packet .
	virtual size_t write(const uint8_t *buffer, size_t length);

	using Print::write;

	// Start processing the next available incoming packet .
	// Returns the size of the packet in bytes, or 0 if no packets are available .
	virtual int parsePacket();

	// Number of bytes remaining in the current packet .
	virtual int available();

	// Read a single byte from the current packet .
	virtual int read();

	// Read up to 'length' bytes from the current packet and place them into buffer .
	// Returns the number of bytes read, or 0 if none are available .
	virtual int read(unsigned char* buffer, size_t length);

	// Read up to 'length' characters from the current packet and place them into buffer .
	// Returns the number of characters read, or 0 if none are available .
	virtual int read(char* buffer, size_t length) { return read((unsigned char*)buffer, length); };

	// Return the next byte from the current packet without moving on to the next byte .
	virtual int peek();

	// Finish sending the current packet .
	virtual void flush();

	// Empty internal receive buffers .
	virtual void empty();

	// Return the IP address of the host who sent the current incoming packet .
	virtual IPAddress remoteIP();

	// Return the port of the host who sent the current incoming packet .
	virtual uint16_t remotePort();

	// Return the destination IP address .
	virtual IPAddress destinationIP();

	// Return the local port .
	virtual uint16_t localPort();

private:

	// Private subclass .
	class Context
	{

	public:

		// Constructor .
		Context();

		// Destructor .
		virtual ~Context();

		// Initialize, start listening on specified port. Returns 1 if successful, 0 if there are no sockets available to use .
		uint8_t begin(uint16_t port);

		// Finish with the UDP socket .
		void stop();

		// Start building up a packet to send to the remote host specific in ip and port .
		int beginPacket(IPAddress ip, uint16_t port);

		// Finish off this packet and send it .
		int endPacket();

		// Write size bytes from buffer into the packet .
		size_t write(const uint8_t *buffer, size_t size);

		// Start processing the next available incoming packet .
		int parsePacket();

		// Number of bytes remaining in the current packet .
		int available();

		// Read a single byte from the current packet .
		int read();

		// Read up to 'length' bytes from the current packet and place them into buffer .
		int read(unsigned char* buffer, size_t length);

		// Read up to 'length' characters from the current packet and place them into buffer .
		int read(char* buffer, size_t length) { return read((unsigned char*)buffer, length); };

		// Return the next byte from the current packet without moving on to the next byte .
		int peek();

		// Finish sending the current packet .
		void flush();

		// Empty internal receive buffers .
		void empty();

		// Return the IP address of the host who sent the current incoming packet .
		IPAddress remoteIP();

		// Return the port of the host who sent the current incoming packet .
		uint16_t remotePort();

		// Return the destination IP address .
		IPAddress destinationIP();

		// Return the local port .
		uint16_t localPort();

		// Increment the reference counter .
		void ref();

		// Decrement the reference counter and eventually delete the object .
		void unref();

	private:

		// Size of the internal buffer .
		enum { BUFFER_SIZE = 24 };

		// Socket identifier .
		uint8_t _sock;

		// Available data in the current UDP packet .
		uint16_t _availableData;

		// Index to current item in the buffer .
		uint16_t _bufferIndex;

		// Number of items stored in the buffer .
		uint16_t _bufferCount;

		// Internal buffer to cache input data .
		uint8_t _buffer[BUFFER_SIZE];

		// Reference counter .
		int _referenceCount;
	};

	// Pointer to udp context .
	Context *_context;
};

// Type definition for compatibility with WiFiLink sketches .
typedef WiFiUdp WiFiUDP;

#endif /* WIFI_UDP_H_ */
