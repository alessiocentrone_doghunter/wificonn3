/*
 * WiFiConn.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConn.h"

/*******************************************************************************
    Class instantiation .
*******************************************************************************/

// Class instance .
WiFiConn WiFi;

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/

// Constructor .
WiFiConn::WiFiConn() :
	_isDeployed(false),
	_requestPacket(_ioBuffer, WL_IO_BUFFER_SIZE),
	_answerPacket(_ioBuffer, WL_IO_BUFFER_SIZE),
	_eventPacket(_ioBuffer, WL_IO_BUFFER_SIZE),
	_espiMaster(WIFI_CONN_SLAVE_SELECT, WIFI_CONN_SLAVE_READY)
{
	// Initialize socket use state .
	for(int i = 0; i < MAX_SOCK_NUM; i++)
	{
		_clientSocketInUse[i] = false;
		_serverSocketInUse[i] = false;
		_udpSocketInUse[i] = false;
	}
}

// Destructor .
WiFiConn::~WiFiConn()
{
	end();
}

// Start WiFi connection to the last used access point .
wl_status_t WiFiConn::begin(uint32_t timeout)
{
	wl_status_t status = WL_NO_WIFI_MODULE_COMM;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(CONNECT_CMD);
		_requestPacket.appendDWord(timeout);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, timeout + WL_SCAN_NETWORKS_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				status = (wl_status_t) value;
			}
		}
	}

	return status;
}

// Start WiFi connection for OPEN networks
wl_status_t WiFiConn::begin(String ssid, uint32_t timeout)
{
	wl_status_t status = WL_NO_WIFI_MODULE_COMM;

	// Initialize internal member .
	if (_init() == true)
	{
		// Check input arguments .
		if (ssid != nullptr)
		{
			// Prepare the request .
			_requestPacket.beginEdit(CONNECT_CMD);
			_requestPacket.appendString(ssid.c_str());
			_requestPacket.appendDWord(timeout);
			_requestPacket.endEdit();

			if (_espiMaster.transfer(&_requestPacket, &_answerPacket, timeout + WL_DEFAULT_TIMEOUT) == true)
			{
				uint8_t value = 0;

				// Try to decode the answer .
				if (_answerPacket.readByte(0, &value) > 0)
				{
					status = (wl_status_t) value;
				}
			}
		}
		else
		{
			status = WL_NO_SSID_AVAIL;
		}
	}

	return status;
}

// Start WiFi connection with passphrase .
wl_status_t WiFiConn::begin(String ssid, String passphrase, uint32_t timeout)
{
	wl_status_t status = WL_NO_WIFI_MODULE_COMM;

	// Initialize internal member .
	if (_init() == true)
	{
		// Check input arguments .
		if (ssid != nullptr)
		{
			// Prepare the request .
			_requestPacket.beginEdit(CONNECT_CMD);
			_requestPacket.appendString(ssid.c_str());
			_requestPacket.appendString(passphrase.c_str());
			_requestPacket.appendDWord(timeout);
			_requestPacket.endEdit();

			if (_espiMaster.transfer(&_requestPacket, &_answerPacket, timeout + WL_DEFAULT_TIMEOUT) == true)
			{
				uint8_t value = 0;

				// Try to decode the answer .
				if (_answerPacket.readByte(0, &value) > 0)
				{
					status = (wl_status_t) value;
				}
			}
		}
		else
		{
			status = WL_NO_SSID_AVAIL;
		}
	}

	return status;
}

// Terminate the connection and release all resources .
void WiFiConn::end()
{
	_deInit();
}

// Disconnect from the network
bool WiFiConn::disconnect(bool wifiOff)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(DISCONNECT_CMD);
		_requestPacket.appendBoolean(wifiOff);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Indicate if the system is connected to a network .
bool WiFiConn::isConnected()
{
	return (status() == WL_CONNECTED);
}

// Return connection status .
wl_status_t WiFiConn::status()
{
	wl_status_t status = WL_NO_WIFI_MODULE_COMM;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CONN_STATUS_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				status = (wl_status_t) value;
			}
		}
	}

	return status;
}

// Get firmware version .
String WiFiConn::firmwareVersion()
{
	// Initialize internal member data if necessary .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_FW_VERSION_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t major = 0;
			uint8_t minor = 0;
			uint16_t patch = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &major) > 0 &&
				_answerPacket.readByte(1, &minor) > 0 &&
				_answerPacket.readWord(2, &patch) > 0 )
			{
				char versionStr[16] = { 0 };

				sprintf(versionStr, "%d.%d.%d", major, minor, patch);

				return String(versionStr);
			}
		}
	}

	// Return an empty string if an error occurs .
	return String();
}

// Get firmware version .
bool WiFiConn::firmwareVersion(uint8_t &major, uint8_t &minor, uint16_t &patch)
{
	// Initialize internal member data if necessary .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_FW_VERSION_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t majorNumber = 0;
			uint8_t minorNumber = 0;
			uint16_t patchNumber = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &majorNumber) > 0 &&
				_answerPacket.readByte(1, &minorNumber) > 0 &&
				_answerPacket.readWord(2, &patchNumber) > 0 )
			{
				major = majorNumber;
				minor = minorNumber;
				patch = patchNumber;

				return true;
			}
		}
	}

	// Return an empty string if an error occurs .
	return false;
}

// Check if the firmware version of ESP module is compatible with the library .
bool WiFiConn::checkFirmwareVersion()
{
	uint8_t major = 0;
	uint8_t minor = 0;
	uint16_t patch = 0;

	/* This line is necessary to avoid compilation warning 'comparison always true'
	 * if WIFI_FIRMWARE_REQUIRED_MINOR is equal to zero .
	 */
	uint8_t requiredMinor = WIFI_FIRMWARE_REQUIRED_MINOR;

	if (firmwareVersion(major, minor, patch) == true)
	{
		if (major == WIFI_FIRMWARE_REQUIRED_MAJOR && minor >= requiredMinor)
		{
			return true;
		}
	}

	return false;
}

// Check if the firmware version of ESP module is compatible with the library (DEPRECATED) .
bool WiFiConn::checkFirmwareVersion(String firmwareVersionRequired)
{
	(void)firmwareVersionRequired;

	return checkFirmwareVersion();
}

// Change IP configuration settings disabling the DHCP client .
bool WiFiConn::config(IPAddress localIP, IPAddress gateway, IPAddress subnet, IPAddress dnsServer1, IPAddress dnsServer2)
{
	bool success = false;

	// Initialize internal member data if necessary .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(SET_IP_CONFIG_CMD);
		_requestPacket.appendDWord(localIP);
		_requestPacket.appendDWord(gateway);
		_requestPacket.appendDWord(subnet);
		_requestPacket.appendDWord(dnsServer1);
		_requestPacket.appendDWord(dnsServer2);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Change DNS IP configuration .
bool WiFiConn::setDNS(IPAddress dnsServer1, IPAddress dnsServer2)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(SET_DNS_CONFIG_CMD);
		_requestPacket.appendDWord(dnsServer1);
		_requestPacket.appendDWord(dnsServer2);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Set the DHCP hostname .
bool WiFiConn::setHostname(String hostname)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(SET_HOSTNAME_CMD);
		_requestPacket.appendString(hostname.c_str());
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Get the interface IP address.
IPAddress WiFiConn::localIP()
{
	IPAddress ip;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_LOCAL_IPADDR_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				ip = value;
			}
		}
	}

	return ip;
}

// Get the gateway IP address.
IPAddress WiFiConn::gatewayIP()
{
	IPAddress gateway;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_GATEWAY_IPADDR_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				gateway = value;
			}
		}
	}

	return gateway;
}

// Get the interface sub-net mask address.
IPAddress WiFiConn::subnetMask()
{
	IPAddress subnet;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_SUBNET_MASK_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				// Sub-net mask is the second of the three parameters returned with GET_IPADDR_CMD command .
				subnet = value;
			}
		}
	}

	return subnet;
}

// Get the interface MAC address.
uint8_t* WiFiConn::macAddress(uint8_t* mac)
{
	if (mac != nullptr)
	{
		bool success = false;

		if (_isDeployed == true)
		{
			// Prepare the request .
			_requestPacket.beginEdit(GET_MACADDR_CMD);
			_requestPacket.endEdit();

			if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
			{
				// Try to decode the answer .
				if (_answerPacket.readBuffer(0, mac, WL_MAC_ADDR_LENGTH) == 6)
				{
					success = true;
				}
			}
		}

		if (success == false)
		{
			// Fill MAC address with zeros .
			memset(mac, 0, WL_MAC_ADDR_LENGTH);
		}
	}

	return mac;
}

// Get the interface MAC address.
String WiFiConn::macAddress()
{
    uint8_t mac[6];
    char macStr[18] = { 0 };

    macAddress(mac);

    sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    return String(macStr);
}

// Get the DNS IP address .
IPAddress WiFiConn::dnsIP(uint8_t dnsNumber)
{
	IPAddress dnsServer;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_DNS_IPADDR_CMD);
		_requestPacket.appendByte(dnsNumber);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				dnsServer = value;
			}
		}
	}

	return dnsServer;
}

// Get the DHCP hostname .
String WiFiConn::hostname()
{
	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_HOSTNAME_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			const char *hostname = _answerPacket.getString(0);

			if (hostname != nullptr)
			{
				return String(hostname);
			}
		}
	}

	return String();
}

// Resolve the given hostname to an IP address .
bool WiFiConn::hostByName(String hostname, IPAddress& hostIP, uint32_t timeout_ms)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_HOST_BY_NAME_CMD);
		_requestPacket.appendString(hostname.c_str());
		_requestPacket.appendDWord(timeout_ms - WL_DEFAULT_TIMEOUT);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, timeout_ms) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &success) > 0 &&_answerPacket.readDWord(1, &value) > 0)
			{
				if (success == true)
				{
					hostIP = value;
				}
			}
			else
			{
				success = false;
			}
		}
	}

	return success;
}

// Return the current SSID (Service Set Identifier) associated with the network .
String WiFiConn::SSID()
{
	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_SSID_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			const char *ssid = _answerPacket.getString(0);

			if (ssid != nullptr)
			{
				return String(ssid);
			}
		}
	}

	return String();
}

// Return the Encryption Type associated with the network .
uint8_t	WiFiConn::encryptionType()
{
	uint8_t encryption = ENC_TYPE_UNKNOWN;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_ENCT_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				encryption = value;
			}
		}
	}

	return encryption;
}

// Return the current RSSI (Received Signal Strength) in dBm .
int32_t WiFiConn::RSSI()
{
	int32_t rssi = 0;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_RSSI_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				rssi = (int32_t)value;
			}
		}
	}

	return rssi;
}

// Return the current BSSID (Basic Service Set Identifier) associated with the network .
uint8_t* WiFiConn::BSSID(uint8_t* bssid)
{
	if (bssid != nullptr)
	{
		bool success = false;

		if (_isDeployed == true)
		{
			// Prepare the request .
			_requestPacket.beginEdit(GET_CURR_BSSID_CMD);
			_requestPacket.endEdit();

			if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
			{
				// Try to decode the answer .
				if (_answerPacket.readBuffer(0, bssid, WL_MAC_ADDR_LENGTH) == 6)
				{
					success = true;
				}
			}
		}

		if (success == false)
		{
			// Fill BSSID with 0xff .
			memset(bssid, 0xff, WL_MAC_ADDR_LENGTH);
		}
	}

	return bssid;
}

// Return the current BSSID (Basic Service Set Identifier) associated with the network .
String WiFiConn::BSSID()
{
    uint8_t bssid[6];
    char bssidStr[18] = { 0 };

    BSSID(bssid);

    sprintf(bssidStr, "%02X:%02X:%02X:%02X:%02X:%02X", bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5]);

    return String(bssidStr);
}

// Return the current channel associated with the network .
uint8_t WiFiConn::channel()
{
	uint8_t channel = 0;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_CHANNEL_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				channel = value;
			}
		}
	}

	return channel;
}

// Return all informations associated with the network .
bool WiFiConn::getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi)
{
	bool result = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_NET_INFO_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the result of the command .
			_answerPacket.readBoolean(0, &result);

			if (result == true)
			{
				bool validAnswer = true;
				uint32_t value = 0;

				// Decode SSID .
				if (_answerPacket.paramLength(1) > 1)
				{
					ssid = _answerPacket.getString(1);
				}
				else
				{
					validAnswer = false;
				}

				// Decode encryption type .
				if (_answerPacket.readByte(2, &encryption) != 1)
				{
					validAnswer = false;
				}

				// Decode RSSI .
				if (_answerPacket.readDWord(3, &value) == 4)
				{
					rssi = (int32_t)value;
				}
				else
				{
					validAnswer = false;
				}

				if (validAnswer != true)
				{
					result = false;
				}
			}
		}
	}

	return result;
}

// Return all informations associated with the network .
bool WiFiConn::getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi, String &bssid, uint8_t &channel, bool &isHidden)
{
	 uint8_t bssidArray[6];

	if (getNetworkInfo(ssid, encryption, rssi, bssidArray, channel, isHidden) == true)
	{
		char bssidStr[18] = { 0 };

		sprintf(bssidStr, "%02X:%02X:%02X:%02X:%02X:%02X", bssidArray[0], bssidArray[1], bssidArray[2], bssidArray[3], bssidArray[4], bssidArray[5]);

		bssid = bssidStr;

		return true;
	}

	return false;
}

// Return all informations associated with the network .
bool WiFiConn::getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi, uint8_t *bssid, uint8_t &channel, bool &isHidden)
{
	bool result = false;

	if (_isDeployed == true && bssid != nullptr)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CURR_NET_INFO_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the result of the command .
			_answerPacket.readBoolean(0, &result);

			if (result == true)
			{
				bool validAnswer = true;
				uint32_t value = 0;

				// Decode SSID .
				if (_answerPacket.paramLength(1) > 1)
				{
					ssid = _answerPacket.getString(1);
				}
				else
				{
					validAnswer = false;
				}

				// Decode encryption type .
				if (_answerPacket.readByte(2, &encryption) != 1)
				{
					validAnswer = false;
				}

				// Decode RSSI .
				if (_answerPacket.readDWord(3, &value) == 4)
				{
					rssi = (int32_t)value;
				}
				else
				{
					validAnswer = false;
				}

				// Decode BSSID .
				if (_answerPacket.readBuffer(4, bssid, WL_MAC_ADDR_LENGTH) != 6)
				{
					validAnswer = false;
				}

				// Decode Channel .
				if (_answerPacket.readByte(5, &channel) != 1)
				{
					validAnswer = false;
				}

				// Decode hidden flag .
				if (_answerPacket.readBoolean(6, &isHidden) != 1)
				{
					validAnswer = false;
				}

				if (validAnswer != true)
				{
					result = false;
				}
			}
		}
	}

	return result;
}

// Scan available WiFi networks .
int8_t WiFiConn::scanNetworks(bool showHidden)
{
	int8_t scanResult = WL_SCAN_FAILED;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(SCAN_NETWORKS_CMD);
		_requestPacket.appendBoolean(showHidden);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_SCAN_NETWORKS_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				scanResult = value;
			}
		}
	}

	return scanResult;
}

// Return the SSID discovered during the network scan .
String WiFiConn::SSID(uint8_t networkItem)
{
	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_SSID_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			const char *ssid = _answerPacket.getString(0);

			if (ssid != nullptr)
			{
				return String(ssid);
			}
		}
	}

	return String();
}

// Return the encryption type of the networks discovered during the scanNetworks
uint8_t	WiFiConn::encryptionType(uint8_t networkItem)
{
	uint8_t encryption = ENC_TYPE_UNKNOWN;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_ENCT_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				encryption = value;
			}
		}
	}

	return encryption;
}

// Return the RSSI of the networks discovered during the scanNetworks .
int32_t WiFiConn::RSSI(uint8_t networkItem)
{
	int32_t rssi = 0;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_RSSI_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &value) > 0)
			{
				rssi = (int32_t)value;
			}
		}
	}

	return rssi;
}

// Return the BSSID of the networks discovered during the scanNetworks .
uint8_t* WiFiConn::BSSID(uint8_t networkItem, uint8_t* bssid)
{
	if (bssid != nullptr)
	{
		bool success = false;

		if (_isDeployed == true)
		{
			// Prepare the request .
			_requestPacket.beginEdit(GET_IDX_BSSID_CMD);
			_requestPacket.appendByte(networkItem);
			_requestPacket.endEdit();

			if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
			{
				// Try to decode the answer .
				if (_answerPacket.readBuffer(0, bssid, WL_MAC_ADDR_LENGTH) == 6)
				{
					success = true;
				}
			}
		}

		if (success == false)
		{
			// Fill BSSID with 0xff .
			memset(bssid, 0xff, WL_MAC_ADDR_LENGTH);
		}
	}

	return bssid;
}

// Return the BSSID of the networks discovered during the scanNetworks .
String WiFiConn::BSSID(uint8_t networkItem)
{
    uint8_t bssid[6];
    char bssidStr[18] = { 0 };

    BSSID(networkItem, bssid);

    sprintf(bssidStr, "%02X:%02X:%02X:%02X:%02X:%02X", bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5]);

    return String(bssidStr);
}

// Return the channel of the networks discovered during the scanNetworks .
uint8_t WiFiConn::channel(uint8_t networkItem)
{
	uint8_t channel = 0;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_CHANNEL_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				channel = value;
			}
		}
	}

	return channel;
}

// Return all informations about a scanned WiFi network .
bool WiFiConn::getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi)
{
	bool result = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_NET_INFO_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the result of the command .
			_answerPacket.readBoolean(0, &result);

			if (result == true)
			{
				bool validAnswer = true;
				uint32_t value = 0;

				// Decode SSID .
				if (_answerPacket.paramLength(1) > 1)
				{
					ssid = _answerPacket.getString(1);
				}
				else
				{
					validAnswer = false;
				}

				// Decode encryption type .
				if (_answerPacket.readByte(2, &encryption) != 1)
				{
					validAnswer = false;
				}

				// Decode RSSI .
				if (_answerPacket.readDWord(3, &value) == 4)
				{
					rssi = (int32_t)value;
				}
				else
				{
					validAnswer = false;
				}

				if (validAnswer != true)
				{
					result = false;
				}
			}
		}
	}

	return result;
}

// Return all informations about a scanned WiFi network .
bool WiFiConn::getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi, String &bssid, uint8_t &channel, bool &isHidden)
{
	 uint8_t bssidArray[6];

	if (getNetworkInfo(networkItem, ssid, encryption, rssi, bssidArray, channel, isHidden) == true)
	{
		char bssidStr[18] = { 0 };

		sprintf(bssidStr, "%02X:%02X:%02X:%02X:%02X:%02X", bssidArray[0], bssidArray[1], bssidArray[2], bssidArray[3], bssidArray[4], bssidArray[5]);

		bssid = bssidStr;

		return true;
	}

	return false;
}

// Return all informations about a scanned WiFi network .
bool WiFiConn::getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi, uint8_t *bssid, uint8_t &channel, bool &isHidden)
{
	bool result = false;

	if (_isDeployed == true && bssid != nullptr)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_IDX_NET_INFO_CMD);
		_requestPacket.appendByte(networkItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the result of the command .
			_answerPacket.readBoolean(0, &result);

			if (result == true)
			{
				bool validAnswer = true;
				uint32_t value = 0;

				// Decode SSID .
				if (_answerPacket.paramLength(1) > 1)
				{
					ssid = _answerPacket.getString(1);
				}
				else
				{
					validAnswer = false;
				}

				// Decode encryption type .
				if (_answerPacket.readByte(2, &encryption) != 1)
				{
					validAnswer = false;
				}

				// Decode RSSI .
				if (_answerPacket.readDWord(3, &value) == 4)
				{
					rssi = (int32_t)value;
				}
				else
				{
					validAnswer = false;
				}

				// Decode BSSID .
				if (_answerPacket.readBuffer(4, bssid, WL_MAC_ADDR_LENGTH) != 6)
				{
					validAnswer = false;
				}

				// Decode Channel .
				if (_answerPacket.readByte(5, &channel) != 1)
				{
					validAnswer = false;
				}

				// Decode hidden flag .
				if (_answerPacket.readBoolean(6, &isHidden) != 1)
				{
					validAnswer = false;
				}

				if (validAnswer != true)
				{
					result = false;
				}
			}
		}
	}

	return result;
}

//  Return the number of access points stored in the non-volatile memory of the WiFi module .
uint8_t WiFiConn::getNumberOfStoredAccessPoints()
{
	uint8_t numOfAccessPoints = 0;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_NUM_STORED_AP_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				numOfAccessPoints = value;
			}
		}
	}

	return numOfAccessPoints;
}

// Return the SSID (Service Set Identifier) stored in the non-volatile memory of the WiFi module .
String WiFiConn::storeSSID(uint8_t accessPointItem)
{
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_STORED_SSID_CMD);
		_requestPacket.appendByte(accessPointItem);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			const char *ssid = _answerPacket.getString(0);

			if (ssid != nullptr)
			{
				return String(ssid);
			}
		}
	}

	return String();
}

// Clear all Access Points stored in the non-volatile memory of the WiFi module .
bool WiFiConn::clearStoredAccessPoints()
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(CLEAR_STORED_AP_CMD);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			success = true;
		}
	}

	return success;
}

/*
 * Inform the WiFi module about the current running sketch .
 *
 * param sketchName: name of the current running sketch (Max 32 chars) .
 *
 * return: 'true' on success, 'false' otherwise
 */
bool WiFiConn::setSketchName(String sketchName)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		if (sketchName)


		// Prepare the request .
		_requestPacket.beginEdit(SET_SKETCH_NAME_CMD);
		_requestPacket.appendString(sketchName.c_str());
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			success = true;
		}
	}

	return success;
}

/*
 * Reset the WiFi module .
 */
void WiFiConn::reset()
{
	sleep();

	delay(100);

	wake();
}


/*
 * Turn on the WiFi module .
 */
void WiFiConn::sleep()
{
	end();

	_sleep();
}

/*
 * Turn off the WiFi module .
 */
void WiFiConn::wake()
{
	_wake();
}

/*******************************************************************************
    Protected methods implementation .
*******************************************************************************/

// Acquire the first available socket .
uint8_t WiFiConn::acquireServerSocket()
{
	for(uint8_t sock = 0; sock < MAX_SOCK_NUM; sock++)
	{
		if (_serverSocketInUse[sock] == false)
		{
			_serverSocketInUse[sock] = true;

			return sock;
		}
	}

	return SOCK_NOT_AVAIL;
}

// Release a socket .
bool WiFiConn::releaseServerSocket(uint8_t sock)
{
	if (sock < MAX_SOCK_NUM)
	{
		if (_serverSocketInUse[sock] == true)
		{
			_serverSocketInUse[sock] = false;

			return true;
		}
	}

	return false;
}

// Start a TCP server on a specified port .
bool WiFiConn::startServer(uint8_t sock, IPAddress ipAddress, uint16_t port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(START_SERVER_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendDWord(ipAddress);
		_requestPacket.appendWord(port);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Stop a TCP server .
bool WiFiConn::stopServer(uint8_t sock)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(STOP_SERVER_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Get TCP server state .
uint8_t WiFiConn::getServerState(uint8_t sock)
{
	uint8_t state = (uint8_t)(wl_tcp_states::CLOSED);

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_SERVER_STATE_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				state = value;
			}
		}
	}

	return state;
}

// Get a new client from the server connection .
bool WiFiConn::getServerClient(uint8_t sock, uint8_t newClientSock)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_SERVER_CLIENT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendByte(newClientSock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Acquire the first available socket .
uint8_t WiFiConn::acquireClientSocket()
{
	for(uint8_t sock = 0; sock < MAX_SOCK_NUM; sock++)
	{
		if (_clientSocketInUse[sock] == false)
		{
			_clientSocketInUse[sock] = true;

			return sock;
		}
	}

	return SOCK_NOT_AVAIL;
}

// Release a socket .
bool WiFiConn::releaseClientSocket(uint8_t sock)
{
	if (sock < MAX_SOCK_NUM)
	{
		if (_clientSocketInUse[sock] == true)
		{
			_clientSocketInUse[sock] = false;

			return true;
		}
	}

	return false;
}

// Start a TCP client on a specified port .
bool WiFiConn::startClient(uint8_t sock, IPAddress ipAddress, uint16_t port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(START_CLIENT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendDWord(ipAddress);
		_requestPacket.appendWord(port);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Stop a TCP client .
bool WiFiConn::stopClient(uint8_t sock)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(STOP_CLIENT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Get TCP client state .
uint8_t WiFiConn::getClientState(uint8_t sock)
{
	uint8_t state = (uint8_t)(wl_tcp_states::CLOSED);

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CLIENT_STATE_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				state = value;
			}
		}
	}

	return state;
}

// Get TCP client available data .
uint16_t WiFiConn::getClientAvailableData(uint8_t sock)
{
	uint16_t available = 0;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CLIENT_AVAIL_DATA_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint16_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readWord(0, &value) > 0)
			{
				available = value;
			}
		}
	}

	return available;
}

// Get client connection state and informations .
bool WiFiConn::getClientStateInfo(uint8_t sock, uint8_t &state, uint16_t &availForRead, uint16_t &availForWrite)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_CLIENT_STATE_INFO_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			if ( _answerPacket.readByte(0, &state) > 0 			&&
				 _answerPacket.readWord(1, &availForRead)  > 0 	&&
				 _answerPacket.readWord(2, &availForWrite) > 0 	)
			{
				success = true;
			}
		}
	}

	if (success == false)
	{
		state = (uint8_t)(wl_tcp_states::CLOSED);
		availForRead = 0;
		availForWrite = 0;
	}

	return success;
}

// Peek client data .
int WiFiConn::peekClient(uint8_t sock)
{
	int data = EOF;		// EOF = no data available .

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(PEEK_CLIENT_DATA_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				data = (int)value;
			}
		}
	}

	return data;
}

// Read client data .
int WiFiConn::readClient(uint8_t sock)
{
	int data = EOF;		// EOF = no data available .

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(READ_CLIENT_DATA_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint8_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readByte(0, &value) > 0)
			{
				data = (int)value;
			}
		}
	}

	return data;
}

// Read client buffer .
uint16_t WiFiConn::readClientBuffer(uint8_t sock, uint8_t *data, uint16_t length)
{
	uint16_t dataCount = 0;

	// Check input arguments .
	if (_isDeployed == true && data != nullptr && length > 0)
	{
		// Clamp buffer length to the maximum value .
		if (length > (WL_ANS_BUFFER_SIZE - 10))
		{
			length = (WL_ANS_BUFFER_SIZE - 10);
		}

		// Prepare the request .
		_requestPacket.beginEdit(READ_CLIENT_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendWord(length);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			dataCount = _answerPacket.readBuffer(0, data, length);
		}
	}

	return dataCount;
}

// Write client buffer .
uint16_t WiFiConn::writeClientBuffer(uint8_t sock, uint8_t *data, uint16_t length)
{
	uint16_t dataCount = 0;

	// Check input arguments .
	if (_isDeployed == true && data != nullptr && length > 0)
	{
		// Clamp buffer length to the maximum value .
		if (length > (WL_REQ_BUFFER_SIZE - 10))
		{
			length = (WL_REQ_BUFFER_SIZE - 10);
		}

		// Prepare the request .
		_requestPacket.beginEdit(WRITE_CLIENT_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendBuffer(data, length);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_WRITE_TIMEOUT) == true)
		{
			// Try to decode the answer .
			if (_answerPacket.readWord(0, &length) > 0)
			{
				dataCount = length;
			}
		}
	}

	return dataCount;
}

// Flush client buffer .
bool WiFiConn::emptyFlushClientBuffer(uint8_t sock, bool rx, bool tx)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(EMPTY_FLUSH_CLIENT_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendBoolean(rx);
		_requestPacket.appendBoolean(tx);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_FLUSH_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Acquire the first available socket .
uint8_t WiFiConn::acquireUdpSocket()
{
	for(uint8_t sock = 0; sock < MAX_SOCK_NUM; sock++)
	{
		if (_udpSocketInUse[sock] == false)
		{
			_udpSocketInUse[sock] = true;

			return sock;
		}
	}

	return SOCK_NOT_AVAIL;
}

// Release a socket .
bool WiFiConn::releaseUdpSocket(uint8_t sock)
{
	if (sock < MAX_SOCK_NUM)
	{
		if (_udpSocketInUse[sock] == true)
		{
			_udpSocketInUse[sock] = false;

			return true;
		}
	}

	return false;
}

// Start a UDP socket on a specified port .
bool WiFiConn::startUdp(uint8_t sock, uint16_t port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(START_UDP_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendWord(port);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Stop a UDP server .
bool WiFiConn::stopUdp(uint8_t sock)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(STOP_UDP_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Start building up an UDP packet .
bool WiFiConn::beginUdpPacket(uint8_t sock, IPAddress ipAddress, uint16_t port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(BEGIN_UDP_PKT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendDWord(ipAddress);
		_requestPacket.appendWord(port);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Finish off the packet and send it .
bool WiFiConn::endUdpPacket(uint8_t sock)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(END_UDP_PKT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_FLUSH_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Parse the next incoming UDP packet and return its size in bytes .
uint16_t WiFiConn::parseUdpPacket(uint8_t sock)
{
	uint16_t packetSize = 0;

	if (_isDeployed == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(PARSE_UDP_PKT_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint16_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readWord(0, &value) > 0)
			{
				packetSize = value;
			}
		}
	}

	return packetSize;
}

// Get UDP available data .
uint16_t WiFiConn::getUdpAvailableData(uint8_t sock)
{
	uint16_t available = 0;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_UDP_AVAIL_DATA_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint16_t value = 0;

			// Try to decode the answer .
			if (_answerPacket.readWord(0, &value) > 0)
			{
				available = value;
			}
		}
	}

	return available;
}


// Read UDP buffer .
uint16_t WiFiConn::readUdpBuffer(uint8_t sock, uint8_t *data, uint16_t length)
{
	uint16_t dataCount = 0;

	// Check input arguments .
	if (_isDeployed == true && data != nullptr && length > 0)
	{
		// Clamp buffer length to the maximum value .
		if (length > (WL_ANS_BUFFER_SIZE - 10))
		{
			length = (WL_ANS_BUFFER_SIZE - 10);
		}

		// Prepare the request .
		_requestPacket.beginEdit(READ_UDP_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendWord(length);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			dataCount = _answerPacket.readBuffer(0, data, length);
		}
	}

	return dataCount;
}

// Write UDP buffer .
uint16_t WiFiConn::writeUdpBuffer(uint8_t sock, uint8_t *data, uint16_t length)
{
	uint16_t dataCount = 0;

	// Check input arguments .
	if (_isDeployed == true && data != nullptr && length > 0)
	{
		// Clamp buffer length to the maximum value .
		if (length > (WL_REQ_BUFFER_SIZE - 10))
		{
			length = (WL_REQ_BUFFER_SIZE - 10);
		}

		// Prepare the request .
		_requestPacket.beginEdit(WRITE_UDP_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendBuffer(data, length);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			// Try to decode the answer .
			if (_answerPacket.readWord(0, &length) > 0)
			{
				dataCount = length;
			}
		}
	}

	return dataCount;
}

// Empty/Flush UDP packet buffers .
bool WiFiConn::emptyFlushUdpBuffer(uint8_t sock, bool rx, bool tx)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(EMPTY_FLUSH_UDP_BUFF_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.appendBoolean(rx);
		_requestPacket.appendBoolean(tx);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_FLUSH_TIMEOUT) == true)
		{
			bool result = false;

			// Try to decode the answer .
			if (_answerPacket.readBoolean(0, &result) > 0)
			{
				success = result;
			}
		}
	}

	return success;
}

// Retrieve remote IP address and port .
bool WiFiConn::getUdpRemoteInfo(uint8_t sock, IPAddress &ip, uint16_t &port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_UDP_REMOTE_INFO_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t param0 = 0;
			uint16_t param1 = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &param0) > 0 && _answerPacket.readWord(1, &param1) > 0)
			{
				ip = param0;
				port = param1;

				success = true;
			}
		}
	}

	return success;
}

// Retrieve local IP address and port .
bool WiFiConn::getUdpLocalInfo(uint8_t sock, IPAddress &ip, uint16_t &port)
{
	bool success = false;

	// Initialize internal member .
	if (_init() == true)
	{
		// Prepare the request .
		_requestPacket.beginEdit(GET_UDP_LOCAL_INFO_CMD);
		_requestPacket.appendByte(sock);
		_requestPacket.endEdit();

		if (_espiMaster.transfer(&_requestPacket, &_answerPacket, WL_DEFAULT_TIMEOUT) == true)
		{
			uint32_t param0 = 0;
			uint16_t param1 = 0;

			// Try to decode the answer .
			if (_answerPacket.readDWord(0, &param0) > 0 && _answerPacket.readWord(1, &param1) > 0)
			{
				ip = param0;
				port = param1;

				success = true;
			}
		}
	}

	return success;
}

/*******************************************************************************
    Private methods implementation .
*******************************************************************************/

// Initialize internal member data .
bool WiFiConn::_init()
{
	if (_isDeployed == false)
	{
		// Wake-up the WiFi module if it's in sleep mode .
		_wake();

		if (_espiMaster.begin() == true)
		{
			// Set the event callback handler .
			//_espiMaster.setEventCallback(eventHandler, &EvtPacket);

#ifdef WIFI_CONN_DEBUG_ESPI

			// Set the error callback handler .
			_espiMaster.setErrorCallback(this, (EspiMaster::EspiErrorMethod) &WiFiConn::_errorHandler);

#endif // WIFI_CONN_DEBUG_ESPI

			_isDeployed = true;
		}
	}

	return _isDeployed;
}

// Release resource and reset internal member data to default values .
void WiFiConn::_deInit()
{
	if (_isDeployed == true)
	{
		// Disconnect station from the network .
		disconnect(true);

		_espiMaster.end();

		_isDeployed = false;
	}
}

// Turn on the WiFi module .
void WiFiConn::_sleep()
{

#if defined(WHATSNEXT_RED)

	uint8_t uartCtrlReg = UCSR0B;
	uint8_t ddrdReg = DDRD;

	UCSR0B &= ~((1<<TXEN0)|(1<<RXEN0));
	DDRD |= (1<<0) | (1<<1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, LOW);
	delayMicroseconds(3);
	PORTD &= ~((1<<0) | (1<<1));
	delay(1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, HIGH);
	delayMicroseconds(3);
	PORTD |= ((1<<0) | (1<<1));
	delay(2);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, LOW);
	delayMicroseconds(3);
	PORTD &= ~((1<<0) | (1<<1));
	delay(1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, HIGH);
	delayMicroseconds(3);
	PORTD |= ((1<<0) | (1<<1));

	DDRD = ddrdReg;
	UCSR0B = uartCtrlReg;

#else

	pinMode(WIFI_CONN_SLAVE_RESET, OUTPUT);
	digitalWrite(WIFI_CONN_SLAVE_RESET, LOW);

#endif

}

// Turn off the WiFi module .
void WiFiConn::_wake()
{

#if defined(WHATSNEXT_RED)

	uint8_t uartCtrlReg = UCSR0B;
	uint8_t ddrdReg = DDRD;

	UCSR0B &= ~((1<<TXEN0)|(1<<RXEN0));
	DDRD |= (1<<0) | (1<<1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, LOW);
	delayMicroseconds(3);
	PORTD &= ~((1<<0) | (1<<1));
	delay(1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, HIGH);
	delayMicroseconds(3);
	PORTD |= ((1<<0) | (1<<1));
	delay(1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, LOW);
	delayMicroseconds(3);
	PORTD &= ~((1<<0) | (1<<1));
	delay(1);

	digitalWrite(WIFI_CONN_SLAVE_SELECT, HIGH);
	delayMicroseconds(3);
	PORTD |= ((1<<0) | (1<<1));

	DDRD = ddrdReg;
	UCSR0B = uartCtrlReg;

#else

	// Configure SLAVE_RESET as input . The pin goes high because there is a pull-up resistor .
	pinMode(WIFI_CONN_SLAVE_RESET, INPUT);

#endif

}

// Print error message when it occurs .
void WiFiConn::_errorHandler(uint32_t errorCode)
{
	Serial.print((String)millis() + " - ");

	switch (errorCode)
	{
		case EspiMaster::ERR_INVALID_PACKET_HEADER:
		{
			Serial.println("Espi error: INVALID PACKET HEADER");
			break;
		}
		case EspiMaster::ERR_INVALID_PACKET_TAIL:
		{
			Serial.println("Espi error: INVALID PACKET TAIL");
			break;
		}
		case EspiMaster::ERR_BUS_COLLISION:
		{
			Serial.println("Espi error: BUS COLLISION");
			break;
		}
		case EspiMaster::ERR_SYNC_SR_ALREADY_ACTIVE:
		{
			Serial.println("Espi error: SYNC SR ALREADY ACTIVE");
			break;
		}
		case EspiMaster::ERR_SYNC_SR_HIGH_TIMEOUT:
		{
			Serial.println("Espi error: SYNC SR HIGH TIMEOUT");
			break;
		}
		case EspiMaster::ERR_SYNC_SR_LOW_TIMEOUT:
		{
			Serial.println("Espi error: SYNC SR LOW TIMEOUT");
			break;
		}
		case EspiMaster::ERR_ANSWER_TIMEOUT:
		{
			Serial.println("Espi error: ANSWER TIMEOUT");
			break;
		}
		default:
		{
			Serial.println("Espi error: UNKNOWN");
			break;
		}
	}
}

/******************************************************************************/


