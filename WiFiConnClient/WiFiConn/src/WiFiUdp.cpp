/*
 * WiFiUdp.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConn.h"

#include "WiFiUdp.h"

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/

// Constructor .
WiFiUdp::WiFiUdp() :
	_context(nullptr)
{

}

// Constructor .
WiFiUdp::WiFiUdp(const WiFiUdp & other)
{
	_context = other._context;

	if (_context != nullptr)
	{
	    _context->ref();
	}
}

// Destructor .
WiFiUdp::~WiFiUdp()
{
	if (_context != nullptr)
	{
		_context->unref();
	}
}

// Assignment operator .
WiFiUdp& WiFiUdp::operator=(WiFiUdp& other)
{
	if (_context != nullptr)
	{
		_context->unref();
	}

	_context = other._context;

	if (_context != nullptr)
	{
		_context->ref();
	}

    return *this;
}

// Start WiFiUDP socket, listening at local port .
uint8_t WiFiUdp::begin(uint16_t port)
{
	if (_context != nullptr)
	{
		_context->unref();
	}

	_context = new Context();

	_context->ref();

	if (_context->begin(port) == 0)
	{
		_context->unref();

		_context = nullptr;

		return 0;
	}

	return 1;
}

// Finish with the UDP socket .
void WiFiUdp::stop()
{
	if (_context != nullptr)
	{
		_context->stop();
	}
}

// Start building up a packet to send to the remote host specific in IP and port .
int WiFiUdp::beginPacket(IPAddress ip, uint16_t port)
{
	if (_context == nullptr)
	{
		_context = new Context();

		_context->ref();
	}

	return _context->beginPacket(ip, port);
}

// Start building up a packet to send to the remote host specific in host and port .
int WiFiUdp::beginPacket(const char *host, uint16_t port)
{
	IPAddress hostIP;

	if (WiFi.hostByName(host, hostIP) == true)
	{
		return beginPacket(hostIP, port);
	}

	return 0;
}

// Finish off this packet and send it .
int WiFiUdp::endPacket()
{
	if (_context != nullptr)
	{
		return _context->endPacket();
	}

	return 0;
}

// Write a single byte into the packet .
size_t WiFiUdp::write(uint8_t b)
{
	return write(&b, 1);
}

// Write size bytes from buffer into the packet .
size_t WiFiUdp::write(const uint8_t *buffer, size_t length)
{
	if (_context != nullptr)
	{
		return _context->write(buffer, length);
	}

	return 0;
}

// Start processing the next available incoming packet .
int WiFiUdp::parsePacket()
{
	if (_context != nullptr)
	{
		return _context->parsePacket();
	}

	return 0;
}

// Number of bytes remaining in the current packet .
int WiFiUdp::available()
{
	if (_context != nullptr)
	{
		return _context->available();
	}

	return 0;
}

// Read a single byte from the current packet .
int WiFiUdp::read()
{
	if (_context != nullptr)
	{
		return _context->read();
	}

	return EOF;
}

// Read up to 'length' bytes from the current packet and place them into buffer .
int WiFiUdp::read(unsigned char* buffer, size_t length)
{
	if (_context != nullptr)
	{
		return _context->read(buffer, length);
	}

	return 0;
}

// Return the next byte from the current packet without moving on to the next byte .
int WiFiUdp::peek()
{
	if (_context != nullptr)
	{
		return _context->peek();
	}

	return EOF;
}

// Finish sending the current packet .
void WiFiUdp::flush()
{
	if (_context != nullptr)
	{
		_context->flush();
	}
}

// Empty internal receive buffers .
void WiFiUdp::empty()
{
	if (_context != nullptr)
	{
		_context->empty();
	}
}

// Return the IP address of the host who sent the current incoming packet .
IPAddress WiFiUdp::remoteIP()
{
	IPAddress ip;

	if (_context != nullptr)
	{
		ip = _context->remoteIP();
	}

	return ip;
}

// Return the port of the host who sent the current incoming packet .
uint16_t WiFiUdp::remotePort()
{
	uint16_t port = 0;

	if (_context != nullptr)
	{
		port = _context->remotePort();
	}

	return port;
}

// Return the destination IP address .
IPAddress WiFiUdp::destinationIP()
{
	IPAddress ip;

	if (_context != nullptr)
	{
		ip = _context->destinationIP();
	}

	return ip;
}

// Return the local port .
uint16_t WiFiUdp::localPort()
{
	uint16_t port = 0;

	if (_context != nullptr)
	{
		port = _context->localPort();
	}

	return port;
}

/*******************************************************************************
    Public methods implementation of the subclass 'Context' .
*******************************************************************************/

// Constructor .
WiFiUdp::Context::Context() :
	_sock(SOCK_NOT_AVAIL),
	_availableData(0),
	_bufferIndex(0),
	_bufferCount(0),
	_referenceCount(0)
{

}

// Destructor .
WiFiUdp::Context::~Context()
{
	stop();
}

// Start WiFiUDP socket, listening at local port .
uint8_t WiFiUdp::Context::begin(uint16_t port)
{
	if (_sock == SOCK_NOT_AVAIL)
	{
		_sock = WiFi.acquireUdpSocket();
	}

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (WiFi.startUdp(_sock, port) == true)
		{
			return 1;
		}

		WiFi.releaseUdpSocket(_sock);
	}

	return 0;
}

// Finish with the UDP socket .
void WiFiUdp::Context::stop()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Close the connection .
		WiFi.stopUdp(_sock);

		// Release the socket .
		WiFi.releaseUdpSocket(_sock);

		_availableData = 0;
		_bufferIndex = 0;
		_bufferCount = 0;

		_sock = SOCK_NOT_AVAIL;
	}
}

// Start building up a packet to send to the remote host specific in IP and port .
int WiFiUdp::Context::beginPacket(IPAddress ip, uint16_t port)
{
	if (_sock == SOCK_NOT_AVAIL)
	{
		_sock = WiFi.acquireUdpSocket();
	}

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (WiFi.beginUdpPacket(_sock, ip, port) == true)
		{
			_availableData = 0;
			_bufferIndex = 0;
			_bufferCount = 0;

			return 1;
		}

		WiFi.releaseUdpSocket(_sock);
	}

	return 0;
}

// Finish off this packet and send it .
int WiFiUdp::Context::endPacket()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		if (WiFi.endUdpPacket(_sock) == true)
		{
			return 1;
		}
	}

	return 0;
}

// Write size bytes from buffer into the packet .
size_t WiFiUdp::Context::write(const uint8_t *buffer, size_t size)
{
	uint16_t count = 0;

	if (_sock != SOCK_NOT_AVAIL && buffer != nullptr && size > 0)
	{
		uint16_t dataWritten = 0;

		while(count < size)
		{
			dataWritten = WiFi.writeUdpBuffer(_sock, (uint8_t *)buffer, (size - count));

			// Update buffer pointer and counter .
			count += dataWritten;
			buffer += dataWritten;

			// Check when data are no longer accepted .
			if (dataWritten == 0)
			{
				break;
			}
		}
	}

	return count;
}

// Start processing the next available incoming packet .
int WiFiUdp::Context::parsePacket()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		_availableData = WiFi.parseUdpPacket(_sock);

		return _availableData;
	}

	return 0;
}

// Number of bytes remaining in the current packet .
int WiFiUdp::Context::available()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Try to update available data if it's equal to zero .
		if (_availableData == 0)
		{
			_availableData = WiFi.getUdpAvailableData(_sock);
		}

		if (_availableData > 0 && _bufferCount == 0)
		{
			uint16_t dataToRead = _availableData;

			if (dataToRead > BUFFER_SIZE)
			{
				dataToRead = BUFFER_SIZE;
			}

			_bufferIndex = 0;

			// Try to fill internal buffer .
			_bufferCount = WiFi.readClientBuffer(_sock, _buffer, dataToRead);

		}

		return _availableData;
	}

	return 0;
}

// Read a single byte from the current packet .
int WiFiUdp::Context::read()
{
	int data = EOF;

	if (_sock != SOCK_NOT_AVAIL)
	{
		// Try to update available data if it's equal to zero .
		if (available() > 0)
		{
			if (_bufferCount > 0)
			{
				data = (int)_buffer[_bufferIndex++];

				_bufferCount--;
				_availableData--;
			}
		}
	}

	return data;
}

// Read up to 'length' bytes from the current packet and place them into buffer .
int WiFiUdp::Context::read(unsigned char* buffer, size_t length)
{
	uint16_t count = 0;

	if (_sock != SOCK_NOT_AVAIL && buffer != nullptr && length > 0)
	{
		// Try to update available data if it's equal to zero .
		if (_availableData == 0)
		{
			_availableData = WiFi.getUdpAvailableData(_sock);
		}

		if (_availableData > 0)
		{
			uint16_t dataToRead = 0;

			if (length > _availableData)
			{
				length = _availableData;
			}

			// Read data from internal buffer first .
			if (_bufferCount > 0)
			{
				dataToRead = length;

				if (dataToRead > _bufferCount)
				{
					dataToRead = _bufferCount;
				}

				for(uint16_t i = 0; i < dataToRead; i++)
				{
					buffer[i] = _buffer[_bufferIndex + i];
				}

				// Update internal index and counter .
				_bufferCount -= dataToRead;
				_bufferIndex += dataToRead;

				// Update buffer pointer and counter .
				count += dataToRead;
				buffer += dataToRead;
			}

			// Read data from ESP buffers .
			while(count < length)
			{
				dataToRead = WiFi.readUdpBuffer(_sock, buffer, length - count);

				// Update buffer pointer and counter .
				count += dataToRead;
				buffer += dataToRead;

				// Check when there are no more data available .
				if (dataToRead == 0)
				{
					break;
				}
			}

			_availableData -= count;
		}
	}

	return (int)count;
}

// Return the next byte from the current packet without moving on to the next byte .
int WiFiUdp::Context::peek()
{
	int data = EOF;

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (available() > 0)
		{
			if (_bufferCount > 0)
			{
				data = (int)_buffer[_bufferIndex];
			}
		}
	}

	return data;
}

// Finish sending the current packet .
void WiFiUdp::Context::flush()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Flush ESP buffers .
		WiFi.emptyFlushUdpBuffer(_sock, false, true);
	}
}

// Empty internal receive buffers .
void WiFiUdp::Context::empty()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Empty internal buffer .
		_bufferIndex = 0;
		_bufferCount = 0;
		_availableData = 0;

		// Empty ESP buffers .
		WiFi.emptyFlushUdpBuffer(_sock, true, false);
	}
}

// Return the IP address of the host who sent the current incoming packet .
IPAddress WiFiUdp::Context::remoteIP()
{
	IPAddress ip;
	uint16_t port = 0;

	if (_sock != SOCK_NOT_AVAIL)
	{
		WiFi.getUdpRemoteInfo(_sock, ip, port);
	}

	return ip;
}

// Return the port of the host who sent the current incoming packet .
uint16_t WiFiUdp::Context::remotePort()
{
	IPAddress ip;
	uint16_t port = 0;

	if (_sock != SOCK_NOT_AVAIL)
	{
		WiFi.getUdpRemoteInfo(_sock, ip, port);
	}

	return port;
}

// Return the destination IP address .
IPAddress WiFiUdp::Context::destinationIP()
{
	IPAddress ip;
	uint16_t port = 0;

	if (_sock != SOCK_NOT_AVAIL)
	{
		WiFi.getUdpLocalInfo(_sock, ip, port);
	}

	return ip;
}

// Return the local port .
uint16_t WiFiUdp::Context::localPort()
{
	IPAddress ip;
	uint16_t port = 0;

	if (_sock != SOCK_NOT_AVAIL)
	{
		WiFi.getUdpLocalInfo(_sock, ip, port);
	}

	return port;
}

// Increment the reference counter .
void WiFiUdp::Context::ref()
{
	_referenceCount++;
}

// Decrement the reference counter and eventually delete the object .
void WiFiUdp::Context::unref()
{
	_referenceCount--;

	if (_referenceCount <= 0)
	{
		delete this;
	}
}

/******************************************************************************/


