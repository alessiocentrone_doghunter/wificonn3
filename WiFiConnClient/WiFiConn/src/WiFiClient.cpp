/*
 * WiFiClient.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConn.h"

#include "WiFiClient.h"

/*******************************************************************************
    Public methods implementation of the class 'WiFiClient' .
*******************************************************************************/

// Constructor .
WiFiClient::WiFiClient() :
	_context(nullptr)
{

}

// Constructor .
WiFiClient::WiFiClient(const WiFiClient & other)
{
	_context = other._context;

	if (_context != nullptr)
	{
	    _context->ref();
	}
}

// Destructor .
WiFiClient::~WiFiClient()
{
	if (_context != nullptr)
	{
		_context->unref();
	}
}

// Assignment operator .
WiFiClient& WiFiClient::operator=(WiFiClient& other)
{
	if (_context != nullptr)
	{
		_context->unref();
	}

	_context = other._context;

	if (_context != nullptr)
	{
		_context->ref();
	}

    return *this;
}

// Get connection status .
uint8_t WiFiClient::status()
{
	if (_context != nullptr)
	{
		return _context->status();
	}

	return (uint8_t) wl_tcp_states::CLOSED;
}

// Connect to a specific IP address and port .
int WiFiClient::connect(IPAddress ip, uint16_t port)
{
	if (_context != nullptr)
	{
		_context->unref();
	}

	_context = new Context();

	_context->ref();

	if (_context->connect(ip, port) == 0)
	{
		_context->unref();

		_context = nullptr;

		return 0;
	}

	return 1;
}

// Connect to a specific host and port .
int WiFiClient::connect(const char *host, uint16_t port)
{
	IPAddress hostIP;

	if (WiFi.hostByName(host, hostIP) == true)
	{
		return connect(hostIP, port);
	}

	return 0;
}

// Write a byte .
size_t WiFiClient::write(uint8_t b)
{
	 return write(&b, 1);
}

// Write a buffer of bytes .
size_t WiFiClient::write(const uint8_t *buffer, size_t length)
{
	if (_context != nullptr)
	{
		return _context->write(buffer, length);
	}

	return 0;
}

// Return the number of bytes available to read .
int WiFiClient::available()
{
	if (_context != nullptr)
	{
		return _context->available();
	}

	return 0;
}

// Read a byte .
int WiFiClient::read()
{
	if (_context != nullptr)
	{
		return _context->read();
	}

	return EOF;
}

// Read a buffer of bytes .
int WiFiClient::read(uint8_t *buffer, size_t length)
{
	if (_context != nullptr)
	{
		return _context->read(buffer, length);
	}

	return 0;
}

// Peek a byte without extracting it from the receive buffer .
int WiFiClient::peek()
{
	if (_context != nullptr)
	{
		return _context->peek();
	}

	return EOF;
}

// Flush internal transmission buffers .
void WiFiClient::flush()
{
	if (_context != nullptr)
	{
		_context->flush();
	}
}

// Empty internal receive buffers .
void WiFiClient::empty()
{
	if (_context != nullptr)
	{
		_context->empty();
	}
}

// Close the connection .
void WiFiClient::stop()
{
	if (_context != nullptr)
	{
		_context->stop();
	}
}

// Indicate if the connection is active .
uint8_t WiFiClient::connected()
{
	if (_context != nullptr)
	{
		return _context->connected();
	}

	return 0;
}

// Indicate if the connection is active .
WiFiClient::operator bool()
{
	return (bool)connected();
}

/*******************************************************************************
    Protected methods implementation of the subclass 'WiFiClient' .
*******************************************************************************/

// Protected constructor .
WiFiClient::WiFiClient(uint8_t sock)
{
	_context = new Context(sock);
	_context->ref();
}

/*******************************************************************************
    Public methods implementation of the subclass 'Context' .
*******************************************************************************/

// Constructor .
WiFiClient::Context::Context() :
	_sock(SOCK_NOT_AVAIL),
	_availableData(0),
	_bufferIndex(0),
	_bufferCount(0),
	_referenceCount(0)
{

}

// Constructor .
WiFiClient::Context::Context(uint8_t sock) :
	_sock(sock),
	_availableData(0),
	_bufferIndex(0),
	_bufferCount(0),
	_referenceCount(0)
{
	if (_sock > MAX_SOCK_NUM)
	{
		_sock = SOCK_NOT_AVAIL;
	}
}

// Destructor .
WiFiClient::Context::~Context()
{
	stop();
}

// Get connection status .
uint8_t WiFiClient::Context::status()
{
	uint8_t state = (uint8_t) wl_tcp_states::CLOSED;

	if (_sock != SOCK_NOT_AVAIL)
	{
		// Try to update available data if it's equal to zero .
		if (_availableData == 0)
		{
			uint16_t availForRead = 0, availForWrite = 0;

			// Update status and available data .
			if (WiFi.getClientStateInfo(_sock, state, availForRead, availForWrite) == true)
			{
				_availableData = availForRead;
			}
		}

		if (_availableData > 0)
		{
			state = (uint8_t) wl_tcp_states::ESTABLISHED;
		}
	}

	return state;
}

// Connect to a specific IP address and port .
int WiFiClient::Context::connect(IPAddress ip, uint16_t port)
{
	if (_sock == SOCK_NOT_AVAIL)
	{
		_sock = WiFi.acquireClientSocket();
	}

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (WiFi.startClient(_sock, ip, port) == true)
		{
			_availableData = 0;
			_bufferIndex = 0;
			_bufferCount = 0;

			return 1;
		}

		WiFi.releaseClientSocket(_sock);
	}

	return 0;
}

// Write a buffer .
size_t WiFiClient::Context::write(const uint8_t *buffer, size_t length)
{
	uint16_t count = 0;

	if (_sock != SOCK_NOT_AVAIL && buffer != nullptr && length > 0)
	{
		uint16_t dataWritten = 0;

		while(count < length)
		{
			dataWritten = WiFi.writeClientBuffer(_sock, (uint8_t *)buffer, (length - count));

			// Update buffer pointer and counter .
			count += dataWritten;
			buffer += dataWritten;

			// Check when data are no longer accepted .
			if (dataWritten == 0)
			{
				break;
			}
		}
	}

	return count;
}

// Return the number of bytes available to read .
int WiFiClient::Context::available()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Try to update available data if it's equal to zero .
		if (_availableData == 0)
		{
			_availableData = WiFi.getClientAvailableData(_sock);
		}

		if (_availableData > 0 && _bufferCount == 0)
		{
			uint16_t dataToRead = _availableData;

			if (dataToRead > BUFFER_SIZE)
			{
				dataToRead = BUFFER_SIZE;
			}

			_bufferIndex = 0;

			// Try to fill internal buffer .
			_bufferCount = WiFi.readClientBuffer(_sock, _buffer, dataToRead);
		}

		return _availableData;
	}

	return 0;
}

// Read a byte .
int WiFiClient::Context::read()
{
	int data = EOF;

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (available() > 0)
		{
			if (_bufferCount > 0)
			{
				data = (int)_buffer[_bufferIndex++];

				_bufferCount--;
				_availableData--;
			}
		}
	}

	return data;
}

// Read a buffer of bytes  .
int WiFiClient::Context::read(uint8_t *buffer, size_t length)
{
	uint16_t count = 0;

	if (_sock != SOCK_NOT_AVAIL && buffer != nullptr && length > 0)
	{
		// Try to update available data if it's equal to zero .
		if (_availableData == 0)
		{
			_availableData = WiFi.getUdpAvailableData(_sock);
		}

		if (_availableData > 0)
		{
			uint16_t dataToRead = 0;

			// Read data from internal buffer first .
			if (_bufferCount > 0)
			{
				dataToRead = length;

				if (length > _availableData)
				{
					length = _availableData;
				}

				if (dataToRead > _bufferCount)
				{
					dataToRead = _bufferCount;
				}

				for(uint16_t i = 0; i < dataToRead; i++)
				{
					buffer[i] = _buffer[_bufferIndex + i];
				}

				// Update internal index and counter .
				_bufferCount -= dataToRead;
				_bufferIndex += dataToRead;

				// Update buffer pointer and counter .
				count += dataToRead;
				buffer += dataToRead;
			}

			// Read data from ESP buffers .
			while(count < length)
			{
				dataToRead = WiFi.readClientBuffer(_sock, buffer, length - count);

				// Update buffer pointer and counter .
				count += dataToRead;
				buffer += dataToRead;

				// Check when there are no more data available .
				if (dataToRead == 0)
				{
					break;
				}
			}

			_availableData -= count;
		}
	}

	return (int)count;
}

// Peek a byte without extracting it from the receive buffer .
int WiFiClient::Context::peek()
{
	int data = EOF;

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (available() > 0)
		{
			if (_bufferCount > 0)
			{
				data = (int)_buffer[_bufferIndex];
			}
		}
	}

	return data;
}

// Flush internal transmission buffers .
void WiFiClient::Context::flush()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Flush ESP buffers .
		WiFi.emptyFlushClientBuffer(_sock, false, true);
	}
}

// Empty internal receive buffers .
void WiFiClient::Context::empty()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Empty internal buffer .
		_bufferIndex = 0;
		_bufferCount = 0;
		_availableData = 0;

		// Empty ESP buffers .
		WiFi.emptyFlushClientBuffer(_sock, true, false);
	}
}

// Close the connection .
void WiFiClient::Context::stop()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Close the connection .
		WiFi.stopClient(_sock);

		// Release the socket .
		WiFi.releaseClientSocket(_sock);

		_availableData = 0;
		_bufferIndex = 0;
		_bufferCount = 0;

		_sock = SOCK_NOT_AVAIL;
	}
}

// Indicate if the connection is active .
uint8_t WiFiClient::Context::connected()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		uint8_t state = status();

		if (state == (uint8_t) wl_tcp_states::ESTABLISHED || _availableData > 0)
		{
			return 1;
		}
	}

	return 0;
}

// Increment the reference counter .
void WiFiClient::Context::ref()
{
	_referenceCount++;
}

// Decrement the reference counter and eventually delete the object .
void WiFiClient::Context::unref()
{
	_referenceCount--;

	if (_referenceCount <= 0)
	{
		delete this;
	}
}

/******************************************************************************/

