/*
 * SpiProtocolMaster.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "EspiMaster.h"

#include "SPI.h"


// --- Defines .
#define SYNC_TIMEOUT				5000	// 5000 ms .
#define DATA_BLOCK_TIMEOUT			10		// 10 ms .

#define SS_TX_US_DELAY				100		// 100 us .
#define SS_RX_US_DELAY				3000	// 3000 us .
#define ANTI_COLLISION_US_DELAY		50		// 50 us .

// --- Static member declaration .
bool EspiMaster::_isDeployed = false;

// --- Class implementation .

// Constructor .
EspiMaster::EspiMaster(uint8_t slaveSelectPin, uint8_t slaveReadyPin) :
	_eventPacket(nullptr),
	_eventObject(nullptr),
	_eventMethod(nullptr),
	_eventFunction(nullptr),
	_errorObject(nullptr),
	_errorMethod(nullptr),
	_errorFunction(nullptr),
	_errorFlags(0),
	_ss_pin(slaveSelectPin),
	_sr_pin(slaveReadyPin),
	_sr_lockedHighCount(0)
{

}

// Destructor .
EspiMaster::~EspiMaster()
{
	end();
}

// Initialize the protocol and the hardware resources .
bool EspiMaster::begin()
{
	if (_isDeployed == false)
	{
		// Reset internal data .
		_eventPacket = nullptr;
		_eventObject = nullptr;
		_eventMethod = nullptr;
		_eventFunction = nullptr;
		_errorObject = nullptr;
		_errorMethod = nullptr;
		_errorFunction = nullptr;
		_errorFlags = 0;
		_sr_lockedHighCount = 0;

		// Initialize master SPI interface .
		SPI.begin();

		// Configure SLAVE_READY as input .
		pinMode(_sr_pin, INPUT);

		// Configure SLAVE_SELECT as output .
		pinMode(_ss_pin, OUTPUT);
		digitalWrite(_ss_pin, HIGH);

		_isDeployed = true;
	}

	return true;
}

// Release the hardware resources .
void EspiMaster::end()
{
	if (_isDeployed == true)
	{
		SPI.end();

		/*
		pinMode(SS, INPUT);
		pinMode(SCK, INPUT);
		pinMode(MOSI, INPUT);
		pinMode(MISO, INPUT);
		*/

		// Release all hardware resources .
		pinMode(_sr_pin, INPUT);
		pinMode(_ss_pin, INPUT);

		// Reset internal data .
		_eventPacket = nullptr;
		_eventObject = nullptr;
		_eventMethod = nullptr;
		_eventFunction = nullptr;
		_errorObject = nullptr;
		_errorMethod = nullptr;
		_errorFunction = nullptr;
		_errorFlags = 0;
		_sr_lockedHighCount = 0;

		_isDeployed = false;
	}
}

// Set the event callback handler .
void EspiMaster::setEventCallback(EspiEventFunction eventFunction, EspiPacket *eventPacket)
{
	_eventObject = nullptr;
	_eventMethod = nullptr;
	_eventFunction = eventFunction;

	_eventPacket = eventPacket;
}

// Set the event callback handler .
void EspiMaster::setEventCallback(void *eventObject, EspiEventMethod eventMethod, EspiPacket *eventPacket)
{
	_eventObject = reinterpret_cast<EspiMaster*>(eventObject);
	_eventMethod = eventMethod;
	_eventFunction = nullptr;

	_eventPacket = eventPacket;
}

// Set the error callback handler .
void EspiMaster::setErrorCallback(EspiErrorFunction errorFunction)
{
	_errorObject = nullptr;
	_errorMethod = nullptr;
	_errorFunction = errorFunction;
}

// Set the error callback handler .
void EspiMaster::setErrorCallback(void *errorObject, EspiErrorMethod errorMethod)
{
	_errorObject = reinterpret_cast<EspiMaster*>(errorObject);
	_errorMethod = errorMethod;
	_errorFunction = nullptr;
}

// Handle the incoming events .
void EspiMaster::handle()
{
	if (_isDeployed == true)
	{
		// Verify that the 'slave ready' signal is active but not locked high .
		if (_checkSlaveReady(HIGH) == true && _sr_lockedHighCount < MAX_SR_HIGH_TIMEOUT)
		{
			if (_eventPacket != nullptr)
			{
				// Try to receive packet .
				if (receivePacket(_eventPacket) == true)
				{
					// Call event callback handler .
					if (_eventObject != nullptr && _eventMethod != nullptr)
					{
						(_eventObject->*_eventMethod)(_eventPacket);
					}
					else if (_eventFunction != nullptr)
					{
						_eventFunction(_eventPacket);
					}
				}
			}
			else
			{
				// Discard the incoming data .
				_receiveData(nullptr, 0);
			}
		}
	}
}

// Send a packet eventually splitting it in multiple data blocks .
bool EspiMaster::sendPacket(EspiPacket *packet)
{
	bool success = false;

	if (_isDeployed == true && packet != nullptr)
	{
		// Check packet validity .
		if (packet->isValid() == true)
		{
			// Try to take the control of the bus .
			if (_startTransmission() == true)
			{
				uint16_t dataSent = 0;
				uint16_t dataToSend = 0;

				uint8_t *packetData = packet->buffer();
				uint16_t packetLength = packet->length();

				success = true;

				// Send the packet splitting it in blocks of 32 bytes .
				while(success == true && dataSent < packetLength)
				{
					dataToSend = packetLength - dataSent;

					if (dataToSend > ESP8266_SPI_MAX_PACKET_LEN)
					{
						dataToSend = ESP8266_SPI_MAX_PACKET_LEN;
					}

					if (_sendData(&packetData[dataSent], dataToSend) == true)
					{
						dataSent += dataToSend;
					}
					else
					{
						// Exit on a transmission error .
						success = false;
					}
				}
			}
		}
	}

	return success;
}

// Receive a packet eventually joining together multiple data blocks .
bool EspiMaster::receivePacket(EspiPacket *packet)
{
	bool success = false;

	// Check input parameters .
	if (_isDeployed == true && packet != nullptr)
	{
		// Check packet validity .
		if (packet->isValid() == true)
		{
			uint16_t dataReceived = 0;
			uint16_t dataToReceive = 32;

			uint8_t *packetData = packet->buffer();
			uint16_t packetLength = 0;

			// Receive the first data block .
			success = _receiveData(packetData, dataToReceive);

			if (success == true)
			{
				dataReceived += dataToReceive;

				// Try to decode header .
				if (packet->beginDecode() == true)
				{
					// Retrieve packet length .
					packetLength = packet->length();

					// Receive the packet joining together multiple data blocks .
					while(success == true && dataReceived < packetLength)
					{
						dataToReceive = packetLength - dataReceived;

						if (dataToReceive > ESP8266_SPI_MAX_PACKET_LEN)
						{
							dataToReceive = ESP8266_SPI_MAX_PACKET_LEN;
						}

						if (_receiveData(&packetData[dataReceived], dataToReceive) == true)
						{
							dataReceived += dataToReceive;
						}
						else
						{
							// Exit on receiving error .
							success = false;
						}
					}

					if (success == true)
					{
						if (packet->endDecode() == false)
						{
							// Report an invalid packet tail .
							_reportError(ERR_INVALID_PACKET_TAIL);

							success = false;
						}
					}
				}
				else
				{
					// Report an invalid packet header .
					_reportError(ERR_INVALID_PACKET_HEADER);

					success = false;
				}
			}
		}

		// Clear the packet if reception fails .
		if (success == false)
		{
			packet->clear();
		}
	}

	return success;
}

// Send a request and wait for the answer .
bool EspiMaster::transfer(EspiPacket *request, EspiPacket *answer, uint32_t timeout)
{
	bool success = false;

	if (_isDeployed == true)
	{
		// Check input parameters .
		if (request != nullptr && answer != nullptr && timeout > 0)
		{
			// Send the request packet .
			if (sendPacket(request) == true)
			{
				// Wait for the answer .
				if (_waitSlaveReady(HIGH, timeout) == true)
				{
					// Receive the answer packet .
					if (receivePacket(answer) == true)
					{
						success = true;
					}
				}
				else
				{
					// Report an answer timeout error .
					_reportError(ERR_ANSWER_TIMEOUT);
				}
			}
		}
	}

	return success;
}

// Read (and eventually reset) error flags .
uint32_t EspiMaster::readErrors(bool reset)
{
	uint32_t errorFlags = _errorFlags;

	if (reset == true)
	{
		_errorFlags = 0;
	}

	return errorFlags;
}


// --- Private methods implementation .

// Try to take control of the communication bus .
bool EspiMaster::_startTransmission(uint8_t attempts)
{
	while (attempts > 0)
	{
		bool collision = false;

		uint32_t startMicros = 0;

		// Check for incoming packet .
		while (_checkSlaveReady(HIGH) == true && _sr_lockedHighCount < MAX_SR_HIGH_TIMEOUT)
		{
			// Handle incoming events .
			handle();
		}

		// Prevent execution when the 'Slave Ready' is locked high .
		if (_sr_lockedHighCount >= MAX_SR_HIGH_TIMEOUT)
		{
			return false;
		}

		// Disable global interrupts .
		noInterrupts();

		startMicros = micros();

		// Take control of the bus enabling slave select .
		_setSlaveSelect(LOW);

		// Poll slave ready signal
		while((micros() - startMicros) < (ANTI_COLLISION_US_DELAY / 2) && collision == false)
		{
			// Slave ready signal must remains low during the anti-collision delay .
			collision = _checkSlaveReady(HIGH);
		}

		// Enable global interrupts .
		interrupts();

		if (collision == false)
		{
			return true;
		}

		// Wait for 'slave ready' signal goes low .
		if (_waitSlaveReady(LOW, DATA_BLOCK_TIMEOUT) == false)
		{
			// Release the control of the bus disabling slave select .
			_setSlaveSelect(HIGH, SS_TX_US_DELAY);

			// Increase the 'SR_HIGH_TIMEOUT' counter .
			_sr_lockedHighCount++;

			// Report a synchronization error: 'slave ready' signal remains high .
			_reportError(ERR_SYNC_SR_HIGH_TIMEOUT);

			return false;
		}

		// Release the control of the bus disabling slave select .
		_setSlaveSelect(HIGH, SS_TX_US_DELAY);
	}

	// Report a collision error .
	_reportError(ERR_BUS_COLLISION);

	return false;
}

// Send a single data block .
bool EspiMaster::_sendData(uint8_t *data, uint16_t length)
{
	// Verify that the 'slave ready' signal is not active before sending packet .
	if (_checkSlaveReady(LOW) == false)
	{
		// Report 'slave ready' signal is active before transmission .
		_reportError(ERR_SYNC_SR_ALREADY_ACTIVE);

		return false;
	}

	// Enable slave select signal .
	_setSlaveSelect(LOW);

	// Wait for 'slave ready' signal goes high .
	if (_waitSlaveReady(HIGH, SYNC_TIMEOUT) == false)
	{
		// Disable slave select signal .
		_setSlaveSelect(HIGH, SS_TX_US_DELAY);

		// Report a synchronization error: 'slave ready' signal remains low .
		_reportError(ERR_SYNC_SR_LOW_TIMEOUT);

		return false;
	}

	// Write data block .
	_writeData(data, length);

	// Disable slave select signal .
	_setSlaveSelect(HIGH);

	// Wait for 'slave ready' signal goes low .
	if (_waitSlaveReady(LOW, DATA_BLOCK_TIMEOUT) == false)
	{
		// Increase the 'SR_HIGH_TIMEOUT' counter .
		_sr_lockedHighCount++;

		// Report a synchronization error: 'slave ready' signal remains high .
		_reportError(ERR_SYNC_SR_HIGH_TIMEOUT);

		return false;
	}

	return true;
}

// Receive a single data block .
bool EspiMaster::_receiveData(uint8_t *data, uint16_t length)
{
	// Wait for the 'slave ready' signal goes high .
	if (_waitSlaveReady(HIGH, SYNC_TIMEOUT) == false)
	{
		// Report a synchronization error: 'slave ready' signal remains low .
		_reportError(ERR_SYNC_SR_LOW_TIMEOUT);

		return false;
	}

	// Insert a small anti-collision delay before enabling slave select .
	delayMicroseconds(ANTI_COLLISION_US_DELAY);

	// Enable slave select signal .
	_setSlaveSelect(LOW);

	// Wait for 'slave ready' signal goes low .
	if (_waitSlaveReady(LOW, DATA_BLOCK_TIMEOUT) == false)
	{
		// Disable slave select signal .
		_setSlaveSelect(HIGH, SS_RX_US_DELAY);

		// Increase the 'SR_HIGH_TIMEOUT' counter .
		_sr_lockedHighCount++;

		// Report a synchronization error: 'slave ready' signal remains high .
		_reportError(ERR_SYNC_SR_HIGH_TIMEOUT);

		return false;
	}

	// Read data block .
	_readData(data, length);

	// Disable slave select signal .
	_setSlaveSelect(HIGH, SS_RX_US_DELAY);

	return true;
}

// Write a single block of data to SPI interface .
void EspiMaster::_writeData(uint8_t *data, uint16_t length)
{
	// White the packet header .
	SPI.transfer((uint8_t)ESP8266_DATA_WRITE);
	SPI.transfer(0x00);							// Dummy data .

	for (uint16_t i = 0; i < ESP8266_SPI_MAX_PACKET_LEN; i++)
	{
		if (i < length && data != nullptr)
		{
			SPI.transfer(data[i]);
		}
		else
		{
			SPI.transfer(0x00);
		}
	}
}

// Read a single block of data from SPI interface .
void EspiMaster::_readData(uint8_t *data, uint16_t length)
{
	// White the packet header .
	SPI.transfer((uint8_t)ESP8266_DATA_READ);
	SPI.transfer(0x00);							// Dummy data .

	for (uint16_t i = 0; i < ESP8266_SPI_MAX_PACKET_LEN; i++)
	{
		if (i < length && data != nullptr)
		{
			data[i] = SPI.transfer(0x00);
		}
		else
		{
			SPI.transfer(0x00);
		}
	}
}

// Report error .
void EspiMaster::_reportError(uint32_t errorCode)
{
	// Set the related error flag .
	_errorFlags |= errorCode;

	// Call error callback handler if any .
	if (_errorObject != nullptr && _errorMethod != nullptr)
	{
		(_errorObject->*_errorMethod)(errorCode);
	}
	else if (_errorFunction != nullptr)
	{
		_errorFunction(errorCode);
	}
}

// Driver slave select signal .
void EspiMaster::_setSlaveSelect(uint8_t state, uint32_t us_delay)
{
	digitalWrite(_ss_pin, state);

	delayMicroseconds(us_delay);
}

// Check the 'slave ready' signal state .
bool EspiMaster::_checkSlaveReady(uint8_t state)
{
	uint8_t pinState = digitalRead(_sr_pin);

	if (pinState == LOW)
	{
		_sr_lockedHighCount = 0;
	}

	return (pinState == state);
}

// Wait for the 'slave ready' signal goes in a specific state .
bool EspiMaster::_waitSlaveReady(uint8_t state, uint32_t timeout)
{
	uint8_t pinState;
	uint32_t startMillis = millis();

	while(1)
	{
		if ((pinState = digitalRead(_sr_pin)) == LOW)
		{
			_sr_lockedHighCount = 0;
		}

		if (pinState == state || (millis() - startMillis) > timeout)
		{
			break;
		}

		yield();
	}

	return (pinState == state);
}

// --- End of file .


