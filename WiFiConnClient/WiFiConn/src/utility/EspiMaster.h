/*
 * EspiMaster.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ESPI_MASTER_H_
#define ESPI_MASTER_H_

// --- Includes .
#include <inttypes.h>

#include "EspiPacket.h"

// --- Class definition .

// Class that provides a communication protocol via SPI (Master side) .
class EspiMaster
{
public:

	// Event callback function definition .
	typedef void(*EspiEventFunction)(EspiPacket *packet);

	// Event callback method definition .
	typedef void(EspiMaster::*EspiEventMethod)(EspiPacket *packet);

	// Error callback function definition .
	typedef void(*EspiErrorFunction)(uint32_t errorCode);

	// Error callback method definition .
	typedef void(EspiMaster::*EspiErrorMethod)(uint32_t errorCode);

	// Enumeration of all error flags .
	enum ErrorFlags: uint32_t
	{
		ERR_INVALID_PACKET_HEADER	= 0x00000001,
		ERR_INVALID_PACKET_TAIL		= 0x00000002,
		ERR_BUS_COLLISION			= 0x00000004,
		ERR_SYNC_SR_ALREADY_ACTIVE  = 0x00000008,
		ERR_SYNC_SR_HIGH_TIMEOUT	= 0x00000010,
		ERR_SYNC_SR_LOW_TIMEOUT		= 0x00000020,
		ERR_ANSWER_TIMEOUT			= 0x00000040,
	};

	// Constructor .
	EspiMaster(uint8_t slaveSelectPin, uint8_t slaveReadyPin);

	// Destructor .
	virtual ~EspiMaster();


	// Initialize the protocol and the hardware resources .
	bool begin();

	// Release the hardware resources .
	void end();


	// Set the event callback handler .
	void setEventCallback(EspiEventFunction eventFunction, EspiPacket *eventPacket);

	// Set the event callback handler .
	void setEventCallback(void *eventObject, EspiEventMethod eventMethod, EspiPacket *eventPacket);

	// Set the error callback handler .
	void setErrorCallback(EspiErrorFunction errorFunction);

	// Set the error callback handler .
	void setErrorCallback(void *errorObject, EspiErrorMethod errorMethod);


	// Handle the incoming event .
	void handle();

	// Send a packet eventually splitting it in multiple data blocks .
	bool sendPacket(EspiPacket *packet);

	// Receive a packet eventually joining together multiple data blocks .
	bool receivePacket(EspiPacket *packet);

	// Send a request and wait for the answer .
	bool transfer(EspiPacket *request, EspiPacket *answer, uint32_t timeout);

	// Read (and eventually reset) error flags .
	uint32_t readErrors(bool reset = false);

private:

	enum { ESP8266_SPI_MAX_PACKET_LEN = 32 };

	enum { MAX_SR_HIGH_TIMEOUT = 3 };

	// SPI commands used by the ESP8266 .
	enum EspCommands: uint8_t
	{
		ESP8266_STATUS_READ		= 0x04,
		ESP8266_STATUS_WRITE	= 0X01,
		ESP8266_DATA_READ		= 0x03,
		ESP8266_DATA_WRITE		= 0x02
	};

	// Indicate the state of use of the hardware resources .
	static bool _isDeployed;

	// Packet to handle and store the event data .
	EspiPacket *_eventPacket;

	// Event callback .
	EspiMaster *_eventObject;
	EspiEventMethod _eventMethod;
	EspiEventFunction _eventFunction;

	// Error callback .
	EspiMaster *_errorObject;
	EspiErrorMethod _errorMethod;
	EspiErrorFunction _errorFunction;


	// Error flags .
	uint32_t _errorFlags;

	// Slave select signal .
	uint8_t _ss_pin;

	// Slave ready signal .
	uint8_t _sr_pin;

	// Count how many times the signal remains locked high .
	uint8_t _sr_lockedHighCount;

	// Try to take control of the communication bus .
	bool _startTransmission(uint8_t attempts = 3);

	// Send a single data block .
	bool _sendData(uint8_t *data, uint16_t length);

	// Receive a single data block .
	bool _receiveData(uint8_t *data, uint16_t length);

	// Write a single block of data to SPI interface .
	void _writeData(uint8_t *data, uint16_t length);

	// Read a single block of data from SPI interface .
	void _readData(uint8_t *data, uint16_t length);

	// Report error .
	void _reportError(uint32_t errorCode);

	// Driver slave select signal .
	void inline _setSlaveSelect(uint8_t state, uint32_t us_delay = 0);

	// Check the 'slave ready' signal state
	bool inline _checkSlaveReady(uint8_t state);

	// Wait for the 'slave ready' signal goes in a specific state .
	bool _waitSlaveReady(uint8_t state, uint32_t timeout);

};

#endif /* ESPI_MASTER_H_ */
