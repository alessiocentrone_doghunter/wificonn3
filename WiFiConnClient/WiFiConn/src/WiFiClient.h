/*
 * WiFiClient.h
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CLIENT_H_
#define WIFI_CLIENT_H_

#include "Arduino.h"	
#include "Print.h"
#include "Client.h"
#include "IPAddress.h"

class WiFiClient : public Client
{

public:

	// Constructor .
	WiFiClient();

	// Constructor .
	WiFiClient(const WiFiClient & other);

	// Destructor .
	virtual ~WiFiClient();

	// Assignment operator .
	WiFiClient & operator=(WiFiClient & other);

	// Get connection status .
	uint8_t status();

	// Connect to a specific IP address and port .
	virtual int connect(IPAddress ip, uint16_t port);

	// Connect to a specific host and port .
	virtual int connect(const char *host, uint16_t port);

	// Write a byte .
	virtual size_t write(uint8_t b);

	// Write a buffer of bytes .
	virtual size_t write(const uint8_t *buffer, size_t length);

	// Return the number of bytes available to read .
	virtual int available();

	// Read a byte .
	virtual int read();

	// Read a buffer of bytes .
	virtual int read(uint8_t *buffer, size_t length);

	// Peek a byte without extracting it from the receive buffer .
	virtual int peek();

	// Flush internal transmission buffers .
	virtual void flush();

	// Empty internal receive buffers .
	virtual void empty();

	// Close the connection .
	virtual void stop();

	// Indicate if the connection is active .
	virtual uint8_t connected();

	// Indicate if the connection is active .
	virtual operator bool();

	friend class WiFiServer;

	using Print::write;

protected:

	// Protected constructor .
	WiFiClient(uint8_t sock);

private:

	// Private subclass .
	class Context
	{

	public:

		// Constructor .
		Context();

		// Constructor .
		Context(uint8_t sock);

		// Destructor .
		virtual ~Context();

		// Get connection status .
		uint8_t status();

		// Connect to a specific IP address and port .
		int connect(IPAddress ip, uint16_t port);

		// Write a buffer of bytes .
		size_t write(const uint8_t *buffer, size_t length);

		// Return the number of bytes available to read .
		int available();

		// Read a byte .
		int read();

		// Read a buffer of bytes .
		int read(uint8_t *buffer, size_t length);

		// Peek a byte without extracting it from the receive buffer .
		int peek();

		// Flush internal transmission buffers .
		void flush();

		// Empty internal receive buffers .
		void empty();

		// Close the connection .
		void stop();

		// Indicate if the connection is active .
		uint8_t connected();

		// Increment the reference counter .
		void ref();

		// Decrement the reference counter and eventually delete the object .
		void unref();

	private:

		// Size of the internal buffer .
		enum { BUFFER_SIZE = 24 };

		// Socket identifier .
		uint8_t _sock;

		// Available data .
		uint16_t _availableData;

		// Index to current item in the buffer .
		uint16_t _bufferIndex;

		// Number of items stored in the buffer .
		uint16_t _bufferCount;

		// Internal buffer to cache input data .
		uint8_t _buffer[BUFFER_SIZE];

		// Reference counter .
		int _referenceCount;
	};

	// Pointer to client context .
	Context *_context;
};

#endif // WIFI_CLIENT_H_
