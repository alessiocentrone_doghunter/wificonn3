/*
 * WiFiConnConfig.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_CONFIG_H_
#define WIFI_CONN_CONFIG_H_

// Firmware version string .
#define WIFI_CONN_CLIENT_FIRMWARE_VERSION     	"3.0.0"

#if defined(WHATSNEXT_RED)

// Pin definitions .
#define WIFI_CONN_SLAVE_SELECT					20
#define WIFI_CONN_SLAVE_READY					7

#elif defined(WHATSNEXT_STAMP)

// Pin definitions .
#define WIFI_CONN_SLAVE_SELECT					PIN_ESP_CS	// Pin PE2 .
#define WIFI_CONN_SLAVE_READY					PIN_ESP_SR	// Pin PE0 .
#define WIFI_CONN_SLAVE_RESET					PIN_ESP_ON_OFF

#elif defined(ARDUINO_AVR_UNO)

// Pin definitions .
#define WIFI_CONN_SLAVE_SELECT					PIN_SPI_SS
#define WIFI_CONN_SLAVE_READY					9
#define WIFI_CONN_SLAVE_RESET					8

#elif defined(ARDUINO_AVR_MEGA2560)

// Pin definitions .
#define WIFI_CONN_SLAVE_SELECT					PIN_SPI_SS
#define WIFI_CONN_SLAVE_READY					49
#define WIFI_CONN_SLAVE_RESET					47

#else

#error "Sorry, WiFiConn library doesn't work with this board"

#endif

// Debug definitions .
#define WIFI_CONN_DEBUG_ESPI					1

#endif /* WIFI_CONN_CONFIG_H_ */
