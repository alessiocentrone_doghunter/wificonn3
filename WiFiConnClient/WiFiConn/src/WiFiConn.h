/*
 * WiFiConn.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_H_
#define WIFI_CONN_H_

// --- Includes .
#include "Arduino.h"
#include "IPAddress.h"

#include "utility/EspiMaster.h"

#include "WiFiConnConfig.h"
#include "WiFiConnDefinitions.h"
#include "WiFiConnTypes.h"

#include "WiFiClient.h"
#include "WiFiServer.h"

// --- Class declaration  .

// Class to interface the ESP8266 WiFi module .
class WiFiConn
{
public:

	/*
	 * Constructor
	 */
	WiFiConn();

	/*
	 * Destructor .
	 */
	virtual ~WiFiConn();

	/*
	 * Start WiFi connection to the last used access point .
	 *
	 * param timeout:		maximum connection time (optional)
	 */
	wl_status_t begin(uint32_t timeout = WL_CONNECTION_TIMEOUT);

	/*
	 *  Start WiFi connection for OPEN networks
	 *
	 * param ssid: 			Pointer to the SSID string
	 * param timeout:		maximum connection time (optional)
	 */
	wl_status_t begin(String ssid, uint32_t timeout = WL_CONNECTION_TIMEOUT);

	/*
	 * Start WiFi connection with passphrase
	 * the most secure supported mode will be automatically selected
	 *
	 * param ssid: 			Pointer to the SSID string.
	 * param passphrase: 	Passphrase. Valid characters in a passphrase must be between ASCII 32-126 (decimal)
	 * param timeout:		maximum connection time (optional)
	 */
	wl_status_t begin(String ssid, String passphrase, uint32_t timeout = WL_CONNECTION_TIMEOUT);

	/*
	 * Terminate the connection and release all resources
	 */
	void end();

	/*
	 * Disconnect from the network .
	 *
	 * param wifiOff: 		Flag to turn off the WiFi station after disconnection .
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool disconnect(bool wifiOff = false);

	/*
	 * Indicate if the system is connected to a network
	 *
	 *  return true if the system is connected
	 */
	bool isConnected();

	/*
	 * Return connection status.
	 *
	 * return: one of the value defined in wl_status_t
	 */
	wl_status_t status();

	/*
	 * Get firmware version
	 *
	 * return: firmware version string .
	 */
	String firmwareVersion();

	/*
	 * Get firmware version
	 *
	 * param major			Major version number
	 * param minor			Minor version number
	 * param patch			Patch version number
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool firmwareVersion(uint8_t &major, uint8_t &minor, uint16_t &patch);

	/*
	 * Check if the firmware version of ESP module is compatible with the library
	 *
	 * return: 'true' on success, 'false' otherwise
	 *
	 */
	bool checkFirmwareVersion();

	/*
	 * Check if the firmware version of ESP module is compatible with the library
	 *
	 * return: 'true' on success, 'false' otherwise
	 *
	 * note: DEPRECATED, use 'checkFirmwareVersion' without parameters instead
	 */
	bool checkFirmwareVersion(String firmwareVersionRequired);

	/*
	 * Change IP configuration settings disabling the DHCP client
	 *
	 * param localIP: 		Static IP configuration
	 * param gateway: 		Static gateway configuration
	 * param subnet:		Static sub-net mask
	 * param dnsServer:		IP configuration for DNS server 1 (optional)
	 * param dnsServer1:	IP configuration for DNS server 2 (optional)
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool config(IPAddress localIP, IPAddress gateway, IPAddress subnet, IPAddress dnsServer1 = 0ul, IPAddress dnsServer2 = 0ul);

	/*
	 * Change DNS IP configuration
	 *
	 * param dnsServer1: 	IP configuration for DNS server 1
	 * param dnsServer1: 	IP configuration for DNS server 2 (optional)
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool setDNS(IPAddress dnsServer1, IPAddress dnsServer2 = 0ul);

	/*
	 * Set the DHCP hostname
	 *
	 * param hostname:		max 32 characters length string .
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool setHostname(String hostname);

	/*
	 * Get the interface IP address.
	 *
	 * return: IP address value
	 */
	IPAddress localIP();

	/*
	 * Get the gateway IP address .
	 *
	 * return: gateway IP address value
	 */
	IPAddress gatewayIP();

	/*
	 * Get the interface sub-net mask address .
	 *
	 * return: sub-net mask address value
	 */
	IPAddress subnetMask();

	/*
	 * Get the interface MAC address .
	 *
	 * return: pointer to uint8_t array with length WL_MAC_ADDR_LENGTH
	 */
	uint8_t* macAddress(uint8_t* mac);

	/*
	 * Get the interface MAC address .
	 *
	 * return: String MAC
	 */
	String macAddress();

	/*
	 * Get the DNS IP address
	 *
	 * param dnsNumber:		specify from which DNS SERVER want to get the IP address
	 *
	 * return: IPAddress DNS Server IP
	 */
	IPAddress dnsIP(uint8_t dnsNumber = 0);

	/*
	 * Get the DHCP hostname
	 *
	 * return: hostname string
	 */
	String hostname();

	/*
	 * Resolve the given hostname to an IP address
	 *
	 * param hostname: 		Name to be resolved
	 * param hostIP: 		IPAddress structure to store the returned IP address
	 * param timeout_ms		Maximum time to resolve the hostname in milliseconds .
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool hostByName(String hostname, IPAddress& hostIP, uint32_t timeout_ms = 10000);

	/*
	 * Return the current SSID (Service Set Identifier) associated with the network .
	 *
	 * return: SSID string
	 */
	String SSID();

	/*
	 * Return the Encryption Type associated with the network .
	 *
	 * return: one value of wl_enc_type enum
	 */
	uint8_t	encryptionType();

	/*
	 * Return the current RSSI (Received Signal Strength Indicator) in dBm .
	 * associated with the network
	 *
	 * return: signed value
	 */
	int32_t RSSI();

	/*
	 * Return the current BSSID (Basic Service Set Identifier) associated with the network .
	 * It is the MAC address of the Access Point
	 *
	 * return: pointer to uint8_t array with length WL_MAC_ADDR_LENGTH
	 */
	uint8_t* BSSID(uint8_t* bssid);

	/*
	 * Return the current BSSID (Basic Service Set Identifier) associated with the network .
	 * It is the MAC address of the Access Point
	 *
	 * return: BSSID string
	 */
	String BSSID();

	/*
	 * Return the current channel associated with the network
	 *
	 * return: channel (1-13), 0 on error
	 */
	uint8_t channel();

	/*
	 * Return all informations associated with the network .
	 *
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *
	 *   return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi);

	/*
	 * Return all informations associated with the network .
	 *
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *  param bssid:		pointer to the Basic Service Set Identifier (String)
	 *  param channel:		reference to the channel used by the network
	 *  param isHidden:		reference to a boolean flag indicating if the network is hidden or not
	 *
	 *   return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi, String &bssid, uint8_t &channel, bool &isHidden);

	/*
	 * Return all informations associated with the network .
	 *
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *  param bssid:		pointer to the Basic Service Set Identifier (uint8_t array with length WL_MAC_ADDR_LENGTH)
	 *  param channel:		reference to the channel used by the network
	 *  param isHidden:		reference to a boolean flag indicating if the network is hidden or not
	 *
	 *   return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(String &ssid, uint8_t &encryption, int32_t &rssi, uint8_t *bssid, uint8_t &channel, bool &isHidden);

	/*
	 * Scan available WiFi networks
	 *
	 *  param showHidden: 	option to scan hidden networks
	 *
	 * return: number of discovered networks, WL_SCAN_RUNNING (-1) or WL_SCAN_FAILED (-2).
	 */
	int8_t scanNetworks(bool showHidden = false);

	/*
	 * Return the SSID (Service Set Identifier) discovered during the network scan .
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: ssid string of the specified item on the networks scanned list
	 */
	String SSID(uint8_t networkItem);

	/*
	 * Return the encryption type of the networks discovered during the scanNetworks
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: encryption type (enum wl_enc_type) of the specified item on the networks scanned list
	 */
	uint8_t	encryptionType(uint8_t networkItem);

	/*
	 * Return the RSSI (Received Signal Strength Indicator) of the networks discovered during the scanNetworks .
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: signed value of RSSI of the specified item on the networks scanned list
	 */
	int32_t RSSI(uint8_t networkItem);

	/*
	 * Return the BSSID (Basic Service Set Identifier) of the networks discovered during the scanNetworks .
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: pointer to uint8_t array with length WL_MAC_ADDR_LENGTH
	 */
	uint8_t* BSSID(uint8_t networkItem, uint8_t* bssid);

	/*
	 * Return the BSSID (Basic Service Set Identifier) of the networks discovered during the scanNetworks .
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: BSSID string
	 */
	String BSSID(uint8_t networkItem);

	/*
	 * Return the channel of the networks discovered during the scanNetworks .
	 *
	 * param networkItem: specify from which network item want to get the information
	 *
	 * return: channel (1-13), 0 on error
	 */
	uint8_t channel(uint8_t networkItem);

	/*
	 *  Return all informations about a scanned WiFi network .
	 *
	 *  param networkItem: 	specify from which network item want to get the information
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *
	 *   return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi);

	/*
	 *  Return all informations about a scanned WiFi network .
	 *
	 *  param networkItem: 	specify from which network item want to get the information
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *  param bssid:		pointer to the Basic Service Set Identifier (String)
	 *  param channel:		reference to the channel used by the network
	 *  param isHidden:		reference to a boolean flag indicating if the network is hidden or not
	 *
	 *   return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi, String &bssid, uint8_t &channel, bool &isHidden);

	/*
	 *  Return all informations about a scanned WiFi network .
	 *
	 *  param networkItem: 	specify from which network item want to get the information
	 *  param ssid: 		reference to the Service Set Identifier string
	 *  param encryption:	reference to the encryption type (enum wl_enc_type)
	 *  param rssi:			reference to the Basic Service Set Identifier.
	 *  param bssid:		pointer to the Basic Service Set Identifier (uint8_t array with length WL_MAC_ADDR_LENGTH)
	 *  param channel:		reference to the channel used by the network
	 *  param isHidden:		reference to a boolean flag indicating if the network is hidden or not
	 *
	 *  return: 'true' on success, 'false' otherwise
	 */
	bool getNetworkInfo(uint8_t networkItem, String &ssid, uint8_t &encryption, int32_t &rssi, uint8_t *bssid, uint8_t &channel, bool &isHidden);

	/*
	 *  Return the number of access points stored in the non-volatile memory of the WiFi module .
	 *
	 *  return: number of access point
	 */
	uint8_t getNumberOfStoredAccessPoints();

	/*
	 * Return the SSID (Service Set Identifier) stored in the non-volatile memory of the WiFi module .
	 *
	 * param accessPointItem: specify which access point item want to get the information
	 *
	 * return: ssid string of the specified item
	 */
	String storeSSID(uint8_t accessPointItem);

	/*
	 * Clear all Access Points stored in the non-volatile memory of the WiFi module .
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool clearStoredAccessPoints();

	/*
	 * Inform the WiFi module about the current running sketch .
	 *
	 * param sketchName: name of the current running sketch (Max 32 chars) .
	 *
	 * return: 'true' on success, 'false' otherwise
	 */
	bool setSketchName(String sketchName);

	/*
	 * Reset the WiFi module .
	 */
	void reset();

	/*
	 * Turn on the WiFi module .
	 */
	void sleep();

	/*
	 * Turn off the WiFi module .
	 */
	void wake();

protected:

	// Acquire the first available socket .
	uint8_t acquireServerSocket();

	// Release a socket .
	bool releaseServerSocket(uint8_t sock);

	// Start a TCP server on a specified port .
	bool startServer(uint8_t sock, IPAddress addr, uint16_t port);

	// Stop a TCP server .
	bool stopServer(uint8_t sock);

	// Get TCP server state .
	uint8_t getServerState(uint8_t sock);

	// Get a new client from the server connection .
	bool getServerClient(uint8_t sock, uint8_t newClientSock);


	// Acquire the first available socket .
	uint8_t acquireClientSocket();

	// Release a socket .
	bool releaseClientSocket(uint8_t sock);

	// Start a TCP client on a specified port .
	bool startClient(uint8_t sock, IPAddress ipAddress, uint16_t port);

	// Stop a TCP client .
	bool stopClient(uint8_t sock);

	// Get TCP client state .
	uint8_t getClientState(uint8_t sock);

	// Get TCP client available data .
	uint16_t getClientAvailableData(uint8_t sock);

	// Get client connection state and informations .
	bool getClientStateInfo(uint8_t sock, uint8_t &state, uint16_t &availForRead, uint16_t &availForWrite);

	// Peek client data .
	int peekClient(uint8_t sock);

	// Read client data .
	int readClient(uint8_t sock);

	// Read client buffer .
	uint16_t readClientBuffer(uint8_t sock, uint8_t *data, uint16_t length);

	// Write client buffer .
	uint16_t writeClientBuffer(uint8_t sock, uint8_t *data, uint16_t length);

	// Empty/Flush client buffer .
	bool emptyFlushClientBuffer(uint8_t sock, bool rx = true, bool tx = true);


	// Acquire the first available socket .
	uint8_t acquireUdpSocket();

	// Release a socket .
	bool releaseUdpSocket(uint8_t sock);

	// Start a UDP connection on a specified port .
	bool startUdp(uint8_t sock, uint16_t port);

	// Stop a UDP connection .
	bool stopUdp(uint8_t sock);

	// Start building up an UDP packet .
	bool beginUdpPacket(uint8_t sock, IPAddress ip, uint16_t port);

	// Finish off the packet and send it .
	bool endUdpPacket(uint8_t sock);

	// Parse incoming UDP packet and return the size of the packet in bytes .
	uint16_t parseUdpPacket(uint8_t sock);

	// Get UDP available data .
	uint16_t getUdpAvailableData(uint8_t sock);

	// Read UDP buffer .
	uint16_t readUdpBuffer(uint8_t sock, uint8_t *data, uint16_t length);

	// Write UDP buffer .
	uint16_t writeUdpBuffer(uint8_t sock, uint8_t *data, uint16_t length);

	// Empty/Flush UDP packet buffers .
	bool emptyFlushUdpBuffer(uint8_t sock, bool rx = true, bool tx = true);

	// Retrieve remote IP address and port .
	bool getUdpRemoteInfo(uint8_t sock, IPAddress &ip, uint16_t &port);

	// Retrieve local IP address and port .
	bool getUdpLocalInfo(uint8_t sock, IPAddress &ip, uint16_t &port);

private:

	enum Timeouts : uint32_t
	{
		WL_DEFAULT_TIMEOUT = 100,
		WL_CONNECTION_TIMEOUT = 10000,
		WL_SCAN_NETWORKS_TIMEOUT = 5500,
		WL_READ_TIMEOUT = 5500,
		WL_WRITE_TIMEOUT = 5500,
		WL_FLUSH_TIMEOUT = 5500,
	};

	// Indicate the state of use of the resources .
	bool _isDeployed;

	// Buffers used by the packets .
	uint8_t _ioBuffer[WL_IO_BUFFER_SIZE];

	// Packets .
	EspiPacket _requestPacket;
	EspiPacket _answerPacket;
	EspiPacket _eventPacket;

	// ESP master interface .
	EspiMaster _espiMaster;

	// Socket use state .
	bool _clientSocketInUse[MAX_SOCK_NUM];
	bool _serverSocketInUse[MAX_SOCK_NUM];
	bool _udpSocketInUse[MAX_SOCK_NUM];

	// Initialize internal member data .
	bool _init();

	// Release resource and reset internal member data to default values .
	void _deInit();

	// Turn on the WiFi module .
	void _sleep();

	// Turn off the WiFi module .
	void _wake();

#ifdef WIFI_CONN_DEBUG_ESPI

	// Print error message when it occurs .
	void _errorHandler(uint32_t errorCode);

#endif // WIFI_CONN_DEBUG_ESPI

	friend class WiFiClient;
	friend class WiFiServer;
	friend class WiFiUdp;

};

// --- Class instantiation .
extern WiFiConn WiFi;

// --- End of file .

#endif /* WIFI_CONN_H_ */
