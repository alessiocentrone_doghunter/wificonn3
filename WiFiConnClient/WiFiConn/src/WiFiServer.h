/*
 * WiFiServer.h
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_SERVER_H_
#define WIFI_SERVER_H_

#include "WiFiClient.h"

class WiFiServer
{
public:

	WiFiServer(uint16_t port);

	WiFiServer(IPAddress ip, uint16_t port);

	virtual ~WiFiServer();

	void begin();

	// Deprecated, only for compatibility with original WiFiLink library .
	WiFiClient available();

	WiFiClient availableClient();

	uint8_t status();

	void close();

	void stop();

private:

	uint8_t _sock;
	uint16_t _port;
	IPAddress _ip;
};

#endif /* WIFI_SERVER_H_ */
