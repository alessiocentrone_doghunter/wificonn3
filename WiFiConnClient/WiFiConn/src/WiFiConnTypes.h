/*
 * WiFiConnTypes.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef	WIFI_CONN_TYPES_H_
#define	WIFI_CONN_TYPES_H_

#include <inttypes.h>

// Error codes .
enum wl_error_codes
{
	WL_FAILURE = -1,
	WL_SUCCESS = 1,
};

/*
WL_CONNECTED: assigned when connected to a WiFi network;
WL_NO_SHIELD: assigned when no WiFi shield is present;
WL_IDLE_STATUS: it is a temporary status assigned when WiFi.begin() is called and remains active until the number of attempts expires (resulting in WL_CONNECT_FAILED) or a connection is established (resulting in WL_CONNECTED);
WL_NO_SSID_AVAIL: assigned when no SSID are available;
WL_SCAN_COMPLETED: assigned when the scan networks is completed;
WL_CONNECT_FAILED: assigned when the connection fails for all the attempts;
WL_CONNECTION_LOST: assigned when the connection is lost;
WL_DISCONNECTED: assigned when disconnected from a network;
 */
typedef enum: uint8_t
{
	WL_NO_SHIELD = 255,
	WL_NO_WIFI_MODULE_COMM = 255,
	WL_IDLE_STATUS = 0,
	WL_NO_SSID_AVAIL,
	WL_SCAN_COMPLETED,
	WL_CONNECTED,
	WL_CONNECT_FAILED,
	WL_CONNECTION_LOST,
	WL_DISCONNECTED

} wl_status_t;

// Authentication modes .
enum wl_auth_modes
{
	AUTH_MODE_INVALID,
	AUTH_MODE_AUTO,
	AUTH_MODE_OPEN_SYSTEM,
	AUTH_MODE_SHARED_KEY,
	AUTH_MODE_WPA,
	AUTH_MODE_WPA2,
	AUTH_MODE_WPA_PSK,
	AUTH_MODE_WPA2_PSK
};

// Encryption modes .
enum wl_enc_types
{
	/* Values map to 802.11 encryption suites... */
	ENC_TYPE_WEP  = 5,
	ENC_TYPE_TKIP = 2,
	ENC_TYPE_CCMP = 4,
	/* ... except these two, 7 and 8 are reserved in 802.11-2007 */
	ENC_TYPE_NONE = 7,
	ENC_TYPE_AUTO = 8,
	ENC_TYPE_UNKNOWN = 255
};

// TCP server/client socket state .
enum wl_tcp_states
{
	CLOSED      = 0,
	LISTEN      = 1,
	SYN_SENT    = 2,
	SYN_RCVD    = 3,
	ESTABLISHED = 4,
	FIN_WAIT_1  = 5,
	FIN_WAIT_2  = 6,
	CLOSE_WAIT  = 7,
	CLOSING     = 8,
	LAST_ACK    = 9,
	TIME_WAIT   = 10
};

// List of supported command codes .
enum wl_command_codes
{
	CONNECT_CMD = 0x01,
	DISCONNECT_CMD,

	GET_CONN_STATUS_CMD,
	GET_FW_VERSION_CMD,

	SET_IP_CONFIG_CMD,
	SET_DNS_CONFIG_CMD,
	SET_HOSTNAME_CMD,

	GET_LOCAL_IPADDR_CMD,
	GET_GATEWAY_IPADDR_CMD,
	GET_SUBNET_MASK_CMD,
	GET_MACADDR_CMD,
	GET_DNS_IPADDR_CMD,
	GET_HOSTNAME_CMD,
	GET_HOST_BY_NAME_CMD,

	GET_CURR_SSID_CMD,
	GET_CURR_ENCT_CMD,
	GET_CURR_RSSI_CMD,
	GET_CURR_BSSID_CMD,
	GET_CURR_CHANNEL_CMD,
	GET_CURR_NET_INFO_CMD,

	SCAN_NETWORKS_CMD,

	GET_IDX_SSID_CMD,
	GET_IDX_ENCT_CMD,
	GET_IDX_RSSI_CMD,
	GET_IDX_BSSID_CMD,
	GET_IDX_CHANNEL_CMD,
	GET_IDX_NET_INFO_CMD,

	START_SERVER_CMD,
	STOP_SERVER_CMD,
	GET_SERVER_STATE_CMD,
	GET_SERVER_CLIENT_CMD,

	START_CLIENT_CMD,
	STOP_CLIENT_CMD,
	GET_CLIENT_STATE_CMD,
	GET_CLIENT_AVAIL_DATA_CMD,
	GET_CLIENT_STATE_INFO_CMD,
	PEEK_CLIENT_DATA_CMD,
	READ_CLIENT_DATA_CMD,
	READ_CLIENT_BUFF_CMD,
	WRITE_CLIENT_BUFF_CMD,
	EMPTY_FLUSH_CLIENT_BUFF_CMD,

	START_UDP_CMD,
	STOP_UDP_CMD,
	BEGIN_UDP_PKT_CMD,
	END_UDP_PKT_CMD,
	PARSE_UDP_PKT_CMD,
	GET_UDP_AVAIL_DATA_CMD,
	READ_UDP_BUFF_CMD,
	WRITE_UDP_BUFF_CMD,
	EMPTY_FLUSH_UDP_BUFF_CMD,
	GET_UDP_REMOTE_INFO_CMD,
	GET_UDP_LOCAL_INFO_CMD,

	GET_NUM_STORED_AP_CMD,
	GET_STORED_SSID_CMD,
	CLEAR_STORED_AP_CMD,

	SET_SKETCH_NAME_CMD,

	UNKNOWN_CMD	= 0xff,
};


#endif // WIFI_CONN_TYPES_H_
