/*
 * WiFiConnDefinitions.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_DEFINITIONS_H_
#define WIFI_CONN_DEFINITIONS_H_

// Required firmware version string .
#define WIFI_FIRMWARE_REQUIRED 				"3.0.0"

// Required firmware version numbers .
#define WIFI_FIRMWARE_REQUIRED_MAJOR		3
#define WIFI_FIRMWARE_REQUIRED_MINOR		0
#define WIFI_FIRMWARE_REQUIRED_PATCH		0

// Maximum size of a SSID
#define WL_SSID_MAX_LENGTH 					32

// Length of passphrase. Valid lengths are 8-63.
#define WL_WPA_KEY_MAX_LENGTH 				63

// Length of key in bytes. Valid values are 5 and 13.
#define WL_WEP_KEY_MAX_LENGTH 				13

// Size of a MAC-address or BSSID
#define WL_MAC_ADDR_LENGTH 					6

// Size of a MAC-address or BSSID
#define WL_IPV4_LENGTH 						4

// Maximum size of a SSID list
#define WL_NETWORKS_LIST_MAXNUM				10

// Maximum number of socket
#define	MAX_SOCK_NUM						4

// Socket not available constant
#define SOCK_NOT_AVAIL  					255

// Default state value for WiFi state field
#define NA_STATE 							-1

// Maximum number of attempts to establish WiFi connection
#define WL_MAX_ATTEMPT_CONNECTION			10

#define WL_SCAN_RUNNING   					(-1)
#define WL_SCAN_FAILED    					(-2)

// Buffer size definitions .
#define WL_IO_BUFFER_SIZE					128
#define WL_REQ_BUFFER_SIZE					WL_IO_BUFFER_SIZE
#define WL_ANS_BUFFER_SIZE					WL_IO_BUFFER_SIZE
#define WL_EVT_BUFFER_SIZE					WL_IO_BUFFER_SIZE

#endif /* WIFI_CONN_DEFINITIONS_H_ */
