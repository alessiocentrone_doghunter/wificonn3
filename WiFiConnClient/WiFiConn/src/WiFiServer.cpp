/*
 * WiFiServer.cpp
 *
 * Dog Hunter. All right reserved.
 *
 * Created by Alessio Centrone, Dario Trimarchi, Chiara Ruggeri.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConn.h"

#include "WiFiServer.h"

/*******************************************************************************
    Public methods implementation .
*******************************************************************************/

WiFiServer::WiFiServer(uint16_t port) :
	_sock(SOCK_NOT_AVAIL),
	_port(port),
	_ip()
{

}

WiFiServer::WiFiServer(IPAddress ip, uint16_t port) :
	_sock(SOCK_NOT_AVAIL),
	_port(port),
	_ip(ip)
{

}

WiFiServer::~WiFiServer()
{
	stop();
}

void WiFiServer::begin()
{
	if (_sock == SOCK_NOT_AVAIL)
	{
		_sock = WiFi.acquireServerSocket();
	}

	if (_sock != SOCK_NOT_AVAIL)
	{
		if (WiFi.startServer(_sock, _ip, _port) == false)
		{
			WiFi.releaseServerSocket(_sock);
		}
	}
}

WiFiClient WiFiServer::available()
{
	return availableClient();
}

WiFiClient WiFiServer::availableClient()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		uint8_t newClientSock = WiFi.acquireClientSocket();

		if (newClientSock != SOCK_NOT_AVAIL)
		{
			if (WiFi.getServerClient(_sock, newClientSock) == true)
			{
				return WiFiClient(newClientSock);
			}

			WiFi.releaseClientSocket(newClientSock);
		}
	}

	return WiFiClient();
}

uint8_t WiFiServer::status()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		return WiFi.getServerState(_sock);
	}

	return (uint8_t)(wl_tcp_states::CLOSED);
}

void WiFiServer::close()
{
	if (_sock != SOCK_NOT_AVAIL)
	{
		// Close the connection .
		WiFi.stopServer(_sock);

		// Release the socket .
		WiFi.releaseServerSocket(_sock);

		_sock = SOCK_NOT_AVAIL;
	}
}

void WiFiServer::stop()
{
	close();
}

/******************************************************************************/
