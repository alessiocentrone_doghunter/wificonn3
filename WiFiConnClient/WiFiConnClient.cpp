/*
 * WiFiConnClient.cpp
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "WiFiConnClient.h"
#include "WiFiConn/src/WiFiConn.h"


const char ssid[] = "whatsnext-guest";
const char passphrase[] = "ht34!eG$";


// The setup function is called once at startup of the sketch .
void setup()
{
	// Initialize random seed .
	srand(analogRead(0));

	// Initialize LED pin as an output, so it can be used to inform the user when scanning occurs.
	pinMode(LED_BUILTIN, OUTPUT);

	// Initialize serial port .
	Serial.begin(115200);

	// Print 'WiFiConnServer' firmware version .
	Serial.print("'WiFiConn' firmware version: ");
	Serial.println(WiFi.firmwareVersion());

	// Read and print the connection status .
	wl_status_t status = WiFi.status();

	Serial.print("'WiFiConn' status: ");
	Serial.println(wifiStatusToString(status));
}

// The loop function is called in an endless loop .
void loop()
{
	int c;

	// Check for available character .
	if ((c = Serial.read()) != EOF)
	{
		switch(c)
		{
			case '0':
			{
				Serial.println("'WiFiConn' set host name: 'ESP_CAF7F3'");

				WiFi.setHostname("ESP_CAF7F3");

				break;
			}
			case '1':
			{
				Serial.println("'WiFiConn' set host name: 'Dog_Hunter'");

				WiFi.setHostname("Dog_Hunter");

				break;
			}
			case '2':
			{
				Serial.println("'WiFiConn' set host name: 'WhatsNext'");

				WiFi.setHostname("WhatsNext");

				break;
			}
			case '3':
			{
				Serial.print("'WiFiConn' set static IP: 192.168.84.25 ...");

				IPAddress localIP = IPAddress(192, 168, 84, 25);
				IPAddress gateway = IPAddress(192, 168, 84, 1);
				IPAddress subnet = IPAddress(255, 255, 255, 0);

				if (WiFi.config(localIP, gateway, subnet) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case '4':
			{
				Serial.print("'WiFiConn' set static IP: 192.168.84.20 ... ");

				IPAddress localIP = IPAddress(192, 168, 84, 20);
				IPAddress gateway = IPAddress(192, 168, 84, 1);
				IPAddress subnet = IPAddress(255, 255, 255, 0);

				if (WiFi.config(localIP, gateway, subnet) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case '5':
			{
				Serial.print("'WiFiConn' set static IP: 192.168.84.66 ... ");

				IPAddress localIP = IPAddress(192, 168, 84, 66);
				IPAddress gateway = IPAddress(192, 168, 84, 1);
				IPAddress subnet = IPAddress(255, 255, 255, 0);

				if (WiFi.config(localIP, gateway, subnet) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case '6':
			{
				Serial.print("'WiFiConn' set dynamic IP: 0.0.0.0 ... ");

				IPAddress localIP = IPAddress(0, 0, 0, 0);
				IPAddress gateway = IPAddress(0, 0, 0, 0);
				IPAddress subnet = IPAddress(0, 0, 0, 0);

				if (WiFi.config(localIP, gateway, subnet) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case '7':
			{
				Serial.print("'WiFiConn' set Norton ConnectSafe DNS ... ");

				IPAddress dns1 = IPAddress(199, 85, 126, 10);
				IPAddress dns2 = IPAddress(199, 85, 127, 10);
				IPAddress ntpServerIP;

				if (WiFi.setDNS(dns1, dns2) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case '8':
			{
				Serial.print("'WiFiConn' set Free DNS ... ");

				IPAddress dns1 = IPAddress(37, 235, 1, 174);
				IPAddress dns2 = IPAddress(37, 235, 1, 177);
				IPAddress ntpServerIP;

				if (WiFi.setDNS(dns1, dns2) == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case 'a':	// Begin connection .
			{
				Serial.print("'WiFiConn' auto-connection ... ");

				// Try to connect to the best stored access point .
				wl_status_t status = WiFi.begin();

				Serial.println(wifiStatusToString(status));

				break;
			}
			case 'b':	// Begin connection .
			{
				Serial.print("'WiFiConn' begin connection ... ");

				// Try to connect to the access point .
				wl_status_t status = WiFi.begin(ssid, passphrase);

				Serial.println(wifiStatusToString(status));

				break;
			}
			case ' ':
			{
				// Read and print the connection status .
				wl_status_t status = WiFi.status();

				Serial.print("'WiFiConn' status: ");
				Serial.println(wifiStatusToString(status));

				break;
			}
			case 'd':
			{
				WiFi.disconnect(false);

				Serial.println("'WiFiConn' disconnect with 'WiFi On'");

				break;
			}
			case 'D':
			{
				WiFi.disconnect(true);

				Serial.println("'WiFiConn' disconnect with 'WiFi Off'");

				break;
			}
			case 'e':	// End connection .
			{
				WiFi.end();

				Serial.println("'WiFiConn' end connection");

				break;
			}
			case 's':
			{
				Serial.print("'WiFiConn' scan networks ... ");

				// Scan the available networks .
				int8_t numberOfNetworks = WiFi.scanNetworks();

				listAvailableNetworks(numberOfNetworks);

				break;
			}

			case 'i':
			{
				Serial.println("'WiFiConn' connection infos");

				// Print current connection informations .
				printConnectionInfo();

				break;
			}

			case 'n':
			{
				Serial.println("'WiFiConn' network infos");

				// Print current network informations .
				printCurrentNetworkInfo();

				break;
			}
			case 'h':
			{
				Serial.println("'WiFiConn' host by name");

				IPAddress ip;

				if (WiFi.hostByName("time.nist.gov", ip) == true)
				{
					Serial.print(" - The address of 'time.nist.gov' is ");
					Serial.println(ipToString(ip));
				}
				else
				{
					Serial.println(" - Unable to solve the Internet address!");
				}

				break;
			}
			case 'l':
			{
				Serial.println("'WiFiConn' list of stored Access Points");

				uint8_t numberOfAccessPoints = WiFi.getNumberOfStoredAccessPoints();

				listStoreAccessPoints(numberOfAccessPoints);

				break;
			}
			case 'c':
			{
				Serial.print("'WiFiConn' clear stored Access Points ... ");

				if (WiFi.clearStoredAccessPoints() == true)
				{
					Serial.println("OK");
				}
				else
				{
					Serial.println("failed!");
				}

				break;
			}
			case 'f':
			{
				Serial.print("'WiFiConn' fill Access Point Vector with ssid: ");

				// Try to connect to a dummy network .
				String dummy_ssid = "Telecom" + (String)(rand() & 1000);

				// The connection will fail but the ssid will be stored .
				WiFi.begin(dummy_ssid, 0);

				Serial.println(dummy_ssid);

				break;
			}
		}
	}
}

// Print current connection informations .
void printConnectionInfo()
{
	Serial.println(" - Local IP address .... : " + ipToString(WiFi.localIP()));
	Serial.println(" - Gateway ............. : " + ipToString(WiFi.gatewayIP()));
	Serial.println(" - Subnet mask ......... : " + ipToString(WiFi.subnetMask()));
	Serial.println(" - MAC address ......... : " + WiFi.macAddress());
	Serial.println(" - DNS 1 address ....... : " + ipToString(WiFi.dnsIP(0)));
	Serial.println(" - DNS 2 address ....... : " + ipToString(WiFi.dnsIP(1)));
	Serial.println(" - Hostname ............ : " + WiFi.hostname());
}

// Print current network informations .
void printCurrentNetworkInfo()
{
	String ssid;
	int32_t rssi = 0;
	uint8_t enc;

	String bssid;
	uint8_t channel;
	bool isHidden;

	bool success = WiFi.getNetworkInfo(ssid, enc, rssi, bssid, channel, isHidden);

	if (success)
	{
		printNetworkInfo(0, ssid, enc, rssi, bssid, channel);
	}
	else
	{
		Serial.println(" - Error or data not available!");
	}

	/*
	ssid = WiFi.SSID();
	rssi = WiFi.RSSI();
	enc = WiFi.encryptionType();

	bssid = WiFi.BSSID();
	channel = WiFi.channel();

	printNetworkInfo(0, ssid, enc, rssi, bssid, channel);
	*/
}

// Convert an IP address into a string .
String ipToString(IPAddress ip)
{
	char ipStr[16] = { 0 };

	sprintf(ipStr, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);

	return String(ipStr);
}

// Print the list of available networks and related informations .
void listAvailableNetworks(int8_t numberOfNetworks)
{
	if (numberOfNetworks < 0)
	{
		Serial.println("scanning error!");
		return;
	}

	Serial.print(numberOfNetworks);
	Serial.println(" networks found");

	// Print the network number and name for each network found .
	for (int networkItem = 0; networkItem < numberOfNetworks; networkItem++)
	{
		String ssid;
		int32_t rssi = 0;
		uint8_t enc;

		String bssid;
		uint8_t channel;
		bool isHidden;

		bool success = WiFi.getNetworkInfo(networkItem, ssid, enc, rssi, bssid, channel, isHidden);

		if (success)
		{
			printNetworkInfo(networkItem + 1, ssid, enc, rssi, bssid, channel);
		}
		else
		{
			Serial.print(" - Error parsing data of network");
			Serial.println((networkItem + 1));
		}
	}
}

// Print the list of the Access Point stored in the non-volatile memory of WiFi module .
void listStoreAccessPoints(uint8_t numberOfAccessPoints)
{
	if (numberOfAccessPoints == 0)
	{
		Serial.println("no Access Point stored!");
		return;
	}

	for (uint8_t i = 0; i < numberOfAccessPoints; i++)
	{

		if (i < 10)
		{
			Serial.print('0');
		}
		Serial.print(i);
		Serial.print(") ");

		Serial.println(WiFi.storeSSID(i));
	}
}

// Print current network informations .
void printNetworkInfo(uint8_t networkItem, const String &ssid, uint8_t encryption, int32_t rssi, const String &bssid, uint8_t channel)
{
	if (networkItem < 10)
	{
		Serial.print('0');
	}
	Serial.print(networkItem);
	Serial.print(") ");

	Serial.print(ssid);

	// Add alignment spaces .
	for (int i = ssid.length(); i < 30; i++)
	{
		Serial.print(' ');
	}

	Serial.print("ch: ");
	if (channel < 10)
	{
		Serial.print('0');
	}
	Serial.print(channel);
	Serial.print(", ");
	Serial.print(rssi);
	Serial.print(" dBm");
	Serial.print(", ");
	Serial.print(bssid);
	Serial.print(", ");
	Serial.println(encTypeToString(encryption));
}

// Convert an encryption type into a string .
String encTypeToString(int encType)
{
	if (encType == ENC_TYPE_NONE)
	{
		return "none";
	}
	if (encType == ENC_TYPE_WEP)
	{
		return "wep";
	}
	if (encType == ENC_TYPE_TKIP)
	{
		return "wpa";
	}
	if (encType == ENC_TYPE_CCMP)
	{
		return "wpa2";
	}
	if (encType == ENC_TYPE_AUTO)
	{
		return "auto";
	}
	return "unknown";
}

// Convert the Wi-Fi status into a string .
const char *wifiStatusToString(wl_status_t status)
{
	switch(status)
	{
		case WL_NO_WIFI_MODULE_COMM: 	return "no WiFi module communication"; break;
		case WL_IDLE_STATUS:			return "idle"; break;
		case WL_NO_SSID_AVAIL:			return "no SSID available"; break;
		case WL_SCAN_COMPLETED:			return "scan completed"; break;
		case WL_CONNECTED:				return "connected"; break;
		case WL_CONNECT_FAILED:			return "connection failed"; break;
		case WL_CONNECTION_LOST:		return "connection lost"; break;
		case WL_DISCONNECTED:			return "disconnected"; break;
		default:						return "unknown"; break;
	}
}
