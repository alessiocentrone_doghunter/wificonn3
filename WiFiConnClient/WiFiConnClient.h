/*
 * WiFiConnClient.h
 *
 * Dog Hunter.  All right reserved.
 *
 * Created by Alessio Centrone.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WIFI_CONN_CLIENT_H_
#define WIFI_CONN_CLIENT_H_

#include "Arduino.h"
#include "IPAddress.h"

#include "WiFiConn/src/WiFiConnDefinitions.h"
#include "WiFiConn/src/WiFiConnTypes.h"

// --- Function declaration .

// Print current connection informations .
void printConnectionInfo();

// Print current network informations .
void printCurrentNetworkInfo();

// Convert an IP address into a string .
String ipToString(IPAddress ip);

// Print the list of available networks and related informations .
void listAvailableNetworks(int8_t numberOfNetworks);

// Print the list of the Access Point stored in the non-volatile memory of WiFi module .
void listStoreAccessPoints(uint8_t numberOfAccessPoints);

// Print current network informations .
void printNetworkInfo(uint8_t networkItem, const String &ssid, uint8_t encryption, int32_t rssi, const String &bssid, uint8_t channel);

// Convert an encryption type into a string .
String encTypeToString(int encType);

// Convert the WiFi status into a string .
const char *wifiStatusToString(wl_status_t status);

#endif /* WIFI_CONN_CLIENT_H_ */
