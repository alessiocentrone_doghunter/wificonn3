#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-10-03 12:45:18

#include "Arduino.h"
#include "WiFiConn/src/WiFiConn.h"
#include "WiFiConn/src/WiFiUdp.h"

void setup() ;
void loop() ;
void startWiFi() ;
void startUdp() ;
void sendNtpPacket(IPAddress& address) ;
uint32_t getTime() ;
void printTime(uint32_t utcTime) ;
inline int getSeconds(uint32_t unixTime) ;
inline int getMinutes(uint32_t unixTime) ;
inline int getHours(uint32_t unixTime) ;


#include "WiFiUdpNtpClient.ino"

#endif
